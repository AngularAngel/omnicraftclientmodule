#version 420

#define BLUR_LENGTH 9

#include <lib/ubo.glsl>

uniform sampler2D u_base_tex;
uniform sampler2D u_blur_tex;
uniform bool u_horizontal;
uniform float[BLUR_LENGTH] u_blur_factors;

in vec2 v_tex_coord;

out vec3 out_color;

void main() {
    vec3 base_color = texture(u_base_tex, v_tex_coord).rgb;

    vec3 blur_color = vec3(0.0);
    
    if (u_horizontal)
        for (int i = 0; i < BLUR_LENGTH; i++) {
            blur_color += texture(u_blur_tex, v_tex_coord + vec2(i - (BLUR_LENGTH - 1) / 2, 0)/u_screen_size.x).rgb*u_blur_factors[i];
        }
    else
        for (int i = 0; i < BLUR_LENGTH; i++) {
            blur_color += texture(u_blur_tex, v_tex_coord + vec2(i - (BLUR_LENGTH - 1) / 2, 0)/u_screen_size.y).rgb*u_blur_factors[i];
        }
    
    out_color = mix(base_color, blur_color, 1.0/16.0);
}