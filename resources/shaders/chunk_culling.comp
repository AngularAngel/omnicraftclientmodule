#version 430

#include <lib/chunk_culling_buffers.glsl>

#define VISIBILITY_CHECK_INTERVAL 10

uniform vec4[6] u_view_frustum;

layout(local_size_x = 1, local_size_y = 1, local_size_z = 1) in;

int planeVisibility(vec4 clip, BoundingBox box) {
    float x0 = box.startx * clip.x;
    float x1 = box.endx * clip.x;
    float y0 = box.starty * clip.y;
    float y1 = box.endy * clip.y;
    float z0 = box.startz * clip.z + clip.w;
    float z1 = box.endz * clip.z + clip.w;
    float p1 = x0 + y0 + z0;
    float p2 = x1 + y0 + z0;
    float p3 = x1 + y1 + z0;
    float p4 = x0 + y1 + z0;
    float p5 = x0 + y0 + z1;
    float p6 = x1 + y0 + z1;
    float p7 = x1 + y1 + z1;
    float p8 = x0 + y1 + z1;

    if (p1 <= 0 && p2 <= 0 && p3 <= 0 && p4 <= 0 && p5 <= 0 && p6 <= 0 && p7 <= 0 && p8 <= 0) {
        return NOT_IN_FRUSTUM;
    }

  return IN_FRUSTUM;
}

int frustumVisibility(BoundingBox box, vec4[6] frustum) {
    int v0 = planeVisibility(frustum[0], box);
    if (v0 == NOT_IN_FRUSTUM) {
        return NOT_IN_FRUSTUM;
    }

    int v1 = planeVisibility(frustum[1], box);
    if (v1 == NOT_IN_FRUSTUM) {
        return NOT_IN_FRUSTUM;
    }

    int v2 = planeVisibility(frustum[2], box);
    if (v2 == NOT_IN_FRUSTUM) {
        return NOT_IN_FRUSTUM;
    }

    int v3 = planeVisibility(frustum[3], box);
    if (v3 == NOT_IN_FRUSTUM) {
        return NOT_IN_FRUSTUM;
    }

    int v4 = planeVisibility(frustum[4], box);
    if (v4 == NOT_IN_FRUSTUM) {
        return NOT_IN_FRUSTUM;
    }

    return IN_FRUSTUM;
}

bool differentFrustums(vec4[6] first, vec4[6] second) {
    for (int i = 0; i < 6; i++) {
        if (first[i] != second[i])
            return true;
    }
    return false;
}

void main() {
    int id = int(gl_GlobalInvocationID.x);

    if (id >= chunkCullingData.length()) {
        return;
    }   

    ChunkCullingData chunkData = chunkCullingData[id];

    IndirectData indirectData = chunkData.indirectData;

    if (indirectData.count < 1) {
        return;
    }

    int counter = (chunkData.temporalCulling[u_temporal_index] & 0xF0) >> 4;
    int frustumStatus = (chunkData.temporalCulling[u_temporal_index] & 12) >> 2;
    int visibleStatus = chunkData.temporalCulling[u_temporal_index] & 3;
    
    bool difFrustums = differentFrustums(prevFrustums[u_temporal_index], u_view_frustum);

    if (frustumStatus == NOT_IN_FRUSTUM && difFrustums) {
        frustumStatus = UNKNOWN_IN_FRUSTUM;
    }

    if (frustumStatus == UNKNOWN_IN_FRUSTUM) {
        if (frustumVisibility(chunkData.boundingBox, u_view_frustum) == NOT_IN_FRUSTUM) {
            frustumStatus = NOT_IN_FRUSTUM;
            visibleStatus = NOT_VISIBLE;
        } else {
            frustumStatus = IN_FRUSTUM;
            visibleStatus = UNKNOWN_VISIBLE;
        }
    }
    
    if (frustumStatus == IN_FRUSTUM) {
        if (visibleStatus == NOT_VISIBLE) {
            counter += 1;
            if (counter >= VISIBILITY_CHECK_INTERVAL || difFrustums) {
                counter = 0;
                visibleStatus = UNKNOWN_VISIBLE;
            }
        }
        if (visibleStatus != NOT_VISIBLE) {
            counter = 0;
            uint indirectDrawID = atomicCounterIncrement(indirectDrawCounter);
            indirectDrawCommands[indirectDrawID] = DrawElementsIndirectCommand(indirectData.count, 1, indirectData.firstIndex, indirectData.baseVertex, 0);
            frustumStatus = IN_FRUSTUM;
            visibleStatus = NOT_VISIBLE;
        }
    }

    chunkCullingData[id].temporalCulling[u_temporal_index] = (counter << 4) + (frustumStatus << 2) + visibleStatus;
}