#version 430

#include <lib/definitions.glsl> 
#include <lib/chunk_culling_buffers.glsl>

in vec3 in_pos;

uniform mat4 u_model_matrix;

void main()
{
    gl_Position = u_model_matrix * vec4(in_pos, 1.0);

    updateTemporalCohesion(getBlockPos(gl_Position.xyz));
}  
