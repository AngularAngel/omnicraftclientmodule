#version 430

#include <lib/ubo.glsl>
#include <lib/transparency_buffers.glsl>
#include <lib/surface_textures.glsl>
#include <lib/light_constants.glsl>
#include <lib/light_calcs.glsl>

uniform sampler2DArrayShadow u_shadow_tex;

uniform mat4[6] u_shadow_mats;

uniform vec3 u_light_direction;
uniform vec3 u_light_color;
uniform vec3[6] u_shadow_positions;

uniform bool u_has_shadows;

in vec2 v_tex_coord;
in vec3 v_world_dir;
in vec3[6] v_shadow_world_dirs;

out vec3 out_color;

bool saturated(vec3 v) {
    return v.x >= 0.0 && v.x <= 1.0 &&
           v.y >= 0.0 && v.y <= 1.0 &&
           v.z >= 0.0 && v.z <= 1.0;
}

float getShadow(float cosang, float texel_depth, vec3 texel_world_dir) {
    vec3[6] texel_shadow_world_dirs;

    for (int i = 0; i < 6; i++)
        texel_shadow_world_dirs[i] = mat3(u_shadow_mats[i]) * texel_world_dir;

    if (u_has_shadows) {
        //Choose best shadow cascade.
        int shadow_map;
        vec3 shadow_pos;
        for (shadow_map = 0; shadow_map < 6; shadow_map++) {
            shadow_pos = (u_shadow_positions[shadow_map] + texel_shadow_world_dirs[shadow_map] * texel_depth) * 0.5 + 0.5;
            if (saturated(shadow_pos)) break;
        }
        
        //Shadow mapping.
        float slope = sqrt(1.0 - cosang * cosang) / cosang;
        shadow_pos.z -= (slope * SHADOW_SLOPE_BIAS + SHADOW_BIAS) * exp(shadow_map * 1.7);
        return texture(u_shadow_tex, vec4(shadow_pos.xy, shadow_map, shadow_pos.z));
    } return 1.0;
}

vec3 getDirectionLight(float depth, vec4 color, vec3 normal, vec3 material) {
    if (length(normal) == 0)
        return vec3(0);

    float cosang = dot(normal, u_light_direction);

    if (cosang <= 0.0)
        return vec3(0);

    vec3 relative_pos = depth*v_world_dir;
    vec3 pos = relative_pos + u_camera_pos.xyz;
    vec3 texel_pos = (floor(pos / (u_texel_length * 2) - normal * 0.1) + vec3(0.5) + normal * 0.5) * (u_texel_length * 2);
    
    vec3 texel_relative_to_camera = texel_pos - u_camera_pos.xyz;
    
    float texel_depth = (texel_relative_to_camera/v_world_dir).x;
    vec3 texel_world_dir = texel_relative_to_camera / texel_depth;

    float shadow = getShadow(cosang, texel_depth, texel_world_dir);
    
    if (shadow <= 0.001)
        return vec3(0);

    //Irradiance is really easy with directional lights!
    vec3 irradiance = u_light_color;

    vec3 reflection = getReflection(color, normal, material, u_light_direction, texel_world_dir, cosang);
    
    return getLighting(irradiance, shadow, reflection);
}

void main() {
    // retrieve data from G-buffer
    float depth = texture(u_depth_tex, v_tex_coord).r * u_z_far;
    vec3 color = texture(u_color_tex, v_tex_coord).rgb;
    vec3 normal = texture(u_normal_tex, v_tex_coord).rgb * 2 - 1;
    vec3 material = texture(u_material_tex, v_tex_coord).rgb;
    
    out_color = getDirectionLight(depth, vec4(color, 1), normal, material);

    int frag_header_index = int(gl_FragCoord.x) + int(gl_FragCoord.y) * u_screen_size.x;
    int curNodeID = surfaceHeaderNodes[frag_header_index];
    while (curNodeID != -1) {
        SurfaceFragmentNode curNode = surfaceFragmentNodes[curNodeID];
        curNodeID = curNode.nextID;

        SurfaceFragment fragment = curNode.fragment;

        if (fragment.depth * u_z_far > depth)
            continue;

        out_color = mix(out_color, out_color*fragment.color.rgb, fragment.color.a) + getDirectionLight(fragment.depth * u_z_far, fragment.color, fragment.normal, fragment.material);
    }
    
    //out_color = vec3(0);
}