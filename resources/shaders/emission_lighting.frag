#version 430

#include <lib/ubo.glsl>
#include <lib/transparency_buffers.glsl>
#include <lib/surface_textures.glsl>

in vec2 v_tex_coord;

out vec3 out_color;

void main() {

    // retrieve data from G-buffer
    float depth = texture(u_depth_tex, v_tex_coord).r;
    vec3 color = texture(u_color_tex, v_tex_coord).rgb;
    vec3 normal = texture(u_normal_tex, v_tex_coord).rgb * 2 - 1;
    vec3 light = texture(u_light_tex, v_tex_coord).rgb;
    
    if (length(normal) < 0.1) {
        out_color = color;
    }
    
    out_color += light;

    int frag_header_index = int(gl_FragCoord.x) + int(gl_FragCoord.y) * u_screen_size.x;
    
    int curNodeID = surfaceHeaderNodes[frag_header_index];
    while (curNodeID != -1) {
        SurfaceFragmentNode curNode = surfaceFragmentNodes[curNodeID];
        curNodeID = curNode.nextID;

        SurfaceFragment fragment = curNode.fragment;

        if (fragment.depth > depth)
            continue;
        
        out_color = mix(out_color, out_color*fragment.color.rgb, fragment.color.a) + fragment.light;
    }
    
    //out_color = vec3(0);
}