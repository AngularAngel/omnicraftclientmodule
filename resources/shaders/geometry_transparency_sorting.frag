#version 430

#include <lib/ubo.glsl>
#include <lib/transparency_buffers.glsl>
#include <lib/geometry_textures.glsl>
#include <lib/definitions.glsl> 

in vec2 v_tex_coord;

void main() {
    int frag_header_index = int(gl_FragCoord.x) + int(gl_FragCoord.y) * u_screen_size.x;

    // retrieve data from G-buffer
    float opaqueDepth = texture(u_depth_tex, v_tex_coord).r;
    
    //Initialize a list to sort fragments into.
    GeometryFragment[] fragmentList = GeometryFragment[MAX_GEOMETRY_TRANSPARENCY_LAYERS](GeometryFragment(0, vec3(0), ivec2(0), ivec2(0)),
                                                           GeometryFragment(0, vec3(0), ivec2(0), ivec2(0)),
                                                           GeometryFragment(0, vec3(0), ivec2(0), ivec2(0)),
                                                           GeometryFragment(0, vec3(0), ivec2(0), ivec2(0)),
                                                           GeometryFragment(0, vec3(0), ivec2(0), ivec2(0)),
                                                           GeometryFragment(0, vec3(0), ivec2(0), ivec2(0)),
                                                           GeometryFragment(0, vec3(0), ivec2(0), ivec2(0)),
                                                           GeometryFragment(0, vec3(0), ivec2(0), ivec2(0)),
                                                           GeometryFragment(0, vec3(0), ivec2(0), ivec2(0)),
                                                           GeometryFragment(0, vec3(0), ivec2(0), ivec2(0)),
                                                           GeometryFragment(0, vec3(0), ivec2(0), ivec2(0)),
                                                           GeometryFragment(0, vec3(0), ivec2(0), ivec2(0)),
                                                           GeometryFragment(0, vec3(0), ivec2(0), ivec2(0)),
                                                           GeometryFragment(0, vec3(0), ivec2(0), ivec2(0)),
                                                           GeometryFragment(0, vec3(0), ivec2(0), ivec2(0)),
                                                           GeometryFragment(0, vec3(0), ivec2(0), ivec2(0)));
    int curNodeID;
    uint listID = 0;
    float cutoffDepth = 0;
    while (true) {
        GeometryFragment selectFrag = GeometryFragment(2, vec3(0), ivec2(0), ivec2(0));
        curNodeID = geometryHeaderNodes[frag_header_index];
        while (curNodeID != -1) {
            GeometryFragmentNode curNode = geometryFragmentNodes[curNodeID];

            float curDepth = curNode.fragment.depth;
            if (curDepth > cutoffDepth && curDepth < opaqueDepth && curDepth < selectFrag.depth)
                selectFrag = curNode.fragment;

            curNodeID = curNode.nextID;
        }
        if (selectFrag.depth != 2) {
            fragmentList[listID++] = selectFrag;
            cutoffDepth = selectFrag.depth;
            if (listID >= MAX_GEOMETRY_TRANSPARENCY_LAYERS)
                break;
        } else 
            break;
    }

    //put them into the linked list in the opposite of the order we sorted them into.
    //They will be reversed here again later when surfaces are rendered.
    curNodeID = geometryHeaderNodes[frag_header_index];
    if (listID == 0)
        geometryHeaderNodes[frag_header_index] = -1;
    else {
        uint listLength = listID;
        listID = 0;
        while (curNodeID != -1) {
            GeometryFragmentNode curNode = geometryFragmentNodes[curNodeID];
            curNode.fragment = fragmentList[listID++];

            if (listID == listLength)
                curNode.nextID = -1;

            geometryFragmentNodes[curNodeID] = curNode;

            curNodeID = curNode.nextID;
        }
    }
}