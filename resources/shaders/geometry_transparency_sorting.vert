#version 420

#include <lib/ubo.glsl>

in vec2 in_pos;
in vec2 in_tex_coord;

out vec2 v_tex_coord;

void main() {
    v_tex_coord = in_tex_coord;

    gl_Position = vec4(mat3(u_screen_matrix) * vec3(in_pos, 0), 1.0);
}