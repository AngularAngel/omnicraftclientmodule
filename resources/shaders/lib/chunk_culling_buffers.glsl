
#ifndef LIB_CHUNK_CULLING_BUFFER
#define LIB_CHUNK_CULLING_BUFFER

#include <position_functions.glsl>

#define UNKNOWN_IN_FRUSTUM 0
#define NOT_IN_FRUSTUM     1
#define IN_FRUSTUM         2

#define UNKNOWN_VISIBLE 0
#define NOT_VISIBLE     1
#define VISIBLE         2

struct BoundingBox {
    float startx;
    float starty;
    float startz;
    float endx;
    float endy;
    float endz;
};

struct IndirectData {
    uint count;
    uint firstIndex;
    uint baseVertex;
};

struct ChunkCullingData {
    BoundingBox boundingBox;
    IndirectData indirectData;
    int temporalCulling[64];
};

struct DrawElementsIndirectCommand {
    uint count;
    uint instanceCount;
    uint firstIndex;
    uint baseVertex;
    uint baseInstance;
};

layout(std430, binding = 4) restrict buffer chunkCullingSSBO {
    vec4 prevFrustums[64][6];
    ChunkCullingData chunkCullingData[];
};

layout(binding = 2, offset = 0) uniform atomic_uint indirectDrawCounter;

layout(std430, binding = 5) writeonly restrict buffer indirectDrawBuffer {
    DrawElementsIndirectCommand indirectDrawCommands[];
};

uniform int u_temporal_index;

void updateTemporalCohesion(vec3 block_pos) {
    ivec3 voxel_coord = ivec3(floor(block_pos));

    int chunkIndex = getChunkIndex(voxel_coord >> CHUNK_BITSHIFT);

    chunkCullingData[chunkIndex].temporalCulling[u_temporal_index] = (IN_FRUSTUM << 2) + VISIBLE;
}

#endif