
#ifndef LIB_GEOMETRY_FRAGMENTS
#define LIB_GEOMETRY_FRAGMENTS

#include <random.glsl>
#include <transparency_buffers.glsl>
#include <geometry_outputs.glsl>
#include <definitions.glsl> 

GeometryFragment getGeometryFragment(float depth, int block_id, int side_id, int transparent) {
    ivec2 texel_position = ivec2(floor(v_tex_coord * u_texels_per_block));
    GeometryFragment fragment = GeometryFragment(depth, v_normal, ivec2(block_id, side_id), texel_position);

    if (transparent == 1) {
        int nodeID = int(atomicCounterIncrement(geometryNodeCounter));
        if (nodeID < u_screen_size.x * u_screen_size.y * MAX_GEOMETRY_TRANSPARENCY_LAYERS) {
            int frag_header_index = int(gl_FragCoord.x) + int(gl_FragCoord.y) * u_screen_size.x;
            GeometryFragmentNode fragNode = GeometryFragmentNode(fragment, atomicExchange(geometryHeaderNodes[frag_header_index], nodeID));
            geometryFragmentNodes[nodeID] = fragNode;
        }
    } else {
        gl_FragDepth = fragment.depth;
        out_normal = fragment.normal * 0.5 + 0.5;
        out_texture_info = ivec4(fragment.textureIDs, fragment.texelCoords);
    }
    return fragment;
}
#endif