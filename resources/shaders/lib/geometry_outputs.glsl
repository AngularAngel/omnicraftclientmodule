
#ifndef LIB_GEOMETRY_OUTPUTS
#define LIB_GEOMETRY_OUTPUTS

out vec3 out_normal;
out ivec4 out_texture_info;
#endif