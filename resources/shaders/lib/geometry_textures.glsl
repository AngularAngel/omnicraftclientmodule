
#ifndef LIB_GEOMETRY_TEXTURES
#define LIB_GEOMETRY_TEXTURES

uniform sampler2D u_depth_tex;
uniform sampler2D u_normal_tex;
uniform isampler2D u_texture_info_tex;
#endif