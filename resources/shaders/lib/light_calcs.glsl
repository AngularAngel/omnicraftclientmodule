
#include <light_constants.glsl>

#ifndef LIB_LIGHT_CALCS
#define LIB_LIGHT_CALCS

//Lambertian reflectance.
vec3 lambRef(vec4 color, float cosang) {
    float diffuse_factor = max(cosang, 0.0)/PI;
    return color.rgb*diffuse_factor*color.a;
}

//Energy-conserving Blinn-Phong.
vec3 ecBlinnPhong(vec3 color, vec3 normal, vec3 material, vec3 halfway_dir, float spec_alb_factor) {
    float spec_exp = pow(1.0 - material.b, 3.0)*2044.0 + 4.0;
    float specular_factor = pow(clamp(dot(normal, halfway_dir), 0.0, 1.0), spec_exp)*(spec_exp + 8.0)/(8.0*PI);
    return vec3(specular_factor)*mix(color, vec3(1.0), spec_alb_factor);
}

//Total reflection!
vec3 getReflection(vec4 color, vec3 normal, vec3 material, vec3 light_direction, vec3 texel_world_dir, float cosang) {

    vec3 view_dir = normalize(-texel_world_dir);

    vec3 halfway_dir = normalize(light_direction + view_dir);

    float fresnel = material.g*(R0 + (1.0 - R0)*pow(clamp(1.0 - dot(normal, view_dir), 0.0, 1.0), 5.0)); //Schlick's approximation.
    float fresnel_metallic = mix(fresnel, 1.0, material.r);

    //If not metallic, specular color should always be white. If fully metallic,
    //specular color should blend from color to white according to the fresnel.
    float spec_alb_factor = mix(1.0, fresnel, material.r);

    //Lambertian reflectance.
    vec3 diffuse = lambRef(color, cosang);

    //Energy-conserving Blinn-Phong.
    vec3 specular = ecBlinnPhong(color.rgb, normal, material, halfway_dir, spec_alb_factor);

    return mix(diffuse, specular, fresnel_metallic);
}

vec3 getLighting(vec3 irradiance, float shadow, vec3 reflection) {
    return irradiance * shadow * reflection;
}

#endif