
#ifndef LIB_LIGHT_CONSTANTS
#define LIB_LIGHT_CONSTANTS

const float PI = 3.14159265358979323;
const float BASE_IOR = 2.3;
const float R0 = pow((1.0 - BASE_IOR)/(1.0 + BASE_IOR), 2.0);
const float SHADOW_BIAS = exp2(-23);
const float SHADOW_SLOPE_BIAS = exp2(-22);

#endif