#ifndef LIB_POSITION_FUNCTIONS
#define LIB_POSITION_FUNCTIONS

#include <ubo.glsl> 

vec3 getBlockPos(vec3 pos) {
    return pos / REAL_BLOCK_EDGE_LENGTH;
}

vec3 getContinuousTexelPos(vec3 block_pos) {
    return block_pos * u_texels_per_block;
}

vec3 getTexelPos(vec3 block_pos, vec3 normal) {
    return floor(getContinuousTexelPos(block_pos) - normal * 0.5);
}

int getVoxelIndex(ivec3 voxel_coord) {
    return voxel_coord.x * CHUNK_EDGE_LENGTH * CHUNK_EDGE_LENGTH + voxel_coord.y * CHUNK_EDGE_LENGTH + voxel_coord.z;
}

int getChunkIndex(ivec3 chunk_coord) {
    chunk_coord = chunk_coord & LOADED_CHUNKS_BITMASK;
    
    return chunk_coord.x * LOADED_CHUNKS_SIDE_LENGTH * LOADED_CHUNKS_SIDE_LENGTH + chunk_coord.y * LOADED_CHUNKS_SIDE_LENGTH + chunk_coord.z;
}

vec3 getWorldPos(float depth, vec3 world_dir) {
    vec3 relative_pos = world_dir * depth * u_z_far;
    return u_camera_pos.xyz + relative_pos;
}

#define MARGIN 0.01

bool containsFrag(vec3 chunk_pos) {
    return (chunk_pos.x >= -MARGIN && chunk_pos.x <= u_chunk_block_length + MARGIN &&
            chunk_pos.y >= -MARGIN && chunk_pos.y <= u_chunk_block_length + MARGIN &&
            chunk_pos.z >= -MARGIN && chunk_pos.z <= u_chunk_block_length + MARGIN);
}

#endif