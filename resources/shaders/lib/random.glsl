
#ifndef LIB_RANDOM
#define LIB_RANDOM

float randomV3ToF (vec3 st) {
    return fract(sin(dot(st.xyz,
                         vec3(234.66, 32.4, 564.7)))
                 * 95.234567);
}

// 2D Random
float randomF (float seed) {
    return fract(sin(983.234567 * seed));
}

vec2 randomFtoV2 (float seed) {
    return vec2(randomF(seed * 0.87), randomF(seed * 1.43));
}

ivec3 randomIV3toIV3(ivec3 v) {
    ivec3 fac = ivec3(11248723, 105436839, 45399083);
    int seed = v.x*fac.x ^ v.y*fac.y ^ v.z*fac.z;
    v = seed*fac;
    return v;
}

float fand(float a, float b) {
    return a + b - a * b;
}

float startingInterp(float interpolator){
    return interpolator * interpolator;
}

float endingInterp(float interpolator){
    return 1 - startingInterp(1 - interpolator);
}

float smoothInterp(float interpolator){
    float start = startingInterp(interpolator);
    float end = endingInterp(interpolator);
    return mix(start, end, interpolator);
}

//valueNoise!
float valueNoise(vec2 st, float seed) {
    vec2 i = floor(st);
    vec2 f = fract(st);

    float bottomLeft = randomF(seed);
    float bottomRight = randomF(seed);
    float topLeft = randomF(seed);
    float topRight = randomF(seed);
    
    // Smooth Interpolation
    float interpolatorX = smoothInterp(f.x);
    float interpolatorY = smoothInterp(f.y);

    float upperCells = mix(topLeft, topRight, interpolatorX);
    float lowerCells = mix(bottomLeft, bottomRight, interpolatorX);

    float noise = mix(upperCells, lowerCells, interpolatorY);
    return noise;
}

int xorshiftRandom(int x) {
	x ^= x << 13;
	x ^= x >> 17;
	x ^= x << 5;
	return x;
}

float simplex(vec3 v){
	const vec2 C = vec2(1.0/6.0, 1.0/3.0);

	// First corner
	vec3 i = floor(v + dot(v, C.yyy));
	vec3 x0 = v - i + dot(i, C.xxx);

	// Other corners
	vec3 g = step(x0.yzx, x0.xyz);
	vec3 l = 1.0 - g;
	vec3 i1 = min(g.xyz, l.zxy);
	vec3 i2 = max(g.xyz, l.zxy);

	// x0 = x0 - 0. + 0.0 * C
	vec3 x1 = x0 - i1 + 1.0*C.xxx;
	vec3 x2 = x0 - i2 + 2.0*C.xxx;
	vec3 x3 = x0 - 1. + 3.0*C.xxx;

	// Get gradients:
	ivec3 rand = randomIV3toIV3(ivec3(i));
	vec3 p0 = vec3(rand);
	
	rand = randomIV3toIV3(ivec3(i + i1));
	vec3 p1 = vec3(rand);
	
	rand = randomIV3toIV3(ivec3(i + i2));
	vec3 p2 = vec3(rand);
	
	rand = randomIV3toIV3(ivec3(i + 1));
	vec3 p3 = vec3(rand);

	// Mix final noise value
	vec4 m = max(0.6 - vec4(dot(x0,x0), dot(x1,x1), dot(x2,x2), dot(x3,x3)), 0.0);
	m = m*m;
	return (42.0/(1 << 31))*dot(m*m, vec4(dot(p0,x0), dot(p1,x1), dot(p2,x2), dot(p3,x3)));
}

vec3 tripleSimplex(vec3 v){
	const vec2 C = vec2(1.0/6.0, 1.0/3.0);

	// First corner
	vec3 i = floor(v + dot(v, C.yyy));
	vec3 x0 = v - i + dot(i, C.xxx);

	// Other corners
	vec3 g = step(x0.yzx, x0.xyz);
	vec3 l = 1.0 - g;
	vec3 i1 = min(g.xyz, l.zxy);
	vec3 i2 = max(g.xyz, l.zxy);

	// x0 = x0 - 0. + 0.0 * C
	vec3 x1 = x0 - i1 + 1.0*C.xxx;
	vec3 x2 = x0 - i2 + 2.0*C.xxx;
	vec3 x3 = x0 - 1. + 3.0*C.xxx;


	vec4 m = max(0.6 - vec4(dot(x0,x0), dot(x1,x1), dot(x2,x2), dot(x3,x3)), 0.0);
	m = m*m;
	m = m*m;

	vec3 result = vec3(0);

	{
		// Get gradients:
		ivec3 rand = randomIV3toIV3(ivec3(i));
		vec3 p0 = vec3(rand);
		
		rand = randomIV3toIV3(ivec3(i + i1));
		vec3 p1 = vec3(rand);
		
		rand = randomIV3toIV3(ivec3(i + i2));
		vec3 p2 = vec3(rand);
		
		rand = randomIV3toIV3(ivec3(i + 1));
		vec3 p3 = vec3(rand);

		// Mix final noise value
		result.x = (42.0/(1 << 31))*dot(m, vec4(dot(p0,x0), dot(p1,x1), dot(p2,x2), dot(p3,x3)));
	}

	i += 5642.3333333;
	
	{
		// Get gradients:
		ivec3 rand = randomIV3toIV3(ivec3(i));
		vec3 p0 = vec3(rand);
		
		rand = randomIV3toIV3(ivec3(i + i1));
		vec3 p1 = vec3(rand);
		
		rand = randomIV3toIV3(ivec3(i + i2));
		vec3 p2 = vec3(rand);
		
		rand = randomIV3toIV3(ivec3(i + 1));
		vec3 p3 = vec3(rand);

		// Mix final noise value
		result.y = (42.0/(1 << 31))*dot(m, vec4(dot(p0,x0), dot(p1,x1), dot(p2,x2), dot(p3,x3)));
	}

	i -= 11202.6666666;

	{
		// Get gradients:
		ivec3 rand = randomIV3toIV3(ivec3(i));
		vec3 p0 = vec3(rand);
		
		rand = randomIV3toIV3(ivec3(i + i1));
		vec3 p1 = vec3(rand);
		
		rand = randomIV3toIV3(ivec3(i + i2));
		vec3 p2 = vec3(rand);
		
		rand = randomIV3toIV3(ivec3(i + 1));
		vec3 p3 = vec3(rand);

		// Mix final noise value
		result.z = (42.0/(1 << 31))*dot(m, vec4(dot(p0,x0), dot(p1,x1), dot(p2,x2), dot(p3,x3)));
	}

	return result;
}

#endif