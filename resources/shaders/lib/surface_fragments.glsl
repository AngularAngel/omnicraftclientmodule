#ifndef LIB_SURFACE_FRAGMENTS
#define LIB_SURFACE_FRAGMENTS

#include <random.glsl>
#include <transparency_buffers.glsl>
#include <surface_outputs.glsl>
#include <definitions.glsl>
#include <chunk_culling_buffers.glsl>
#include <position_functions.glsl>

uniform sampler2DRect u_texture_palette;

uniform isampler3D u_side_tex1;
uniform isampler3D u_side_tex2;

uniform sampler3D u_ambient_light_tex1;
uniform sampler3D u_ambient_light_tex2;

struct ChunkBlockAndSideData {
    int blocks[BLOCKS_PER_CHUNK];
    int topSides[BLOCKS_PER_CHUNK];
    int bottomSides[BLOCKS_PER_CHUNK];
    int leftSides[BLOCKS_PER_CHUNK];
    int rightSides[BLOCKS_PER_CHUNK];
    int frontSides[BLOCKS_PER_CHUNK];
    int backSides[BLOCKS_PER_CHUNK];
};

layout(std430, binding = 6) restrict buffer chunkBlockSSBO {
    ChunkBlockAndSideData chunkBlockAndSideData[];
};

vec4 getTexturePaletteColor(int index, int palette_length, int texture_id) {
    return texelFetch(u_texture_palette, ivec2(index, palette_length - texture_id));
}

int getBlock(int voxelIndex, int chunkIndex) {

    return chunkBlockAndSideData[chunkIndex].blocks[voxelIndex];
}

int getSide(int side, int voxelIndex, int chunkIndex) {

    switch(side) {
        case 0:
            return chunkBlockAndSideData[chunkIndex].topSides[voxelIndex];
        case 1:
            return chunkBlockAndSideData[chunkIndex].bottomSides[voxelIndex];
        case 2:
            return chunkBlockAndSideData[chunkIndex].leftSides[voxelIndex];
        case 3:
            return chunkBlockAndSideData[chunkIndex].rightSides[voxelIndex];
        case 4:
            return chunkBlockAndSideData[chunkIndex].frontSides[voxelIndex];
        case 5:
            return chunkBlockAndSideData[chunkIndex].backSides[voxelIndex];
    }
    
    return 0;
}

float getSeed(vec3 texel_pos) {
    return randomV3ToF(texel_pos);
}

int current_texture_index = 0;

float noise(ivec3 voxelPos, float randomness) {
    return (vec3(randomIV3toIV3(voxelPos)) / (1 << 31)).x * randomness;
}

float multilayeredSimplex(int texture_id, int palette_length, float layerCount, float brightnessOffset, int numColors, vec3 texel_pos) {
    int currentLayer = 1; //No width for this pattern, we do it pixel by pixel.

    vec3 simplex1Wavelength = getTexturePaletteColor(current_texture_index, palette_length, texture_id).rgb;
    float simplex1Weight = getTexturePaletteColor(current_texture_index, palette_length, texture_id).a;
    current_texture_index += 1;

    vec3 simplexDomain = tripleSimplex(texel_pos / simplex1Wavelength);

    float brightness = simplexDomain.x * simplex1Weight + brightnessOffset;

    while (currentLayer < floor(layerCount)) {
        vec3 layerWavelength = getTexturePaletteColor(current_texture_index, palette_length, texture_id).rgb;
        float layerWeight = getTexturePaletteColor(current_texture_index, palette_length, texture_id).a;
        current_texture_index += 1;
        vec3 layerDomainWarp = getTexturePaletteColor(current_texture_index, palette_length, texture_id).rgb;
        current_texture_index += 1;

        brightness += simplex(texel_pos / layerWavelength + simplexDomain * layerDomainWarp) * layerWeight;
        currentLayer += 1;
    }

    if (fract(layerCount) > 0.4) {
        brightness += noise(ivec3(texel_pos), getTexturePaletteColor(current_texture_index, palette_length, texture_id).r);
        current_texture_index += 1;
    }
    return brightness;
}

vec4 getColor(int texture_id, int palette_length, float offset, int numColors) {
    int colorIndex = min(numColors - 1, max(0, int(floor(offset * numColors))));
    return getTexturePaletteColor(current_texture_index + colorIndex, palette_length, texture_id);
}

vec4 getTexturePatternVec(ivec2 texel_position, int texture_id, int palette_length, vec3 texel_pos) {
    float seed = getSeed(texel_pos);
    vec4 pattern_params = getTexturePaletteColor(current_texture_index, palette_length, texture_id);

    int pattern_type = int(pattern_params.r);
    int pattern_width = int(pattern_params.g);
    int pattern_height = int(pattern_params.b);
    int numColors = int(pattern_params.a);

    current_texture_index += 1;

    vec4 result = vec4(0);

    switch(pattern_type) {
        case 0: { //Solid
            result = getTexturePaletteColor(current_texture_index, palette_length, texture_id);
            break;
        }
        case 1: { //Random from list
            result = getColor(texture_id, palette_length, randomF(seed), numColors);
            break;
        }
        case 2: { //Checkerboard
            int result_index = current_texture_index + ((texel_position.x / pattern_width) + (texel_position.y / pattern_height)) % numColors;
            result = getTexturePaletteColor(result_index, palette_length, texture_id);
            break;
        }
        case 3: { //Value noise
            float n = valueNoise(texel_position / 2, getSeed(texel_pos));
            n = smoothInterp(n);
            n = fand(n, pattern_params.g);
            result = getTexturePaletteColor(current_texture_index, palette_length, texture_id) * n;
            result.a = 1;
            break;
        }
        case 4: { //Random within values
            float variation = pattern_params.a;
            result = vec4(randomF(seed * 0.23) * variation * 2 - variation,
                                randomF(seed) * variation * 2 - variation,
                                randomF(seed * 0.564) * variation * 2 - variation, 0);
            current_texture_index -= numColors; //We need to adjust this because we used it for something else.
            break;
        }
        case 5: { //Multilayered Simplex
            float brightness = multilayeredSimplex(texture_id, palette_length, pattern_params.g, pattern_params.b, numColors, texel_pos);
            result = getColor(texture_id, palette_length, brightness, numColors);
            break;
        }
        case 6: { //Multilayered Simplex W/ Worley
            float brightness = multilayeredSimplex(texture_id, palette_length, pattern_params.g, pattern_params.b, numColors, texel_pos);
            result = getColor(texture_id, palette_length, brightness, numColors);
            break;
        }
        case 7: { //Transparent Value noise
            float n = valueNoise(texel_position / 2, getSeed(texel_pos));
            n = smoothInterp(n);
            n = fand(n, pattern_params.g);
            result = getTexturePaletteColor(current_texture_index, palette_length, texture_id);
            result.a = n;
            break;
        }
        case 8: { //Threshold Value noise
            float n = valueNoise(texel_position / 2, getSeed(texel_pos));
            n = smoothInterp(n);
            if (n >= pattern_params.g)
                result = getTexturePaletteColor(current_texture_index, palette_length, texture_id);
            else
                result = vec4(-1);
            break;
        }
        default:
            result = vec4(1, 0, 0, 1); //getTexturePaletteColor(current_texture_index, palette_length, texture_id);
            break;
    }

    current_texture_index += numColors;

    return result;
}

bool hasBits(int bitfield, int bits) {
    return (bitfield & bits) == bits;
}

vec4 modifyVec(vec4 current, vec4 mod, vec4 application_params) {
    int application_type = int(application_params.g);

    switch(application_type) {
        case 0: //Set!
            return mod;
        case 0x01: //Add!
            return current + mod;
        case 0x02: //Sub!
            return current - mod;
        case 0x03: //Mult!
            return current * mod;
        case 0x04: //Div!
            return current / mod;
        case 0x05: //Mix!
            float n = mod.a;
            mod.a = 1;
            return mix(current, mod, n);
        case 0x06: //Threshold Set!
            vec4 result = current;
            if (mod.r >= 0)
                result.r = mod.r;
            if (mod.g >= 0)
                result.g = mod.g;
            if (mod.b >= 0)
                result.b = mod.b;
            if (mod.a >= 0)
                result.a = mod.a;
            return result;
    }
}

SurfaceFragment applyVec(SurfaceFragment surfaceFragment, vec4 vec, vec4 application_params) {
    int application_target = int(application_params.r);

    if (hasBits(application_target, 0x80))
        surfaceFragment.color = modifyVec(surfaceFragment.color, vec, application_params);
    if (hasBits(application_target, 0x40))
        surfaceFragment.normal = modifyVec(vec4(surfaceFragment.normal, 0), vec, application_params).rgb;
    if (hasBits(application_target, 0x20))
        surfaceFragment.material.r = modifyVec(vec4(surfaceFragment.material.r), vec, application_params).r;
    if (hasBits(application_target, 0x10))
        surfaceFragment.material.g = modifyVec(vec4(surfaceFragment.material.g), vec, application_params).r;
    if (hasBits(application_target, 0x08))
        surfaceFragment.material.b = modifyVec(vec4(surfaceFragment.material.b), vec, application_params).r;
    if (hasBits(application_target, 0x04))
        surfaceFragment.light = modifyVec(vec4(surfaceFragment.light, 0), vec, application_params).rgb;
    return surfaceFragment;
}

SurfaceFragment applyTexturePattern(SurfaceFragment surfaceFragment, ivec2 texel_position, int texture_id, int palette_length, vec3 texel_pos) {
    vec4 application_params = getTexturePaletteColor(current_texture_index, palette_length, texture_id);
    current_texture_index += 1;

    vec4 vec = getTexturePatternVec(texel_position, texture_id, palette_length, texel_pos);
    
    surfaceFragment = applyVec(surfaceFragment, vec, application_params);

    return surfaceFragment;
}

SurfaceFragment applyTexturePatterns(SurfaceFragment surfaceFragment, ivec2 texel_position, int texture_id, int palette_length, vec3 texel_pos) {
    bool continuing = true;
    int counter = 0;
    while(continuing && counter < 30) {
        vec4 application_params = getTexturePaletteColor(current_texture_index, palette_length, texture_id);
    
        int application_type = int(application_params.r);

        surfaceFragment = applyTexturePattern(surfaceFragment, texel_position, texture_id, palette_length, texel_pos);

        continuing = !hasBits(application_type, 0x01);
        counter += 1;
    }

    return surfaceFragment;
}

vec3 getSkyLight(int light_id, vec3 sky_dir, float displayMultiplier) {
    if (displayMultiplier <= 0) 
        return vec3(0);
    float directionMultiplier = max(dot(u_light_directions[light_id].xyz, sky_dir), 0.0);
    return u_light_colors[light_id].rgb * directionMultiplier * displayMultiplier / 5;
}

vec3 getSkyLight(int light_id, vec3 sky_dir, float displayMultiplier, float cutoff) {
    if (displayMultiplier <= 0) 
        return vec3(0);
    float directionMultiplier = max(dot(u_light_directions[light_id].xyz, sky_dir), 0.0);
    if (directionMultiplier < cutoff)
        directionMultiplier = 0;
    return u_light_colors[light_id].rgb * directionMultiplier * displayMultiplier / 5;
}

vec3 getAmbientLighting(vec3 block_pos, vec3 normal) {
    block_pos += normal * 0.25;

    vec3 chunk_tex_pos = (block_pos) / textureSize(u_ambient_light_tex1, 0);

    vec4 lighting1 = texture(u_ambient_light_tex1, chunk_tex_pos);
    vec4 lighting2 = texture(u_ambient_light_tex2, chunk_tex_pos);

    vec3 ambient_light = lighting1.rgb / 2;

    for (int i = 0; i < DIRECTIONAL_LIGHT_ARRAY_LENGTH; i++) {
        ambient_light += getSkyLight(i, vec3(0, 1, 0), lighting1.a);
        ambient_light += getSkyLight(i, normalize(vec3(0, 1, 1)), lighting2.r);
        ambient_light += getSkyLight(i, normalize(vec3(1, 1, 0)), lighting2.g);
        ambient_light += getSkyLight(i, normalize(vec3(0, 1, -1)), lighting2.b);
        ambient_light += getSkyLight(i, normalize(vec3(-1, 1, 0)), lighting2.a);
    }

    return ambient_light;
}

vec3 getChunkPos(vec3 blockPos) {
    return blockPos - (ivec3(floor(blockPos)) & (CHUNK_BITMASK));
}

GeometryFragment getTextures(vec3 block_pos, GeometryFragment geometryFragment) {

    int chunkIndex = -1;

    if (geometryFragment.textureIDs.x == -1) {

        ivec3 voxel_coord = ivec3(floor(block_pos - geometryFragment.normal * 0.5));

        int voxelIndex = getVoxelIndex(voxel_coord & VOXEL_BITMASK);
        chunkIndex = getChunkIndex(voxel_coord >> CHUNK_BITSHIFT);
        
        geometryFragment.textureIDs.x = getBlock(voxelIndex, chunkIndex);

        if (geometryFragment.normal.y > 0.95)
            geometryFragment.textureIDs.y = getSide(0, voxelIndex, chunkIndex);
        else if (geometryFragment.normal.y < -0.95)
            geometryFragment.textureIDs.y = getSide(1, voxelIndex, chunkIndex);
        else if (geometryFragment.normal.x < -0.95)
            geometryFragment.textureIDs.y = getSide(2, voxelIndex, chunkIndex);
        else if (geometryFragment.normal.x > 0.95)
            geometryFragment.textureIDs.y = getSide(3, voxelIndex, chunkIndex);
        else if (geometryFragment.normal.z > 0.95)
            geometryFragment.textureIDs.y = getSide(4, voxelIndex, chunkIndex);
        else
            geometryFragment.textureIDs.y = getSide(5, voxelIndex, chunkIndex);
    }
    
    if (chunkIndex > -1)
        chunkCullingData[chunkIndex].temporalCulling[0] = (IN_FRUSTUM << 2) + VISIBLE;

    return geometryFragment;
}

void returnSurfaceFragment(SurfaceFragment surfaceFragment) {
    //surfaceFragment.color.rgb = vec3(0);
    //surfaceFragment.light.rgb = vec3(0);
    //if (surfaceFragment.color.a > 0)
    //    surfaceFragment.light.rgb = vec3(surfaceFragment.depth * 10);
    SurfaceFragmentNode fragNode = SurfaceFragmentNode(surfaceFragment, -1);
    if (surfaceFragment.color.a < 1) {
        int nodeID = int(atomicCounterIncrement(surfaceNodeCounter));
        if (nodeID < u_screen_size.x * u_screen_size.y * MAX_SURFACE_TRANSPARENCY_LAYERS) {
            int frag_header_index = int(gl_FragCoord.x) + int(gl_FragCoord.y) * u_screen_size.x;
            fragNode.nextID = atomicExchange(surfaceHeaderNodes[frag_header_index], nodeID);
            surfaceFragmentNodes[nodeID] = fragNode;
        }
    } else {
        gl_FragDepth = surfaceFragment.depth;
        out_color = surfaceFragment.color.rgb;
        out_normal = surfaceFragment.normal * 0.5 + 0.5;
        out_material = surfaceFragment.material;
        out_light = surfaceFragment.light;
    }
}

SurfaceFragment colorSurfaceFragment(vec3 block_pos, GeometryFragment geometryFragment, vec3 texel_pos, float thickness) {
    SurfaceFragment surfaceFragment = SurfaceFragment(geometryFragment.depth, vec4(0), geometryFragment.normal, vec3(0), vec3(0));

    if (geometryFragment.textureIDs.x == -2)
        return surfaceFragment;

    ivec2 texel_position = geometryFragment.texelCoords;
    
    ivec2 texture_size = textureSize(u_texture_palette);
    
    int palette_size = texture_size.x - 1;
    int palette_length = texture_size.y - 1;

    surfaceFragment = applyTexturePatterns(surfaceFragment, texel_position, geometryFragment.textureIDs.x, palette_length, texel_pos);

    //color = brickTexture(ivec3(floor(2*v_pos*u_texels_per_block)), voxel_coord);
    //color = vec3(0);

    current_texture_index = 0;
    surfaceFragment = applyTexturePatterns(surfaceFragment, texel_position, geometryFragment.textureIDs.y, palette_length, texel_pos);
    
    if (surfaceFragment.color.a < 1) {
        surfaceFragment.color.a = surfaceFragment.color.a*(thickness + 0.3);
        if (surfaceFragment.color.a >= 1)
            surfaceFragment.color.a = 0.99;
    }
    
    surfaceFragment.normal = normalize(surfaceFragment.normal);

    surfaceFragment.light += (surfaceFragment.color.rgb * getAmbientLighting(block_pos, surfaceFragment.normal) * surfaceFragment.color.a);

    //emission = vec3(0);
    
    returnSurfaceFragment(surfaceFragment);

    return surfaceFragment;
}

SurfaceFragment colorSurfaceFragment(vec3 block_pos, GeometryFragment geometryFragment, vec3 texel_pos) {
    return colorSurfaceFragment(block_pos, geometryFragment, texel_pos, 1);
}

struct RayMarchResult {
	bool hitAThing;
	int normal;
	int palette;
};

int applyRaycastingConditionInner(vec3 texel_pos, GeometryFragment geometryFragment, int palette_length, vec3 stepDirection) {
    vec4 raycasting_params = getTexturePaletteColor(current_texture_index, palette_length, geometryFragment.textureIDs.x);

    ivec2 texel_position = geometryFragment.texelCoords;

    int return_condition = int(raycasting_params.r);

    switch(return_condition) {
        case 0: //flat block, return immediately.
            return int(raycasting_params.a);

        case 1: //internal cube condition.
            texel_pos.x = int(texel_pos.x) % int(ceil(u_texels_per_block));
            texel_pos.y = int(texel_pos.y) % int(ceil(u_texels_per_block));
            texel_pos.z = int(texel_pos.z) % int(ceil(u_texels_per_block));

            vec4 offset_params = getTexturePaletteColor(current_texture_index + 1, palette_length, geometryFragment.textureIDs.x);

            current_texture_index += 2;
            
            texel_pos.xyz += offset_params.xyz;

            if (texel_pos.x < 0)
                texel_pos.x += int(ceil(u_texels_per_block));
            if (texel_pos.y < 0)
                texel_pos.y += int(ceil(u_texels_per_block));
            if (texel_pos.z < 0)
                texel_pos.z += int(ceil(u_texels_per_block));
            
            float cubeXSideProtrusion = float(raycasting_params.g) / 2.0;
            float cubeYSideProtrusion = float(raycasting_params.b) / 2.0;
            float cubeZSideProtrusion = float(raycasting_params.a) / 2.0;

            if (texel_pos.x < (u_texels_per_block / 2) - cubeXSideProtrusion || texel_pos.x >= (u_texels_per_block / 2) + cubeXSideProtrusion)
                return -1;
            if (texel_pos.y < (u_texels_per_block / 2) - cubeYSideProtrusion || texel_pos.y >= (u_texels_per_block / 2) + cubeYSideProtrusion)
                return -1;
            if (texel_pos.z < (u_texels_per_block / 2) - cubeZSideProtrusion || texel_pos.z >= (u_texels_per_block / 2) + cubeZSideProtrusion)
                return -1;

            return int(offset_params.a);

        case 2: { //Value noise condition.
            current_texture_index += 1;

            texel_pos += raycasting_params.g;
            float n = simplex(texel_pos / 5);
            n = smoothInterp(n);
            return n >= raycasting_params.b ? int(raycasting_params.a) : -1;
        }

        case 3: //Multiple conditions - hopefully we never hit this here.
            break;

        case 4: { //1 dimensional value noise
            current_texture_index += 2;
            if (stepDirection != normalize(raycasting_params.gba))
                return -1;
            raycasting_params = getTexturePaletteColor(current_texture_index - 1, palette_length, geometryFragment.textureIDs.x);
            
            texel_pos += raycasting_params.g;
            float n = simplex(texel_pos / 5);
            n = smoothInterp(n);
            return n >= raycasting_params.b ? int(raycasting_params.a) : -1;
        }
    }
    return -1;
}



int applyRaycastingConditionOuter(vec3 texel_pos, GeometryFragment geometryFragment, int palette_length, vec3 stepDirection) {
    current_texture_index = 0;
    vec4 raycasting_params = getTexturePaletteColor(current_texture_index, palette_length, geometryFragment.textureIDs.x);

    ivec2 texel_position = geometryFragment.texelCoords;

    int return_condition = int(raycasting_params.r);

    switch(return_condition) {
        case 3: //Multiple conditions!
            current_texture_index = 1;
            for (int i = 0; i < raycasting_params.g; i++) {
                int textureID = applyRaycastingConditionInner(texel_pos, geometryFragment, palette_length, stepDirection);
                
                if (textureID > -1)
                    return textureID;
            }
            break;

        default: return applyRaycastingConditionInner(texel_pos, geometryFragment, palette_length, stepDirection);
    }
    return -1;
}

SurfaceFragment getSurfaceFragment(float backDepth, GeometryFragment geometryFragment, vec3 world_dir) {

    ivec2 texture_size = textureSize(u_texture_palette);
    
    int palette_size = texture_size.x - 1;
    int palette_length = texture_size.y - 1;

    // Branchless implementation of "A Fast Voxel Traversal Algorithm for Ray Tracing"  http://www.cse.yorku.ca/~amana/research/grid.pdf
    vec3 world_pos = getWorldPos(geometryFragment.depth, world_dir);
    vec3 block_pos = getBlockPos(world_pos);
    vec3 rayDirection = normalize(world_dir);
    vec3 starting_pos = getContinuousTexelPos(block_pos) - rayDirection * 0.01; //Add some bias to make sure we draw the whole front of the block.
    vec3 starting_texel_pos = floor(starting_pos);
    vec3 texel_pos = starting_texel_pos;
    
    vec3 texelStep = sign(rayDirection); //sets all coordinates to -1 or 1, based on their sign.
    vec3 distanceStep = 1 / rayDirection;
    vec3 absDistanceStep = abs(distanceStep);

    vec3 lastDistBound1 = (starting_texel_pos - starting_pos) / rayDirection;
    vec3 lastDistBound2 = lastDistBound1 + distanceStep;

    vec3 lastDistancesOfVoxel = max(lastDistBound1, lastDistBound2) - absDistanceStep; //tMax;
    if (rayDirection.x == 0) lastDistancesOfVoxel.x = 1.0 / 0.0;
    if (rayDirection.y == 0) lastDistancesOfVoxel.y = 1.0 / 0.0;
    if (rayDirection.z == 0) lastDistancesOfVoxel.z = 1.0 / 0.0;

    float currentActualDistance = 0;

    float thickness = (backDepth * u_z_far - geometryFragment.depth * u_z_far) - 0.1; //Add some more bias to try and avoid drawing outside our block.
    
    SurfaceFragment surfaceFragment;

    int counter = 0, limit = 128;
    vec3 curStep;
    
    int textureID;

    do {
        
        vec3 nextDistancesOfVoxel = lastDistancesOfVoxel + absDistanceStep; //tNext
        float currentWorkingDistance = min(nextDistancesOfVoxel.x, min(nextDistancesOfVoxel.y, nextDistancesOfVoxel.z));

        vec3 stepDirection = floor((currentWorkingDistance - lastDistancesOfVoxel) / absDistanceStep + 0.00001);
        curStep = normalize(stepDirection * texelStep);
        if (counter > 0) {
            texel_pos += curStep;
            lastDistancesOfVoxel += stepDirection * absDistanceStep;
        }

        currentActualDistance = length(texel_pos - starting_texel_pos);

        if (currentActualDistance >= thickness * u_texels_per_block * 2) {
            surfaceFragment = SurfaceFragment(geometryFragment.depth, vec4(0, 0, 0, 0), geometryFragment.normal, vec3(0), vec3(0, 0, 0));
            returnSurfaceFragment(surfaceFragment);
            
            return surfaceFragment;
        }
        
        counter++;
        textureID = applyRaycastingConditionOuter(texel_pos, geometryFragment, palette_length, curStep);
    } while (textureID < 0 && counter < limit);

    geometryFragment.textureIDs.x = textureID;

    current_texture_index = 0;

    geometryFragment.depth += currentActualDistance / (u_texels_per_block * 2 * u_z_far);
    geometryFragment.normal = curStep;
    surfaceFragment = colorSurfaceFragment(block_pos, geometryFragment, texel_pos, thickness);
    returnSurfaceFragment(surfaceFragment);

    return surfaceFragment;
}

#endif