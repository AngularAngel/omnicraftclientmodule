
#ifndef LIB_SURFACE_OUTPUTS
#define LIB_SURFACE_OUTPUTS

out vec3 out_color;
out vec3 out_normal;
out vec3 out_material;
out vec3 out_light;
#endif