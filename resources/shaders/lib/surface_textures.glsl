
#ifndef LIB_SURFACE_TEXTURES
#define LIB_SURFACE_TEXTURES

uniform sampler2D u_depth_tex;
uniform sampler2D u_color_tex;
uniform sampler2D u_normal_tex;
uniform sampler2D u_material_tex;
uniform sampler2D u_light_tex;
#endif