

#ifndef LIB_TEXTURE_METHODS
#define LIB_TEXTURE_METHODS

bool isSideHalfBlock(ivec3 voxelPosition, ivec3 voxel_coord) {
    return (v_normal.y < 0.95 && v_normal.y > -0.95) ||
        (texelFetch(u_block_tex, voxel_coord + ivec3(-1, 0, 0), 0).r == 0 && (voxelPosition.x & 7) <= 4) ||
        (texelFetch(u_block_tex, voxel_coord + ivec3(1, 0, 0), 0).r == 0 && (voxelPosition.x & 7) >= 4) ||
        (texelFetch(u_block_tex, voxel_coord + ivec3(0, 0, -1), 0).r == 0 && (voxelPosition.z & 7) <= 4) ||
        (texelFetch(u_block_tex, voxel_coord + ivec3(0, 0, 1), 0).r == 0 && (voxelPosition.z & 7) >= 4) ||
        (texelFetch(u_block_tex, voxel_coord + ivec3(-1, 0, -1), 0).r == 0 && (voxelPosition.x & 7) <= 4 && (voxelPosition.z & 7) <= 4) ||
        (texelFetch(u_block_tex, voxel_coord + ivec3(1, 0, -1), 0).r == 0 && (voxelPosition.x & 7) >= 4 && (voxelPosition.z & 7) <= 4) ||
        (texelFetch(u_block_tex, voxel_coord + ivec3(-1, 0, 1), 0).r == 0 && (voxelPosition.x & 7) <= 4 && (voxelPosition.z & 7) >= 4) ||
        (texelFetch(u_block_tex, voxel_coord + ivec3(1, 0, 1), 0).r == 0 && (voxelPosition.x & 7) >= 4 && (voxelPosition.z & 7) >= 4);
}

vec3 sideAlternatingBrickTexture(ivec3 voxelPosition, ivec3 voxel_coord, int brickWidth, int brickHeight) {
    bool set_10 = texelFetch(u_block_tex, voxel_coord + ivec3(-1, 0, 0), 0).r == 0;
    bool set10 = texelFetch(u_block_tex, voxel_coord + ivec3(1, 0, 0), 0).r == 0;
    bool set0_1 = texelFetch(u_block_tex, voxel_coord + ivec3(0, 0, -1), 0).r == 0;
    bool set01 = texelFetch(u_block_tex, voxel_coord + ivec3(0, 0, 1), 0).r == 0;
    bool set_11 = texelFetch(u_block_tex, voxel_coord + ivec3(-1, 0, 1), 0).r == 0;
    bool set11 = texelFetch(u_block_tex, voxel_coord + ivec3(1, 0, 1), 0).r == 0;
    bool set_1_1 = texelFetch(u_block_tex, voxel_coord + ivec3(-1, 0, -1), 0).r == 0;
    bool set1_1 = texelFetch(u_block_tex, voxel_coord + ivec3(1, 0, -1), 0).r == 0;
    int conds = 0;
    conds += int((set_10 || set_1_1 || set_11) && (voxelPosition.x & 7) == 4);
    conds += int((set10 || set1_1 || set11) && (voxelPosition.x & 7) == 4);
    conds += int((set0_1 || set1_1 || set_1_1) && (voxelPosition.z & 7) == 4);
    conds += int((set01 || set11 || set_11) && (voxelPosition.z & 7) == 4);
    int conds2 = 0;
    conds2 += int((set_10) && (voxelPosition.x & 7) < 4);
    conds2 += int((set01) && (voxelPosition.x & 7) > 4);
    conds2 += int((set0_1) && (voxelPosition.z & 7) < 4);
    conds2 += int((set01) && (voxelPosition.z & 7) > 4);
    if((v_normal.y > 0.95 || v_normal.y < -0.95) && conds != 0 && conds2 == 0) {
        return vec3(0.8, 0.8, 0.8);
    }
    int layerY = voxelPosition.y & ~(brickHeight-1);
    if(layerY == voxelPosition.y && v_normal.y == 0) {
        return vec3(0.8, 0.8, 0.8);
    }
    int newX = voxelPosition.x - layerY + brickWidth/2;
    int layerX = newX & ~(brickWidth-1);
    if(layerX == newX && v_normal.x == 0) {
        if((voxelPosition.x & 7) == 0 && texelFetch(u_block_tex, voxel_coord - ivec3(1, 0, 0), 0).r == 0) {
            
        } else {
            return vec3(0.8, 0.8, 0.8);
        }
    }
    int newZ = voxelPosition.z - layerY;
    int layerZ = newZ & ~(brickWidth-1);
    if(layerZ == newZ && v_normal.z == 0) {
        if((voxelPosition.z & 7) == 0 && texelFetch(u_block_tex, voxel_coord - ivec3(0, 0, 1), 0).r == 0) {
            
        } else {
            return vec3(0.8, 0.8, 0.8);
        }
    }
    return vec3(0.7, 0.1, 0.0);
}

vec3 topAlternatingBrickTexture(ivec3 voxelPosition, int brickWidth, int brickHeight) {
    int layerZ = voxelPosition.z & ~(brickHeight-1);
    if(layerZ == voxelPosition.z && v_normal.z == 0) {
        return vec3(0.8, 0.8, 0.8);
    }
    int newX = voxelPosition.x - layerZ + brickWidth/2;
    int layerX = newX & ~(brickWidth-1);
    if(layerX == newX && v_normal.x == 0) {
        return vec3(0.8, 0.8, 0.8);
    }
    return vec3(0.7, 0.1, 0.0);
}

vec3 topDiagonalPattern(ivec3 voxelPosition, int brickWidth, int brickHeight) {
    {
        int layerZ = voxelPosition.z & ~(brickHeight-1);
        int newX = voxelPosition.x - layerZ + brickWidth/2;
        int layerX = newX & ~(brickWidth-1);
        if(layerX == newX && v_normal.x == 0) {
            return vec3(0.8, 0.8, 0.8);
        }
        if((newX & (brickWidth*2 - 1)) < brickWidth && layerZ == voxelPosition.z) {
            return vec3(0.8, 0.8, 0.8);
        }
    }
    {
        int layerX = voxelPosition.x & ~(brickHeight-1);
        int newZ = voxelPosition.z - layerX - brickWidth;
        int layerZ = newZ & ~(brickWidth-1);
        if(layerZ == newZ && v_normal.z == 0) {
            return vec3(0.8, 0.8, 0.8);
        }
        if((newZ & (brickWidth*2 - 1)) < brickWidth && layerX == voxelPosition.x) {
            return vec3(0.8, 0.8, 0.8);
        }
    }
    return vec3(0.7, 0.1, 0.0);
}

vec3 brickTexture(ivec3 voxelPosition, ivec3 voxel_coord) {
    int seed;
    int brickHeight = 4;
    int brickWidth = 8;
    if(isSideHalfBlock(voxelPosition, voxel_coord)) {
        return sideAlternatingBrickTexture(voxelPosition, voxel_coord, brickWidth, brickHeight);
    } else {
        return topDiagonalPattern(voxelPosition, brickWidth, brickHeight);
    }
    return vec3(0.7, 0.1, 0.0);
}

vec3 stoneTexturePerlin(ivec3 voxelPosition, vec2 texel_position) {
    int ySeed = xorshiftRandom(int(voxelPosition.y))*5473281;
    float perlinResult = perlinNoise(vec2(voxelPosition.x, voxelPosition.z)/4, ySeed);
    if(perlinResult > 0.3) return vec3(0.7, 0.7, 0.7);
    if(perlinResult > 0.0) return vec3(0.6, 0.6, 0.6);
    if(perlinResult > -0.3) return vec3(0.5, 0.5, 0.5);
    return vec3(0.4, 0.4, 0.4);
}

vec3 stoneTexture(ivec3 voxelPosition, vec2 texel_position) {
    int yseed = xorshiftRandom(int(texel_position.y))*5473281;
    
    int lineLength = (xorshiftRandom(yseed) % 8) - 3;;
    
    for (int i = 0; i < texel_position.x; i++) {
        if (lineLength == 0) {
            yseed = xorshiftRandom(yseed);
            lineLength = (xorshiftRandom(yseed) % 8) - 3;
        } else if (lineLength > 0)
            lineLength--;
        else if (lineLength < 0)
            lineLength++;
    }
    
    if (lineLength > 0)
        return vec3(0.6, 0.6, 0.6);
    else
        return vec3(0.5, 0.5, 0.5);
    
}
#endif