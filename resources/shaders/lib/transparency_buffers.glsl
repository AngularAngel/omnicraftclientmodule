
#ifndef LIB_TRANSPARENCY_BUFFERS
#define LIB_TRANSPARENCY_BUFFERS

struct GeometryFragment {
    float depth;
    vec3 normal;
    ivec2 textureIDs;
    ivec2 texelCoords;
};

struct SurfaceFragment {
    float depth;
    vec4 color;
    vec3 normal, material, light;
};

struct GeometryFragmentNode {
    GeometryFragment fragment;
    int nextID;
};

struct SurfaceFragmentNode {
    SurfaceFragment fragment;
    int nextID;
};

layout(binding = 0, offset = 0) uniform atomic_uint geometryNodeCounter;

layout(binding = 1, offset = 0) uniform atomic_uint surfaceNodeCounter;

layout(std430, binding = 0) restrict buffer geometryHeaderSSBO {
    int geometryHeaderNodes[];
};

layout(std430, binding = 1) restrict buffer geometryFragmentSSBO {
    GeometryFragmentNode geometryFragmentNodes[];
};

layout(std430, binding = 2) restrict buffer surfaceHeaderSSBO {
    int surfaceHeaderNodes[];
};

layout(std430, binding = 3) restrict buffer surfaceFragmentSSBO {
    SurfaceFragmentNode surfaceFragmentNodes[];
};

#endif