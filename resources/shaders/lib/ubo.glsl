#ifndef LIB_UBOS
#define LIB_UBOS

#include <definitions.glsl> 

layout (std140, binding = 0) uniform UBO {
    mat4 u_view_matrix;
    mat4 u_projection_matrix;
    mat4 u_inv_dir_matrix;
    mat4 u_screen_matrix;
    vec4[DIRECTIONAL_LIGHT_ARRAY_LENGTH] u_light_directions;
    vec4[DIRECTIONAL_LIGHT_ARRAY_LENGTH] u_light_colors;
    vec4 u_camera_pos;
    vec2 u_max_dir;
    ivec2 u_screen_size;
    float u_z_far;
    float u_texels_per_block;
    float u_texel_length;
    int u_chunk_block_length;
    int u_render_distance;
};

#define LOADED_CHUNKS_SIDE_LENGTH (1 << int(ceil(log2((u_render_distance + 1) * 2))))
#define LOADED_CHUNKS_BITMASK (LOADED_CHUNKS_SIDE_LENGTH - 1)

#endif