#version 430

#include <lib/ubo.glsl>
#include <lib/transparency_buffers.glsl>
#include <lib/surface_textures.glsl>
#include <lib/light_constants.glsl>
#include <lib/light_calcs.glsl>

uniform samplerCubeShadow u_shadow_tex;

uniform vec3 u_light_position;
uniform vec3 u_light_color;

uniform float u_light_linear;
uniform float u_light_quadratic;
uniform float u_light_radius;
uniform float u_shadow_z_near;
uniform float u_shadow_res;

uniform bool u_has_shadows;

out vec3 out_color;

vec2 calcTexCoord() {
   return gl_FragCoord.xy / u_screen_size;
}

float z_to_depth(float n, float f, float z) {
    float z_range = f - n;
    float ndc_z = (f + n) / z_range - (2.0 * f * n) / (z * z_range);
    return ndc_z * 0.5 + 0.5;
}

float getShadow(vec3 light_vector, vec3 light_dir, float cosang) {
    if (u_has_shadows) {
        float shadow_view_z = max(max(abs(light_vector.x), abs(light_vector.y)), abs(light_vector.z));

        float slope = sqrt(1.0 - cosang * cosang) / cosang;
        float shadow_texel_size = sqrt(3.0) * shadow_view_z / u_shadow_res;
        float shadow_bias = (slope + 1.0) * shadow_texel_size;

        float shadow_depth = z_to_depth(u_shadow_z_near, u_light_radius, shadow_view_z - shadow_bias);
        return texture(u_shadow_tex, vec4(-light_dir, shadow_depth));
    } else
        return 1;
}

vec3 getIrradiance(vec3 texel_pos) {
    float distance = length(u_light_position - texel_pos);

    float attenuation = 1.0 / (1.0 + u_light_linear * distance + u_light_quadratic * distance * distance);
    return u_light_color*attenuation;
}

vec3 getPointLight(vec2 tex_coord, float depth, vec4 color, vec3 normal, vec3 material) {
    if (length(normal) == 0)
        return vec3(0);

    vec3 world_dir = mat3(u_inv_dir_matrix) * vec3(((tex_coord * 2.0) - 1.0) * u_max_dir, -1.0);

    vec3 relative_pos = world_dir * depth;
    vec3 pos = u_camera_pos.xyz + relative_pos;
    vec3 texel_pos = (floor(pos/(u_texel_length*2) - normal * 0.01) + vec3(0.5) + normal * 0.5) * (u_texel_length*2);
    
    vec3 light_vector = u_light_position - texel_pos;
    
    vec3 light_dir = normalize(light_vector);
    
    float cosang = dot(normal, light_dir);
    
    if (cosang <= 0.0)
        return vec3(0);
    
    float shadow = getShadow(light_vector, light_dir, cosang);
    
    if (shadow <= 0.0)
        return vec3(0);

    vec3 texel_relative_to_camera = texel_pos - u_camera_pos.xyz;
    
    float texel_depth = (texel_relative_to_camera / world_dir).x;
    vec3 texel_world_dir = texel_relative_to_camera / texel_depth;

    vec3 irradiance = getIrradiance(texel_pos);
    
    vec3 reflection = getReflection(color, normal, material, light_dir, texel_world_dir, cosang);

    return getLighting(irradiance, shadow, reflection);
}

void main() {
    vec2 tex_coord = calcTexCoord();

    // retrieve data from G-buffer
    float depth = texture(u_depth_tex, tex_coord).r * u_z_far;
    vec3 color = texture(u_color_tex, tex_coord).rgb;
    vec3 normal = texture(u_normal_tex, tex_coord).rgb * 2 - 1;
    vec3 material = texture(u_material_tex, tex_coord).rgb;

    out_color = getPointLight(tex_coord, depth, vec4(color, 1), normal, material);

    int frag_header_index = int(gl_FragCoord.x) + int(gl_FragCoord.y) * u_screen_size.x;
    int curNodeID = surfaceHeaderNodes[frag_header_index];
    while (curNodeID != -1) {
        SurfaceFragmentNode curNode = surfaceFragmentNodes[curNodeID];
        curNodeID = curNode.nextID;

        SurfaceFragment fragment = curNode.fragment;

        if (fragment.depth * u_z_far > depth)
            continue;

        out_color = mix(out_color, out_color*fragment.color.rgb, fragment.color.a) + getPointLight(tex_coord, fragment.depth * u_z_far, fragment.color, fragment.normal, fragment.material);
    }
    
    //out_color = vec3(0);
}