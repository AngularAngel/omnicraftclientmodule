#version 420

#include <lib/ubo.glsl>

uniform mat4 u_model_matrix;

in vec3 in_pos;

void main() {
    gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vec4(in_pos, 1.0);
}