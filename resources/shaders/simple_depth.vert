#version 430

#include <lib/definitions.glsl> 
#include <lib/chunk_culling_buffers.glsl>
#include <lib/position_functions.glsl>

in vec3 in_pos;

uniform mat4 u_model_matrix;
uniform mat4 u_shadow_view_matrix;

void main()
{   
    vec4 world_pos = u_model_matrix * vec4(in_pos, 1.0);

    gl_Position = u_shadow_view_matrix * world_pos;

    updateTemporalCohesion(getBlockPos(world_pos.xyz));
}  
