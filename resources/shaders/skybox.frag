#version 430

#include <lib/ubo.glsl>
#include <lib/transparency_buffers.glsl>
#include <lib/geometry_outputs.glsl>

in vec2 v_tex_coord;

void main() {
    int frag_index = int(gl_FragCoord.x) + int(gl_FragCoord.y) * u_screen_size.x;
    geometryHeaderNodes[frag_index] = -1;
    surfaceHeaderNodes[frag_index] = -1;

    gl_FragDepth = uintBitsToFloat(0x7F800000);
    out_normal = vec3(0.5);
    out_texture_info = ivec4(-3, -3, 0, 0);
}