#version 430

#include <lib/ubo.glsl>
#include <lib/transparency_buffers.glsl>
#include <lib/geometry_textures.glsl>
#include <lib/surface_outputs.glsl>
#include <lib/surface_fragments.glsl>
#include <lib/position_functions.glsl>

vec2 calcTexCoord() {
   return gl_FragCoord.xy / u_screen_size;
}

void main() {
    vec2 tex_coord = calcTexCoord();

    // retrieve data from G-buffer
    float opaqueDepth = texture(u_depth_tex, tex_coord).r;
    vec3 opaqueNormal = texture(u_normal_tex, tex_coord).rgb * 2 - 1;
    ivec2 texture_ids = texture(u_texture_info_tex, tex_coord).rg;
    ivec2 texture_coords = texture(u_texture_info_tex, tex_coord).ba;

    vec3 world_dir = mat3(u_inv_dir_matrix) * vec3(((tex_coord * 2.0) - 1.0) * u_max_dir, -1.0);
    
    vec3 pos = getWorldPos(opaqueDepth, world_dir);

    bool output_opaque = false;
    
    if (length(opaqueNormal) < 0.1) {
        gl_FragDepth = opaqueDepth;
        out_color = vec3(0);
        for (int i = 0; i < DIRECTIONAL_LIGHT_ARRAY_LENGTH; i++) {
            vec3 skylight = vec3(0);
            skylight += vec3(0.15, 0.15, 0.75) * getSkyLight(i, vec3(0, 1, 0), 10);
            skylight += vec3(0.15, 0.15, 0.75) * getSkyLight(i, normalize(vec3(0.5, 1, 0)), 0.5);
            skylight += vec3(0.15, 0.15, 0.75) * getSkyLight(i, normalize(vec3(-0.5, 1, 0)), 0.5);
            skylight += vec3(0.15, 0.15, 0.75) * getSkyLight(i, normalize(vec3(0, 1, 0.5)), 0.5);
            skylight += vec3(0.15, 0.15, 0.75) * getSkyLight(i, normalize(vec3(0, 1, -0.5)), 0.5);
            skylight += vec3(0.75, 0.15, 0.15) * getSkyLight(i, normalize(world_dir), 1);
            out_color += u_light_colors[i].rgb * getSkyLight(i, normalize(world_dir), 100, 0.99995);
            out_color += max(skylight - vec3(0.05, 0.05, 0.05), vec3(0));
        }
        
        out_normal = vec3(0.5);
        out_material = vec3(0);
        out_light = vec3(0);
        output_opaque = true;
    } else {
        vec3 block_pos = getBlockPos(pos);
        if (true) {
            colorSurfaceFragment(block_pos, getTextures(block_pos, GeometryFragment(opaqueDepth, opaqueNormal, texture_ids, texture_coords)), getTexelPos(block_pos, opaqueNormal));
            output_opaque = true;
        }
    }
    
    int frag_header_index = int(gl_FragCoord.x) + int(gl_FragCoord.y) * u_screen_size.x;
    
    int curGeometryNodeID = geometryHeaderNodes[frag_header_index];

    pos = getWorldPos(geometryFragmentNodes[curGeometryNodeID].fragment.depth, world_dir);

    while (curGeometryNodeID != -1) {
        GeometryFragmentNode curGeometryNode = geometryFragmentNodes[curGeometryNodeID];

        vec3 block_pos = getBlockPos(pos);
        vec3 chunk_pos = getChunkPos(block_pos);

        float nextDepth = opaqueDepth;

        if (curGeometryNode.nextID > -1)
            nextDepth = geometryFragmentNodes[curGeometryNode.nextID].fragment.depth;

        if (curGeometryNode.fragment.textureIDs.x != -2) {
            if (getSurfaceFragment(nextDepth, getTextures(block_pos, curGeometryNode.fragment), world_dir).color.a == 1)
                output_opaque = true;
        }
        
        curGeometryNodeID = curGeometryNode.nextID;
        pos = getWorldPos(geometryFragmentNodes[curGeometryNode.nextID].fragment.depth, world_dir);
    }

    if (!output_opaque)
        discard;
}