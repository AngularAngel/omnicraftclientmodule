#version 420

#include <lib/ubo.glsl>

in vec2 in_pos;

void main() {
    vec4 pos = vec4(mat3(u_screen_matrix) * vec3(in_pos, 0), 1.0);
    gl_Position = pos;
}