#version 460

#include <lib/ubo.glsl>

flat in int i_subsurface_texture_id;
flat in int i_surface_texture_id;
flat in int i_transparent;
in vec2 v_tex_coord;
in vec3 v_normal;
in float v_view_z;

#include <lib/geometry_fragments.glsl>

void main() {
    int out_subsurface_texture_id = i_subsurface_texture_id, out_surface_texture_id = i_surface_texture_id;
    
    float depth = -v_view_z/u_z_far;

    if (!gl_FrontFacing) {
        if (i_transparent != 1)
            discard;
        else {
            out_subsurface_texture_id = -2;
            out_surface_texture_id = -2;
            depth -= 0.0000001;
        }
    }
    
    getGeometryFragment(depth, out_subsurface_texture_id, out_surface_texture_id, i_transparent);
    if (i_transparent == 1)
        discard;
}