#version 420

#include <lib/ubo.glsl>

uniform mat4 u_model_matrix;

in vec3 in_pos;
in vec2 in_tex_coord;
in vec3 in_normal;
in int in_subsurface_texture_id;
in int in_surface_texture_id;
in int in_transparent;

flat out int i_subsurface_texture_id;
flat out int i_surface_texture_id;
flat out int i_transparent;
out vec2 v_tex_coord;
out vec3 v_normal;
out float v_view_z;

void main() {
    vec4 pos = u_model_matrix * vec4(in_pos, 1.0);

    i_subsurface_texture_id = in_subsurface_texture_id;
    i_surface_texture_id = in_surface_texture_id;
    i_transparent = in_transparent;
    
    v_tex_coord = in_tex_coord;
    v_normal = in_normal;
    
    vec4 view_pos = u_view_matrix * pos;
    v_view_z = view_pos.z;
    
    gl_Position = u_projection_matrix * view_pos;
}