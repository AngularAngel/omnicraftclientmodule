#version 420

#include <lib/ubo.glsl>

uniform sampler2D u_hdr_tex;

uniform float u_exposure;

in vec2 v_tex_coord;

out vec3 out_color;

const vec3 GAMMA = vec3(1.0/2.2);

void main() {
    vec3 base_color = texture(u_hdr_tex, v_tex_coord).rgb;

    vec3 luma = vec3(0.2126, 0.7152, 0.0722);

    //out_color = min(base_color*luma, normalize(base_color*luma))/luma;
    //out_color = min(base_color, normalize(base_color)); 
    //out_color = clamp(base_color*u_exposure, 0.0, 1.0); 
    out_color = vec3(1.0) - exp(-base_color * u_exposure);
    //out_color = max(out_color, vec3(1.0) - exp(-base_color * u_exposure));

    //out_color = base_color;
    out_color = pow(out_color, GAMMA);
}