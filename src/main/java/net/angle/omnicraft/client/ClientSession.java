package net.angle.omnicraft.client;

import com.samrj.devil.math.Vec3;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import lombok.Getter;
import net.angle.omnicraft.client.rendering.CubeRenderTemplate;
import net.angle.omnicraft.client.screens.GameScreen;
import net.angle.omnicraft.common.ChunkCollisionComponent;
import net.angle.omnicraft.common.BasicChunkUnloaderComponent;
import net.angle.omnicraft.common.OmnicraftGameWorld;
import net.angle.omnicraft.server.OmnicraftServer;
import net.angle.omnientity.api.Component;
import net.angle.omnientity.api.Entity;
import net.angle.omnientity.api.EntityEnvironment;
import net.angle.omnientity.api.MotionComponent;
import net.angle.omnientity.api.OmniEntityModule;
import net.angle.omnilighting.api.OmniLightingModule;
import net.angle.omninetwork.impl.BasicNettyClient;
import net.angle.omniserialization.api.OmniSerializationModule;
import net.angle.omniserialization.api.ObjectSerializerRegistry;
import net.angle.omniregistry.api.RegistryCollection;
import net.angle.omniserialization.api.ClassIDRegistry;
import net.angle.omniworld.api.GameWorld;

/**
 *
 * @author angle
 */
public class ClientSession {
    private final OmnicraftClient client;
    private final @Getter OmnicraftGameWorld gameWorld;
    private final ObjectSerializerRegistry serializerRegistry;
    private final BasicNettyClient nettyClient;
    private @Getter Entity player;
    private boolean shuttingDown = false;

    public ClientSession(OmnicraftClient client) {
        this(client, null, -1);
    }

    public ClientSession(OmnicraftClient client, String host, int port) {
        this.client = client;
        gameWorld = new OmnicraftGameWorld(client.getRuntime(), true);
        serializerRegistry = client.getRuntime().getService(OmniSerializationModule.class).getSerializerRegistry();
        if (Objects.nonNull(host))
            nettyClient = new BasicNettyClient(serializerRegistry, host, port);
        else
            nettyClient = new BasicNettyClient(serializerRegistry);
    }
    
    public void init() {
        gameWorld.init();
        
        EntityEnvironment entities = gameWorld.getEntities();
        entities.registerTemplate(new CubeRenderTemplate());
        
        ClassIDRegistry componentInterfaceRegistry = entities.getComponentInterfaceRegistry();
        client.getRuntime().getService(OmniEntityModule.class).prepComponentInterfaceRegistry(componentInterfaceRegistry);
        client.getRuntime().getService(OmniLightingModule.class).prepComponentInterfaceRegistry(componentInterfaceRegistry);
        
        ClassIDRegistry componentClassRegistry = entities.getComponentClassRegistry();
        client.getRuntime().getService(OmniEntityModule.class).prepComponentClassRegistry(componentClassRegistry);
        client.getRuntime().getService(OmniLightingModule.class).prepComponentClassRegistry(componentClassRegistry);
        
        Map<Class<? extends Component>, Function<Entity, Component>> componentConstructors = entities.getComponentConstructors();
        client.getRuntime().getService(OmniEntityModule.class).prepComponentConstructors(componentConstructors);
        client.getRuntime().getService(OmniLightingModule.class).prepComponentConstructors(componentConstructors);
        
        client.getRuntime().prepSerializerRegistry(serializerRegistry);
        
        nettyClient.addDatagramTarget(ClientSession.class, this);
        nettyClient.addDatagramTarget(RegistryCollection.class, gameWorld.getRegistries());
        nettyClient.addDatagramTarget(EntityEnvironment.class, entities);
        nettyClient.addDatagramTarget(GameWorld.class, gameWorld);
        
        nettyClient.init();
        nettyClient.start();
    }
    
    public void initializePlayer(int id, boolean chunkUnloader) {
        player = client.getRuntime().getService(OmniEntityModule.class).getClientPlayer(id, gameWorld.getEntities());
        player.addComponent(new ChunkCollisionComponent(player, gameWorld.getChunkMap(), gameWorld.getBlocksComponent(), new Vec3(0.5f)));
        if (chunkUnloader)
            player.addComponent(new BasicChunkUnloaderComponent(gameWorld.getChunkMap(), OmnicraftServer.RENDER_DISTANCE, player));
        gameWorld.addEntity(player);
        
        player.init();
        client.changeScreen(new GameScreen(client, this));
    }
    
    public boolean isPaused() {
        return gameWorld.isPaused();
    }
    
    public void pause() {
        gameWorld.pause();
    }
    
    public void unpause() {
        gameWorld.unpause();
    }
    
    public void sendUpdatesToServer(float dt) {
        if (nettyClient.isConnected()) {
            gameWorld.getEntities().sendUpdates(MotionComponent.class, nettyClient::sendToServer);
        }
    }
    
    public void step(float dt) {
        nettyClient.readUpdates();
        if (Objects.nonNull(player)) {
            sendUpdatesToServer(dt);
            gameWorld.step(dt);
        }   
    }
    
    public void beginShutdown() {
        shuttingDown = true;
        destroy(false);
    }
    
    public boolean isShuttingDown() {
        return shuttingDown;
    }
    
    public boolean isShutdown() {
        return nettyClient.isShutdown();
    }
    
    public void destroy(Boolean crashed) {
        nettyClient.interrupt();
        gameWorld.destroy(crashed);
    }
}