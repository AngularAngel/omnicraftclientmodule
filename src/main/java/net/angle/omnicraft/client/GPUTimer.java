package net.angle.omnicraft.client;

import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.function.LongConsumer;
import org.lwjgl.opengl.GL11C;
import org.lwjgl.opengl.GL33C;

/**
 *
 * @author Mostly Smashmaster, Partially Angle
 */
public class GPUTimer {
    private record TimerQuery(int startID, int endID) {}
    private final ArrayDeque<TimerQuery> TIMER_QUERIES = new ArrayDeque<>();
    private TimerQuery frameTimer;
    
    public void start() {
        frameTimer = new TimerQuery(GL33C.glGenQueries(), GL33C.glGenQueries());
        GL33C.glQueryCounter(frameTimer.startID, GL33C.GL_TIMESTAMP);
        TIMER_QUERIES.add(frameTimer);
    }
    
    public void stop(LongConsumer longCon) {
        GL33C.glQueryCounter(frameTimer.endID, GL33C.GL_TIMESTAMP);

        Iterator<TimerQuery> it = TIMER_QUERIES.iterator();
        while (it.hasNext())
        {
            TimerQuery query = it.next();
            if (GL33C.glGetQueryObjecti(query.endID, GL33C.GL_QUERY_RESULT_AVAILABLE) == GL11C.GL_TRUE)
            {
                long timerStart = GL33C.glGetQueryObjecti64(query.startID, GL33C.GL_QUERY_RESULT);
                long timerEnd = GL33C.glGetQueryObjecti64(query.endID, GL33C.GL_QUERY_RESULT);
                
                longCon.accept(timerEnd - timerStart);
                it.remove();
                GL33C.glDeleteQueries(query.startID);
                GL33C.glDeleteQueries(query.endID);
            }
        }
    }
    
    public void stop(String label) {
        stop((value) -> {
            System.out.println(label + ": " + value/1_000_000.0 + " ms elapsed");
        });
    }
}
