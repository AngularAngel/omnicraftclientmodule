package net.angle.omnicraft.client;

import com.samrj.devil.game.GameWindow;
import java.util.Objects;
import lombok.Getter;
import net.angle.omniclient.api.Settings;
import net.angle.omniclient.impl.BasicClient;
import net.angle.omniclient.impl.BasicSettings;
import net.angle.omnicraft.client.screens.TitleScreen;
import net.angle.omnicraft.common.BasicOmnicraftRuntime;
import net.angle.omnicraft.common.OmnicraftRuntime;
import net.angle.omnicraft.server.OmnicraftServer;
import net.angle.omniresource.api.ResourceManager;

/**
 *
 * @author angle
 */
public class OmnicraftClient extends BasicClient {
    private final @Getter OmnicraftRuntime runtime;
    private final Settings settings = new BasicSettings();
    
    private OmnicraftServer server;
    private ClientSession clientSession;
    
    private boolean shuttingDown = false;
    
    public final Settings.Setting<Boolean> vSync;
    
    public OmnicraftClient(ResourceManager resourceManager, String title, OmnicraftRuntime runtime) {
        super(resourceManager, title);
        this.runtime = runtime;
        
        vSync = settings.addSetting(new BasicSettings.BasicSetting("VSync"));
        vSync.setCallback((t) -> {
            GameWindow.setVsync(t);
        });
    }

    @Override
    public void init() {
        super.init();
        vSync.setValue(true);
    }
    
    public static void main(String[] args) {
        BasicOmnicraftRuntime basicRuntime = new BasicOmnicraftRuntime();
        basicRuntime.createAndStartRuntime();
    }
    
    public void startSinglePlayerGame() {
        server = new OmnicraftServer(getRuntime());
        clientSession = new ClientSession(this);
        
        server.init();
        clientSession.init();
    }
    
    public void startMultiPlayerGame(String host, int port) {
        clientSession = new ClientSession(this, host, port);
        clientSession.init();
    }
    
    public void startMultiPlayerGame(String host) {
        startMultiPlayerGame(host, 25565);
    }
    
    public void endSinglePlayerGame() {
        clientSession.beginShutdown();
        if (Objects.nonNull(server))
            server.interrupt();
        changeScreen(new TitleScreen(this));
    }
    
    public void endApplication() {
        shuttingDown = true;
    }
    
    public void endClientSession() {
        clientSession = null;
    }

    @Override
    public void step(float dt) {
        if (Objects.nonNull(clientSession)) {
            if (!clientSession.isShuttingDown())
                clientSession.step(dt);
            else if (clientSession.isShutdown())
                clientSession = null;
        } else if (shuttingDown)
            GameWindow.stop();
        super.step(dt);
    }
    
    public void beginShutdown() {
        shuttingDown = true;
    }

    @Override
    public void destroy(Boolean crashed) {
        if (Objects.nonNull(clientSession))
            clientSession.destroy(crashed);
        if (Objects.nonNull(server))
            server.interrupt();
        super.destroy(crashed);
    }
}