package net.angle.omnicraft.client;

import com.samrj.devil.game.Console;
import java.util.HashMap;
import java.util.Objects;
import java.util.function.Function;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 *
 * @author angle
 */
public class OmnicraftConsole extends Console {
    private final HashMap<String, Command> commands = new HashMap<>();
    
    public OmnicraftConsole() {
        setCallback((inputString) -> {
            String[] tokens = inputString.split(" ");
            Command command = commands.get(tokens[0]);
            if (Objects.nonNull(command))
                return command.apply(inputString.substring(command.getName().length()));
            else
                return "Unrecognized Command: " + tokens[0];
        });
        addCommand(new Command("clear", "Clear the console") {
            @Override
            public String apply(String inputString) {
                clear();
                return null;
            }
        });
        addCommand(new Command("echo", "Write to console output.") {
            @Override
            public String apply(String inputString) {
                if (inputString.length() > 0)
                    return inputString.substring(1);
                else return "";
            }
        });
        addCommand(new Command("help", "List all commands.") {
            @Override
            public String apply(String inputString) {
                StringBuilder output = new StringBuilder("Listing all commands!");
                    output.append('\n');
                
                for (Command command : commands.values()) {
                    output.append('\n');
                    output.append(command.getName() + ": " + command.getDescription());
                }
                
                return output.toString();
            }
        });
    }
    
    public boolean addCommand(Command command) {
        if (commands.containsKey(command.getName()))
            return false;
        else {
            commands.put(command.getName(), command);
            return true;
        }
    }
    
    @RequiredArgsConstructor
    public static abstract class Command implements Function<String, String> {
        private final @Getter String name;
        private final @Getter String description;
    }
}