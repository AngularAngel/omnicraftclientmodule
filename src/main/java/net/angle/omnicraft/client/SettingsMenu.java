package net.angle.omnicraft.client;

import com.samrj.devil.gui.Align;
import com.samrj.devil.gui.ComboBox;
import com.samrj.devil.gui.DUI;
import com.samrj.devil.gui.FixedRow;
import com.samrj.devil.gui.Form;
import com.samrj.devil.gui.LayoutRows;
import com.samrj.devil.gui.ScrollBox;
import com.samrj.devil.gui.Text;
import com.samrj.devil.gui.Window;
import net.angle.omniclient.api.Settings.Setting;

/**
 *
 * @author angle
 */
public class SettingsMenu {
    private static final float WIDTH = 512.0f, HEIGHT = 640.0f;
    private static final float ROW_HEIGHT = 30;
    private static final float COMBO_WIDTH = 160.0f;
    
    private final OmnicraftClient client;
    public final Window window;

    public SettingsMenu(OmnicraftClient client) {
        this.client = client;
        this.window = new Window()
                .setTitle("Settings")
                .setPadding(0.0f)
                .setContent(layout())
                .setSizeFromContent();
        
    }
    
    private Form row(String label, Form form) {
        return new FixedRow()
                .setAlignment(Align.W.vector())
                .add(0.0f, new Text(label))
                .add(WIDTH/2.0f, form)
                .setSizeFromContent()
                .setHeight(ROW_HEIGHT);
    }
    
    private Form combool(Setting<Boolean> setting, String label, String option0, String option1) {
        return row(label, new ComboBox()
                .setOptions(option0, option1)
                .setSelection(setting.getValue() ? 1 : 0)
                .setSelectCallback(box -> setting.setValue(box.getSelection() == 1))
                .setSize(COMBO_WIDTH));
    }
    
    private Form combool(Setting<Boolean> setting, String label)
    {
        return combool(setting, label, "Off", "On");
    }
    
    private Form layout() {
        ScrollBox scroll = new ScrollBox();
        LayoutRows rows = new LayoutRows();
        
        rows.add(new Text("Display"), Align.N.vector());
        
        rows.add(combool(client.vSync, "Vertical Sync"));
        
        return scroll.setContent(rows).setSizeFromContent(HEIGHT);
    }
    
    public void setVisible(boolean visible) {
        if (visible) {
            DUI.show(window.setPosCenterViewport());
        }
        else if (window.isVisible())
            DUI.hide(window);
    }
}
