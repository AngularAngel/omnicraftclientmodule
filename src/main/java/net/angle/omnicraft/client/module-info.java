
module omnicraft.client {
    requires com.google.common;
    requires org.lwjgl;
    requires org.lwjgl.glfw;
    requires org.lwjgl.opengl;
    requires java.logging;
    requires static lombok;
    requires devil.util;
    requires osgi.core;
    requires io.netty.handler;
    requires omni.module;
    requires omni.resource;
    requires omni.rendering;
    requires omni.client;
    requires omni.entity;
    requires omni.network;
    requires omni.registry;
    requires omni.serialization;
    requires omni.world;
    requires omni.blocks;
    requires omni.sides;
    requires omni.lighting;
    requires omni.generation;
}