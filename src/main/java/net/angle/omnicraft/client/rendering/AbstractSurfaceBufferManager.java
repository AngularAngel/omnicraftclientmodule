package net.angle.omnicraft.client.rendering;

import com.samrj.devil.gl.VertexBuilder;
import com.samrj.devil.math.Vec2;
import com.samrj.devil.math.Vec3;
import lombok.Getter;
import lombok.Setter;
import net.angle.omnirendering.impl.BasicTextureVertexManager3D;
import net.angle.omniworld.api.space.BlockFace;

/**
 *
 * @author angle
 */
public abstract class AbstractSurfaceBufferManager extends BasicTextureVertexManager3D {
    private  Vec3 bufferVNormal;
    private  VertexBuilder.IntAttribute buffer_subsurface_palette_index;
    private  VertexBuilder.IntAttribute buffer_surface_palette_index;
    private  VertexBuilder.IntAttribute buffer_transparent;
    
    private @Getter @Setter boolean drawable = false;
    
    public AbstractSurfaceBufferManager() {
        super();
    }
    
    public void beginVariables() {
        beginVariables(10507, 15760);
    }

    @Override
    public void beginVariables(int vertices, int indices) {
        super.beginVariables(vertices, indices);
        bufferVNormal = vertexBuilder.vec3("in_normal");
        buffer_subsurface_palette_index = vertexBuilder.aint("in_subsurface_texture_id");
        buffer_surface_palette_index = vertexBuilder.aint("in_surface_texture_id");
        buffer_transparent = vertexBuilder.aint("in_transparent");
    }
    
    public void vertex(int subsurface_id, int surface_id, int transparent, Vec3 vPos, Vec3 vNormal, Vec2 vTexCoord) {
        bufferVNormal.set(vNormal);
        buffer_subsurface_palette_index.x = subsurface_id; 
        buffer_surface_palette_index.x = surface_id; 
        buffer_transparent.x = transparent; 
        super.vertex(vPos, vTexCoord);
    }
    
    public void bufferSurface(BlockFace face, Vec3 startPos, Vec2 dimensions, int subsurface_id, int surface_id, int transparent, Vec2 textureOffset) {
        Vec3 normal = face.getNormal();
        
        Vec3 topLeftPos = startPos;
        Vec3 topRightPos = Vec3.add(topLeftPos, face.getRightwards().mult(dimensions.x));
        Vec3 bottomRightPos = Vec3.add(topRightPos, face.getDownwards().mult(dimensions.y));
        Vec3 bottomLeftPos = Vec3.add(topLeftPos, face.getDownwards().mult(dimensions.y));
        
        
        int startingVertex = getNextVertex();
        
        //First, the vertices
        vertex(subsurface_id, surface_id, transparent, topLeftPos, normal, textureOffset);
        vertex(subsurface_id, surface_id, transparent, topRightPos, normal, new Vec2(dimensions.x * 2, 0).add(textureOffset));
        vertex(subsurface_id, surface_id, transparent, bottomRightPos, normal, new Vec2(dimensions.x * 2, dimensions.y * 2).add(textureOffset));
        vertex(subsurface_id, surface_id, transparent, bottomLeftPos, normal, new Vec2(0, dimensions.y * 2).add(textureOffset));
        
        //add first trangle, starting at top left corner, then bottom right, then top right
        vertexBuilder.index(startingVertex);
        vertexBuilder.index(startingVertex + 2);
        vertexBuilder.index(startingVertex + 1);
        
        //add second at top left corner, then bottom left, then bottom right
        vertexBuilder.index(startingVertex);
        vertexBuilder.index(startingVertex + 3);
        vertexBuilder.index(startingVertex + 2);
    }
    
    public void finish() {
        if (getNumVertices() > 0) {
            upload();
            drawable = true;
        } else
            drawable = false;
    }

    @Override
    public void deleteVertices() {
        drawable = false;
        super.deleteVertices();
    }
}