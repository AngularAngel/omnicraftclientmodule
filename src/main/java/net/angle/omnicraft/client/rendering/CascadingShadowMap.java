/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.angle.omnicraft.client.rendering;

import com.samrj.devil.gl.DGL;
import com.samrj.devil.gl.FBO;
import com.samrj.devil.gl.Texture2DArray;
import com.samrj.devil.graphics.Camera3D;
import com.samrj.devil.graphics.ViewFrustum;
import com.samrj.devil.math.Mat4;
import com.samrj.devil.math.Quat;
import com.samrj.devil.math.Util;
import com.samrj.devil.math.Vec2;
import com.samrj.devil.math.Vec3;
import java.io.IOException;
import java.util.function.Consumer;
import java.util.stream.Stream;
import net.angle.omnientity.api.CameraComponent;
import org.lwjgl.opengl.GL11C;
import static org.lwjgl.opengl.GL12C.GL_CLAMP_TO_EDGE;
import org.lwjgl.opengl.GL14C;
import static org.lwjgl.opengl.GL30C.GL_COMPARE_REF_TO_TEXTURE;
import static org.lwjgl.opengl.GL30C.GL_DEPTH_ATTACHMENT;

/**
 *
 * @author angle
 */
public class CascadingShadowMap {
    
    public static final float Z_SPLIT_0 = 2.0f;
    public static final float Z_SPLIT_1 = 8.0f;
    public static final float Z_SPLIT_2 = 32.0f;
    public static final float Z_SPLIT_3 = 128.0f;
    public static final float Z_SPLIT_4 = 1024.0f;
    public static final float DEPTH_RANGE = 8192.0f;
    
    private final int size;
    
    public final Texture2DArray texture;
    public final ShadowMap map0, map1, map2, map3, map4, map5;
    
    private final Mat4 lightRotMat = new Mat4();
    private final Mat4 invViewMat = new Mat4();
    
    public CascadingShadowMap(CameraComponent cameraComp, int size) throws IOException {
        this.size = size;
        
        texture = DGL.genTex2DArray();
        texture.bind();
        texture.parami(GL11C.GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        texture.parami(GL11C.GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        texture.parami(GL14C.GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
        texture.parami(GL14C.GL_TEXTURE_COMPARE_FUNC, GL11C.GL_LEQUAL);
        texture.image(size, size, 6, GL14C.GL_DEPTH_COMPONENT24);
        
        map0 = new ShadowMap(cameraComp.getFrustum().zNear, Z_SPLIT_0, 0);
        map1 = new ShadowMap(Z_SPLIT_0, Z_SPLIT_1, 1);
        map2 = new ShadowMap(Z_SPLIT_1, Z_SPLIT_2, 2);
        map3 = new ShadowMap(Z_SPLIT_2, Z_SPLIT_3, 3);
        map4 = new ShadowMap(Z_SPLIT_3, Z_SPLIT_4, 4);
        map5 = new ShadowMap(Z_SPLIT_4, cameraComp.getFrustum().zFar, 5);
    }
    
    public void render(CameraComponent cameraComp, Vec3 lightDir, Consumer<ShadowMap> geomDrawFunc) {
        invViewMat.setTranslation(cameraComp.getCamera().pos);
        invViewMat.rotate(cameraComp.getCamera().dir);
        lightRotMat.setRotation(Quat.rotation(lightDir, new Vec3(0, 0, 1)));
        
        GL11C.glViewport(0, 0, size, size);
        
        Stream.of(map0, map1, map2, map3, map4, map5).forEach(shadowMap -> shadowMap.render(cameraComp, geomDrawFunc));
    }
    
    private static float discretize(float f, float d) {
        return Util.floor(f/d)*d;
    }
    
    public void destroy() {
        DGL.delete(texture);
        Stream.of(map0, map1, map2, map3, map4, map5).forEach(ShadowMap::destroy);
    }
    
    public class ShadowMap {
        
        public final float near, far;
        public final Mat4 matrix = new Mat4();
        
        private final FBO fbo;
        
        private ShadowMap(float near, float far, int layer) {
            this.near = near;
            this.far = far;
            
            fbo = DGL.genFBO();
            DGL.bindFBO(fbo);
            fbo.readBuffer(GL11C.GL_NONE);
            fbo.drawBuffers(GL11C.GL_NONE);
            fbo.textureLayer(texture, layer, GL_DEPTH_ATTACHMENT);
        }
        
        private void render(CameraComponent cameraComp, Consumer<ShadowMap> geomDrawFunc) {
            Vec2 circumsphere = cameraComp.getFrustum().getCircumsphere(near, far);
            Vec3 center = new Vec3(0, 0, circumsphere.x).mult(invViewMat).mult(lightRotMat);
            float radius = circumsphere.y;
            
            float texelSize = 2.0f*radius/size;
            center.x = discretize(center.x, texelSize);
            center.y = discretize(center.y, texelSize);
            
            float left = center.x - radius;
            float right = center.x + radius;
            float bottom = center.y - radius;
            float top = center.y + radius;
            float back = -center.z - radius - DEPTH_RANGE;
            float front = -center.z + radius + DEPTH_RANGE;
            
            matrix.setOrthographic(left, right, bottom, top, back, front).mult(lightRotMat);
            
            DGL.bindFBO(fbo);
            geomDrawFunc.accept(this);
        }
        
        private void destroy() {
            DGL.delete(fbo);
        }
    }
}
