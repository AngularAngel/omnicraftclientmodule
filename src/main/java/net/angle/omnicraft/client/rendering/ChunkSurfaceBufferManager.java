/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.angle.omnicraft.client.rendering;

import com.samrj.devil.geo3d.Box3;
import com.samrj.devil.geo3d.Frustum;
import com.samrj.devil.math.Vec2;
import com.samrj.devil.math.Vec3;
import com.samrj.devil.util.alloc.Allocation;
import java.util.Objects;
import lombok.Getter;
import lombok.Setter;
import net.angle.omnicraft.client.rendering.components.ChunkRenderer;
import net.angle.omniworld.api.chunks.Chunk;
import net.angle.omniworld.api.space.BlockFace;

/**
 * Holds the vertices and indices for a single chunk, before it is sent to the
 * ChunkVertexManager to be uploaded to the graphics card, and manages chunk textures more permanently.
 * @author angle
 */
public class ChunkSurfaceBufferManager extends AbstractSurfaceBufferManager {
    private final ChunkRenderer chunkRenderer;
    private final @Getter Chunk chunk;
    
    private @Getter Allocation vertexAllocation, indexAllocation;
    private @Getter @Setter Box3 aabb;

    public ChunkSurfaceBufferManager(ChunkRenderer chunkRenderer, Chunk chunk) {
        super();
        this.chunkRenderer = chunkRenderer;
        this.chunk = chunk;
    }
    
    public void bufferBlockFace(int transparent, Vec3 drawStart, BlockFace face, Vec2 dimensions, int chunkTexLength, float realEdgeLengthOfBlock) {
        Vec3 normal = face.getNormal();

        //Build a square out of two triangles.
        Vec3 topLeftPos = face.modifyDrawStart(drawStart);
        Vec3 topRightPos = Vec3.add(topLeftPos, face.getRightwards().mult(dimensions.x));
        Vec3 bottomRightPos = Vec3.add(topRightPos, face.getDownwards().mult(dimensions.y));
        Vec3 bottomLeftPos = Vec3.add(topLeftPos, face.getDownwards().mult(dimensions.y));
        
        topLeftPos.mult(realEdgeLengthOfBlock);
        topRightPos.mult(realEdgeLengthOfBlock);
        bottomLeftPos.mult(realEdgeLengthOfBlock);
        bottomRightPos.mult(realEdgeLengthOfBlock);
        
        int startingVertex = getNextVertex();

        //First, the vertices
        vertex(-1, -1, transparent, topLeftPos, normal, new Vec2(0, 0));
        vertex(-1, -1, transparent, topRightPos, normal, new Vec2(dimensions.x, 0));
        vertex(-1, -1, transparent, bottomRightPos, normal, new Vec2(dimensions.x, dimensions.y));
        vertex(-1, -1, transparent, bottomLeftPos, normal, new Vec2(0, dimensions.y));
        
        //add first trangle, starting at top left corner, then bottom right, then top right
        index(startingVertex);
        index(startingVertex + 2);
        index(startingVertex + 1);
        
        //add second at top left corner, then bottom left, then bottom right
        index(startingVertex);
        index(startingVertex + 3);
        index(startingVertex + 2);
    }

    @Override
    public void upload() {
        if (Objects.nonNull(vertexAllocation)) {
            deAllocate();
        }
        vertexAllocation = chunkRenderer.getVertexAllocator().allocateRegion(getNumVertices());
        indexAllocation = chunkRenderer.getIndexAllocator().allocateRegion(getNumIndices());
        
        chunkRenderer.copyAndUploadChunkData(this);
    }
    
    public boolean hasTextures() {
        return Objects.nonNull(vertexAllocation);
    }
    
    public boolean shouldDraw() {
        return isDrawable() && hasTextures();
    }
    
    public boolean shouldDraw(Frustum visibleFrustum) {
        return shouldDraw() && (Objects.isNull(visibleFrustum) || visibleFrustum.touching(aabb));
    }

    public void draw(Frustum visibleFrustum) {
        if (shouldDraw(visibleFrustum)) {
            super.draw();
        }
    }
    
    public void deAllocate() {
        if (Objects.nonNull(vertexAllocation) && vertexAllocation.isAllocated()) {
            vertexAllocation.deallocate();
            indexAllocation.deallocate();
        }
    }

    @Override
    public void destroy() {
        deAllocate();
        super.destroy();
    }
}