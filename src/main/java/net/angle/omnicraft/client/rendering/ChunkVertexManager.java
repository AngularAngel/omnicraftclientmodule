package net.angle.omnicraft.client.rendering;

import com.samrj.devil.geo3d.Box3;
import com.samrj.devil.geo3d.Frustum;
import com.samrj.devil.gl.BufferObject;
import com.samrj.devil.gl.DGL;
import com.samrj.devil.gl.Image;
import com.samrj.devil.gl.ShaderProgram;
import com.samrj.devil.gl.Texture3D;
import com.samrj.devil.gl.VertexBuilder;
import com.samrj.devil.math.Mat4;
import com.samrj.devil.math.Util;
import com.samrj.devil.math.Vec2;
import com.samrj.devil.math.Vec3;
import com.samrj.devil.math.Vec3i;
import com.samrj.devil.math.Vec4;
import com.samrj.devil.util.alloc.Allocation;
import com.samrj.devil.util.alloc.Allocator;
import com.samrj.devil.util.alloc.FirstFitAllocator;
import java.nio.ByteBuffer;
import java.util.Objects;
import lombok.Getter;
import net.angle.omniblocks.api.Block;
import net.angle.omnicraft.client.OmnicraftClient;
import net.angle.omnicraft.client.rendering.components.ChunkCullingShader;
import net.angle.omnicraft.client.rendering.components.SurfaceGeometryShader;
import net.angle.omnicraft.common.OmnicraftGameWorld;
import static net.angle.omnicraft.server.OmnicraftServer.RENDER_DISTANCE;
import net.angle.omnilighting.impl.PrimaryLightingChannels;
import net.angle.omniregistry.api.DatumRegistry;
import net.angle.omnirendering.impl.AllocatedTextureVertexManager3D;
import net.angle.omnisides.api.Side;
import net.angle.omniworld.api.chunks.Chunk;
import net.angle.omniworld.api.chunks.VoxelComponent;
import net.angle.omniworld.api.space.BlockFace;
import net.angle.omniworld.api.space.CoordinateSystem;
import net.angle.omniworld.impl.space.PrimaryBlockFaces;
import static org.lwjgl.opengl.ARBDrawIndirect.GL_DRAW_INDIRECT_BUFFER;
import org.lwjgl.opengl.ARBIndirectParameters;
import static org.lwjgl.opengl.ARBIndirectParameters.GL_PARAMETER_BUFFER_ARB;
import org.lwjgl.opengl.GL11C;
import static org.lwjgl.opengl.GL11C.GL_UNSIGNED_INT;
import static org.lwjgl.opengl.GL11C.glGetInteger;
import org.lwjgl.opengl.GL12C;
import org.lwjgl.opengl.GL13C;
import static org.lwjgl.opengl.GL15C.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15C.GL_ARRAY_BUFFER_BINDING;
import static org.lwjgl.opengl.GL15C.GL_DYNAMIC_DRAW;
import static org.lwjgl.opengl.GL15C.GL_ELEMENT_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15C.GL_ELEMENT_ARRAY_BUFFER_BINDING;
import static org.lwjgl.opengl.GL15C.glBindBuffer;
import static org.lwjgl.opengl.GL15C.nglBufferSubData;
import org.lwjgl.opengl.GL30C;
import static org.lwjgl.opengl.GL30C.glBindBufferBase;
import org.lwjgl.opengl.GL42C;
import org.lwjgl.opengl.GL43C;
import org.lwjgl.system.MemoryUtil;
import static org.lwjgl.system.MemoryUtil.memAddress0;

/**
 * //Manages the vertices, indices, and indirect drawing commands for all chunks currently being drawn.
 * @author angle
 */
public class ChunkVertexManager extends AllocatedTextureVertexManager3D {
    private final int LOADED_CHUNKS_SIDE_LENGTH;
    public final int GIANT_TEXTURE_SIDE_LENGTH;
    private final int MAX_CHUNKS;
    
    
    //Size of 6 vec4's.
    private static final int FRUSTUM_DATA_SIZE = 6 * 4 * Integer.BYTES;
    //Size of 64 frustums.
    private static final int PREV_FRUSTUMS_DATA_SIZE = 64 * FRUSTUM_DATA_SIZE;
    //Size of bounding box (2 vec3s) + 3 ints.
    private static final int CHUNK_CULLING_DATA_SIZE = (2 * 3 * Float.BYTES) + 3 * Integer.BYTES + 64 * Integer.BYTES;
    //Size of 5 ints.
    private static final int INDIRECT_DRAW_DATA_SIZE = 5 * Integer.BYTES;
    
    //
    private final int CHUNK_DATA_SIDE_LENGTH;
    
    //1 int per block in the chunk, plus 1 for each currounding block.
    private final int CHUNK_BLOCK_DATA_SIZE;
    
    private static final int CHUNK_CULLING_SSBO_BASE = 4; //Fifth SSBO
    private static final int ATOMIC_COUNTER_BASE = 2; //Third atomic counter
    private static final int INDIRECT_DRAW_SSBO_BASE = 5; //Sixth SSBO
    private static final int CHUNK_BLOCK_DATA_SSBO_BASE = 6; //Seventh SSBO
    
    private final VoxelComponent<Block> blocksComponent;
    private final VoxelComponent<Side>[] sideComponents;
    private final VoxelComponent<Long> lightingComponent;
    
    private final CoordinateSystem coordinateSystem;
    
    private final @Getter Allocator vertexAllocator, indexAllocator;
    private final SurfaceGeometryShader chunkSurfaceShader;
    private final ChunkCullingShader chunkCullingShader;
    private final BufferObject chunkCullingData;
    private final BufferObject atomicBuffer;
    private final BufferObject indirectDrawBuffer;
    
    private final BufferObject chunkBlockData;
    
    private Texture3D ambientLightingTexture1;
    private Texture3D ambientLightingTexture2;
    
    private Vec3 bufferVNormal;
    private VertexBuilder.IntAttribute buffer_subsurface_palette_index;
    private VertexBuilder.IntAttribute buffer_surface_palette_index;
    private VertexBuilder.IntAttribute buffer_transparent;
    
    private int temporalIndex;

    public ChunkVertexManager(OmnicraftClient client, OmnicraftGameWorld world, SurfaceGeometryShader chunkSurfaceShader) {
        super();
        
        this.chunkSurfaceShader = chunkSurfaceShader;
        chunkCullingShader = new ChunkCullingShader(client);
        
        DatumRegistry<VoxelComponent> chunkComponents = (DatumRegistry<VoxelComponent>) world.getRegistries().getEntryByName("Chunk Components");
        blocksComponent = chunkComponents.getEntryByName("Blocks");
        sideComponents = new VoxelComponent[6];
        for (BlockFace blockFace : PrimaryBlockFaces.values())
            sideComponents[blockFace.getId()] = chunkComponents.getEntryByName(blockFace.getName() + " Sides");
        lightingComponent = chunkComponents.getEntryByName("Lighting");
        
        coordinateSystem = world.getChunkMap().getCoordinateSystem();
        CHUNK_DATA_SIDE_LENGTH = coordinateSystem.getBlockEdgeLengthOfChunk();
        int blockDataPerChunk = (int) Math.pow(CHUNK_DATA_SIDE_LENGTH, 3);
        
        CHUNK_BLOCK_DATA_SIZE = blockDataPerChunk * 7 * Integer.BYTES;
        
        LOADED_CHUNKS_SIDE_LENGTH = 1 << (32 - Integer.numberOfLeadingZeros((RENDER_DISTANCE + 1) * 2 - 1));
        
        MAX_CHUNKS = (int) Math.pow(LOADED_CHUNKS_SIDE_LENGTH, 3);
        
        GIANT_TEXTURE_SIDE_LENGTH = LOADED_CHUNKS_SIDE_LENGTH * coordinateSystem.getBlockEdgeLengthOfChunk();
        
        vertexAllocator = new FirstFitAllocator(0, 1, (allocator, increase) -> {return allocator.getCapacity() + increase;});
        indexAllocator = new FirstFitAllocator(0, 1, (allocator, increase) -> {return allocator.getCapacity() + increase;});
        
        chunkCullingData = DGL.genBufferObject(GL43C.GL_SHADER_STORAGE_BUFFER);
        chunkCullingData.bufferData(new int[(PREV_FRUSTUMS_DATA_SIZE + CHUNK_CULLING_DATA_SIZE*MAX_CHUNKS)/Integer.BYTES], GL_DYNAMIC_DRAW);
        chunkCullingData.bindBufferBase(CHUNK_CULLING_SSBO_BASE);
        
        atomicBuffer = DGL.genBufferObject(GL42C.GL_ATOMIC_COUNTER_BUFFER);
        atomicBuffer.bufferData(Integer.BYTES, GL_DYNAMIC_DRAW); //Literally just one number.
        atomicBuffer.bindBufferBase(ATOMIC_COUNTER_BASE);
        glBindBuffer(GL_PARAMETER_BUFFER_ARB, atomicBuffer.id);
        
        indirectDrawBuffer = DGL.genBufferObject(GL_DRAW_INDIRECT_BUFFER);
        indirectDrawBuffer.bufferData(INDIRECT_DRAW_DATA_SIZE * MAX_CHUNKS, GL_DYNAMIC_DRAW);
        indirectDrawBuffer.bindBuffer();
        glBindBufferBase(GL43C.GL_SHADER_STORAGE_BUFFER, INDIRECT_DRAW_SSBO_BASE, indirectDrawBuffer.id);
        
        chunkBlockData = DGL.genBufferObject(GL43C.GL_SHADER_STORAGE_BUFFER);
        chunkBlockData.bufferData(((long)CHUNK_BLOCK_DATA_SIZE * MAX_CHUNKS), GL_DYNAMIC_DRAW);
        chunkBlockData.bindBufferBase(CHUNK_BLOCK_DATA_SSBO_BASE);
    }

    @Override
    public void beginVariables(int vertices, int indices) {
        super.beginVariables(vertices, indices);
        bufferVNormal = vertexBuilder.vec3("in_normal");
        buffer_subsurface_palette_index = vertexBuilder.aint("in_subsurface_texture_id");
        buffer_surface_palette_index = vertexBuilder.aint("in_surface_texture_id");
        buffer_transparent = vertexBuilder.aint("in_transparent");
    }
    
    public void generateAmbientlightingTextures() {
        ambientLightingTexture1 = DGL.genTex3D();
        ambientLightingTexture2 = DGL.genTex3D();

        ambientLightingTexture1.bind(GL13C.GL_TEXTURE1);
        ambientLightingTexture1.parami(GL12C.GL_TEXTURE_WRAP_R, GL11C.GL_REPEAT);
        ambientLightingTexture1.parami(GL11C.GL_TEXTURE_WRAP_S, GL11C.GL_REPEAT);
        ambientLightingTexture1.parami(GL11C.GL_TEXTURE_WRAP_T, GL11C.GL_REPEAT);

        ambientLightingTexture2.bind(GL13C.GL_TEXTURE2);
        ambientLightingTexture2.parami(GL12C.GL_TEXTURE_WRAP_R, GL11C.GL_REPEAT);
        ambientLightingTexture2.parami(GL11C.GL_TEXTURE_WRAP_S, GL11C.GL_REPEAT);
        ambientLightingTexture2.parami(GL11C.GL_TEXTURE_WRAP_T, GL11C.GL_REPEAT);
    }
    
    public void imageAmbientLightingTextures(int length) {
        ambientLightingTexture1.image(length, length, length, GL30C.GL_RGBA8);
        ambientLightingTexture2.image(length, length, length, GL30C.GL_RGBA8);
    }
    
    @Override
    public void init() {
        chunkCullingShader.init();
        beginVariables(105070, 157600);
        begin();
        upload();
        
        generateAmbientlightingTextures();
        
        imageAmbientLightingTextures(GIANT_TEXTURE_SIDE_LENGTH);
    }
    
    public void vertex(int subsurface_id, int surface_id, int transparent, Vec3 vPos, Vec3 vNormal, Vec2 vTexCoord) {
        bufferVNormal.set(vNormal);
        buffer_subsurface_palette_index.x = subsurface_id; 
        buffer_surface_palette_index.x = surface_id; 
        buffer_transparent.x = transparent; 
        super.vertex(vPos, vTexCoord);
    }
    
    public void copyAndReUploadVertices(int allocationOffset, int allocationLength, ByteBuffer buffer) {
        int vertexSize = vertexBuilder.vertexSize();
        
        int prevBinding = glGetInteger(GL_ARRAY_BUFFER_BINDING);
        glBindBuffer(GL_ARRAY_BUFFER, vertexBuilder.vbo());
        int offset = vertexSize * allocationOffset;
        nglBufferSubData(GL_ARRAY_BUFFER, offset, allocationLength * vertexSize, memAddress0(buffer));
        glBindBuffer(GL_ARRAY_BUFFER, prevBinding);
    }
    
    public void copyAndReUploadIndices(int allocationOffset, int allocationLength, ByteBuffer buffer) {
        int prevBinding = glGetInteger(GL_ELEMENT_ARRAY_BUFFER_BINDING);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vertexBuilder.ibo());
        int offset = allocationOffset * Integer.BYTES;
        nglBufferSubData(GL_ELEMENT_ARRAY_BUFFER, offset, allocationLength * Integer.BYTES, memAddress0(buffer));
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, prevBinding);
    }
    
    private void copyAndUploadChunkCulling(int allocationOffset, ByteBuffer buffer) {
        int offset = PREV_FRUSTUMS_DATA_SIZE + allocationOffset * CHUNK_CULLING_DATA_SIZE;
        chunkCullingData.bufferSubData(offset, CHUNK_CULLING_DATA_SIZE, buffer);
    }
    
    private void copyAndUploadPrevFrustum(int allocationOffset, ByteBuffer buffer) {
        int offset = allocationOffset * FRUSTUM_DATA_SIZE;
        chunkCullingData.bufferSubData(offset, FRUSTUM_DATA_SIZE, buffer);
    }
    
    private void copyAndUploadPrevFrustum(int allocationOffset, Frustum frustum) {
        ByteBuffer buffer = MemoryUtil.memAlloc(FRUSTUM_DATA_SIZE);
        
        Vec4[] planes = frustum.getPlanes();
        
        for (Vec4 vec4 : planes)
            vec4.write(buffer);
        
        copyAndUploadPrevFrustum(allocationOffset, buffer);
        
        MemoryUtil.memFree(buffer);
    }
    
    private int getChunkIndex(Vec3i coordinates) {
        coordinates.div(coordinateSystem.getBlockEdgeLengthOfChunk());
        
        coordinates.x = coordinates.x & LOADED_CHUNKS_SIDE_LENGTH - 1;
        coordinates.y = coordinates.y & LOADED_CHUNKS_SIDE_LENGTH - 1;
        coordinates.z = coordinates.z & LOADED_CHUNKS_SIDE_LENGTH - 1;
    
        return coordinates.x * LOADED_CHUNKS_SIDE_LENGTH * LOADED_CHUNKS_SIDE_LENGTH + coordinates.y * LOADED_CHUNKS_SIDE_LENGTH + coordinates.z;
    }
    
    public void copyAndUploadChunkCulling(Box3 aabb, Vec3i coordinates, int commandStart, int commandLength, int vertexOffset) {
        ByteBuffer cullingBuffer = MemoryUtil.memAlloc(CHUNK_CULLING_DATA_SIZE);
        
        cullingBuffer.putFloat(aabb.min.x);
        cullingBuffer.putFloat(aabb.min.y);
        cullingBuffer.putFloat(aabb.min.z);
        
        cullingBuffer.putFloat(aabb.max.x);
        cullingBuffer.putFloat(aabb.max.y);
        cullingBuffer.putFloat(aabb.max.z);
        
        cullingBuffer.putInt(commandLength);
        cullingBuffer.putInt(commandStart);
        cullingBuffer.putInt(vertexOffset);
        
        for (int i = 0; i < 64; i++)
            cullingBuffer.putInt(0);
        
        copyAndUploadChunkCulling(getChunkIndex(coordinates), cullingBuffer);
        
        MemoryUtil.memFree(cullingBuffer);
    }
    
    private void copyAndUploadChunkBlockData(int allocationOffset, ByteBuffer buffer) {
        long offset = (long) allocationOffset * CHUNK_BLOCK_DATA_SIZE;
        chunkBlockData.bufferSubData(offset, CHUNK_BLOCK_DATA_SIZE, buffer);
    }
    
    public void copyAndUploadChunkBlockData(Chunk chunk) {
        ByteBuffer blockDataBuffer = MemoryUtil.memAlloc(CHUNK_BLOCK_DATA_SIZE);
        
        for (int i = 0; i < CHUNK_DATA_SIDE_LENGTH; i++)
            for (int j = 0; j < CHUNK_DATA_SIDE_LENGTH; j++)
                for (int k = 0; k < CHUNK_DATA_SIDE_LENGTH; k++)
                    blockDataBuffer.putInt(chunk.get(blocksComponent, i, j, k).getTextureDefinitionID());
        for (VoxelComponent<Side> sideComponent : sideComponents)
            for (int i = 0; i < CHUNK_DATA_SIDE_LENGTH; i++)
                for (int j = 0; j < CHUNK_DATA_SIDE_LENGTH; j++)
                    for (int k = 0; k < CHUNK_DATA_SIDE_LENGTH; k++)
                        blockDataBuffer.putInt(chunk.get(sideComponent, i, j, k).getTextureDefinitionID());
        
        copyAndUploadChunkBlockData(getChunkIndex(chunk.getCoordinates()), blockDataBuffer);
        
        MemoryUtil.memFree(blockDataBuffer);
    }
    
    public void subImageAmbientLightingTexture1(Image image, int xoffset, int yoffset, int depth) {
        ambientLightingTexture1.subimage(image, xoffset, yoffset, depth);
    }
    
    public void subImageAmbientLightingTexture2(Image image, int xoffset, int yoffset, int depth) {
        ambientLightingTexture2.subimage(image, xoffset, yoffset, depth);
    }
    
    public void copyAndUploadChunkLightingData(Chunk chunk) {
        Vec3i coordinates = chunk.getCoordinates();
        
        coordinates.x = coordinates.x & GIANT_TEXTURE_SIDE_LENGTH - 1;
        coordinates.y = coordinates.y & GIANT_TEXTURE_SIDE_LENGTH - 1;
        coordinates.z = coordinates.z & GIANT_TEXTURE_SIDE_LENGTH - 1;
        
        int length = chunk.getEdgeLength();
        Image image = DGL.genImage(length, length, 4, Util.PrimType.BYTE);
        for (int depth = 0; depth < length; depth++) {
            final int z = depth;
            image.shade((int x, int y, int band) -> {
                y = length - y - 1;
                Long lighting = chunk.get(lightingComponent, x, y, z);
                if (band == 0)
                    return (byte) 0 | PrimaryLightingChannels.RED.getValue(lighting);
                if (band == 1)
                    return (byte) 0 | PrimaryLightingChannels.BLUE.getValue(lighting);
                if (band == 2)
                    return (byte) 0 | PrimaryLightingChannels.GREEN.getValue(lighting);
                if (band == 3)
                    return (byte) 0 | PrimaryLightingChannels.VERTICAL.getValue(lighting);
                else
                    throw new IllegalArgumentException("Asked for Band: " + band);
            });
            subImageAmbientLightingTexture1(image, coordinates.x, coordinates.y, coordinates.z + depth);
        }
        for (int depth = 0; depth < length; depth++) {
            final int z = depth;
            image.shade((int x, int y, int band) -> {
                y = length - y - 1;
                Long lighting = chunk.get(lightingComponent, x, y, z);
                if (band == 0)
                    return (byte) 0 | PrimaryLightingChannels.NORTH_DIAGONAL.getValue(lighting);
                if (band == 1)
                    return (byte) 0 | PrimaryLightingChannels.EAST_DIAGONAL.getValue(lighting);
                if (band == 2)
                    return (byte) 0 | PrimaryLightingChannels.SOUTH_DIAGONAL.getValue(lighting);
                if (band == 3)
                    return (byte) 0 | PrimaryLightingChannels.WEST_DIAGONAL.getValue(lighting);
                else
                    throw new IllegalArgumentException("Asked for Band: " + band);
            });
            subImageAmbientLightingTexture2(image, coordinates.x, coordinates.y, coordinates.z + depth);
        }
        DGL.delete(image);
    }
    
    public void copyAndUploadChunkData(ChunkSurfaceBufferManager chunkManager) {
        Allocation vertexAllocation = chunkManager.getVertexAllocation();
        Allocation indexAllocation = chunkManager.getIndexAllocation();
        
        copyAndReUploadVertices(vertexAllocation.getOffset(), chunkManager.getNumVertices(), chunkManager.getVertexBuffer());
        
        copyAndReUploadIndices(indexAllocation.getOffset(), chunkManager.getNumIndices(), chunkManager.getIndexBuffer());
        
        copyAndUploadChunkCulling(chunkManager.getAabb(), chunkManager.getChunk().getCoordinates(), indexAllocation.getOffset(), chunkManager.getNumIndices(), vertexAllocation.getOffset());
    }

    public void clearChunkData(Vec3i coordinates) {
        copyAndUploadChunkCulling(new Box3(), new Vec3i(coordinates), 0, 0, 0);
        
        coordinates.x = coordinates.x & GIANT_TEXTURE_SIDE_LENGTH - 1;
        coordinates.y = coordinates.y & GIANT_TEXTURE_SIDE_LENGTH - 1;
        coordinates.z = coordinates.z & GIANT_TEXTURE_SIDE_LENGTH - 1;
        
        int length = coordinateSystem.getBlockEdgeLengthOfChunk();
        Image image = DGL.genImage(length, length, 4, Util.PrimType.BYTE);
        for (int depth = 0; depth < length; depth++) {
            image.shade((int x, int y, int band) -> {
                return 0;
            });
            subImageAmbientLightingTexture1(image, coordinates.x, coordinates.y, coordinates.z + depth);
        }
        for (int depth = 0; depth < length; depth++) {
            image.shade((int x, int y, int band) -> {
                return 0;
            });
            subImageAmbientLightingTexture2(image, coordinates.x, coordinates.y, coordinates.z + depth);
        }
        DGL.delete(image);
    }
    
    public void prepShader(ShaderProgram shader) {
        shader.uniformMat4("u_model_matrix", Mat4.identity());
        if (temporalIndex > 0)
            shader.uniform1i("u_temporal_index", temporalIndex);
    }

    public void draw(ShaderProgram shader, Frustum frustum) {
        if (Objects.isNull(shader)) {
            shader = chunkSurfaceShader.getShader();
            temporalIndex = 0;
        } else {
            temporalIndex++;
        }
        
        chunkCullingShader.render();
        
        chunkCullingShader.getShader().uniformVec4v("u_view_frustum", frustum.getPlanes());
        chunkCullingShader.getShader().uniform1i("u_temporal_index", temporalIndex);
        
        atomicBuffer.bufferData(new int[]{0}, GL_DYNAMIC_DRAW);
        
        GL43C.glDispatchCompute(MAX_CHUNKS, 1, 1);
        
        copyAndUploadPrevFrustum(temporalIndex, frustum);
        
        shader.use();
        
        prepShader(shader);
        
        GL11C.glDisable(GL11C.GL_CULL_FACE);
        
        DGL.draw(vertexBuilder, () -> ARBIndirectParameters.glMultiDrawElementsIndirectCountARB(GL11C.GL_TRIANGLES, GL_UNSIGNED_INT, 0, 0, MAX_CHUNKS, 0));
    }
    
    public void destroyAmbientLightingTextures() {
        if (Objects.nonNull(ambientLightingTexture1)) {
            DGL.delete(ambientLightingTexture1);
            ambientLightingTexture1 = null;
            DGL.delete(ambientLightingTexture2);
            ambientLightingTexture2 = null;
        }
    }
    
    @Override
    public void destroy() {
        super.destroy();
        chunkCullingShader.destroy(false);
        destroyAmbientLightingTextures();
        DGL.delete(chunkCullingData, atomicBuffer, indirectDrawBuffer, chunkBlockData);
    }
}