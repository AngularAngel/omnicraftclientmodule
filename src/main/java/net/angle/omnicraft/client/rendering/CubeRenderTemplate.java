package net.angle.omnicraft.client.rendering;

import com.samrj.devil.math.Vec2;
import com.samrj.devil.math.Vec3;
import lombok.NoArgsConstructor;
import net.angle.omnirendering.api.RenderComponent;
import net.angle.omnirendering.api.RenderTemplate;
import net.angle.omniworld.api.space.BlockFace;
import net.angle.omniworld.impl.space.PrimaryAxes;
import net.angle.omniworld.impl.space.PrimaryBlockFaces;
/**
 *
 * @author angle
 */
@NoArgsConstructor
public class CubeRenderTemplate implements RenderTemplate {
    
    @Override
    public RenderComponent getComponent(Object[] info) {
        EntitySurfaceBufferManager surfaceBuffer = new EntitySurfaceBufferManager();
        
        surfaceBuffer.deleteVertices();
        surfaceBuffer.beginVariables();
        surfaceBuffer.begin();
        
        float size = (float) info[0];
        
        for (BlockFace face : PrimaryBlockFaces.values())
            surfaceBuffer.bufferSurface(face, face.modifyDrawStart(new Vec3(-0.5f * size), size), new Vec2(size), 0, 1, 0, face.getPosition().getAxis() == PrimaryAxes.Z ? new Vec2(0.5f, 0) : new Vec2());
        
        surfaceBuffer.finish();
        
        return surfaceBuffer;
    }
}