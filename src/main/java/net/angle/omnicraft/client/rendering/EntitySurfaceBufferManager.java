package net.angle.omnicraft.client.rendering;

/**
 *
 * @author angle
 */
public class EntitySurfaceBufferManager extends AbstractSurfaceBufferManager {


    @Override
    public String toString() {
        return "EntitySurface(Indices: " + getNumIndices() + ")";
    }
}