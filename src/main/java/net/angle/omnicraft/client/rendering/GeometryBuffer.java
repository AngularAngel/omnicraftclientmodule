/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.angle.omnicraft.client.rendering;

import com.samrj.devil.game.GameWindow;
import com.samrj.devil.gl.BufferObject;
import com.samrj.devil.gl.DGL;
import com.samrj.devil.math.Vec2i;
import net.angle.omnirendering.api.RenderComponent;
import net.angle.omnirendering.impl.BasicTextureFrameBufferManager;
import org.lwjgl.opengl.GL11C;
import static org.lwjgl.opengl.GL30C.*;
import org.lwjgl.opengl.GL42C;
import org.lwjgl.opengl.GL43C;

/**
 * This is used in deferred rendering. First we render a bunch of data to this buffer, then we use it in our lighting pass.
 * @author angle
 */
public class GeometryBuffer extends BasicTextureFrameBufferManager {
    private final BufferObject atomicBuffer;
    private final BufferObject headerBuffer;
    private final BufferObject fragmentBuffer;
    
    //size of depth + (normal) + (textureIDs + texelCoords) + nextID;
    private static final int FRAG_NODE_SIZE = Float.BYTES + (3 * Float.BYTES) + (2 * 2 * Integer.BYTES) + Integer.BYTES;
    private static final int FRAG_NODE_LAYERS = 16;
    private static final int ATOMIC_COUNTER_BASE = 0; //First atomic counter
    private static final int HEADER_SSBO_BASE = 0;    //First SSBO
    private static final int FRAGMENT_SSBO_BASE = 1;  //Second SSBO
    
    public final RenderComponent drawStarter = new RenderComponent() {
        @Override
        public void render() {
            drawToFBO();
            GL11C.glDepthMask(true);

            GL11C.glClear(GL11C.GL_COLOR_BUFFER_BIT | GL11C.GL_DEPTH_BUFFER_BIT);
        }
    };
    
    public final RenderComponent drawFinisher = new RenderComponent() {
        @Override
        public void render() {
            bindTextures();
            readFBO();
        }
    };
    
    public GeometryBuffer() {
        super(new String[]{"u_depth_tex", "u_normal_tex", "u_texture_info_tex"},
              new String[]{null, "out_normal", "out_texture_info"},
              new int[]{-1, 0, 1},
              new int[]{GL_TEXTURE6, GL_TEXTURE7, GL_TEXTURE8},
              new int[]{GL_DEPTH_COMPONENT32, GL_RGB10, GL_RGBA8I},
              new int[]{GL_DEPTH_ATTACHMENT, GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1});

        getFBO().drawBuffers(GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1);
        
        atomicBuffer = DGL.genBufferObject(GL42C.GL_ATOMIC_COUNTER_BUFFER);
        atomicBuffer.bufferData(4, GL_DYNAMIC_DRAW); //Literally just one number.
        atomicBuffer.bindBufferBase(ATOMIC_COUNTER_BASE);
        
        headerBuffer = DGL.genBufferObject(GL43C.GL_SHADER_STORAGE_BUFFER);
        
        fragmentBuffer = DGL.genBufferObject(GL43C.GL_SHADER_STORAGE_BUFFER);
        Vec2i resolution = GameWindow.getResolution();
        
        bufferSSBOData(resolution.x, resolution.y);
        headerBuffer.bindBufferBase(HEADER_SSBO_BASE);
        fragmentBuffer.bindBufferBase(FRAGMENT_SSBO_BASE);
    }
    
    public GeometryBuffer(int width, int height) {
        this();
        
        resize(width, height);
    }
    
    public GeometryBuffer(Vec2i resolution) {
        this(resolution.x, resolution.y);
    }
    
    private void bufferSSBOData(int width, int height) {
        headerBuffer.bufferData(Integer.BYTES * width * height, GL_DYNAMIC_DRAW);
        fragmentBuffer.bufferData(FRAG_NODE_SIZE * width * height * FRAG_NODE_LAYERS, GL_DYNAMIC_DRAW);
    }
    
    @Override
    public final void resize(int width, int height) {
        bufferSSBOData(width, height);
        super.resize(width, height);
    }
    
    @Override
    public void drawToFBO() {
        atomicBuffer.bufferData(new int[]{0}, GL_DYNAMIC_DRAW);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);

        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);
        
        super.drawToFBO();
    }
    
    @Override
    public void destroy() {
        DGL.delete(atomicBuffer, headerBuffer, fragmentBuffer);
        super.destroy();
    }
}