package net.angle.omnicraft.client.rendering;

import com.samrj.devil.geo3d.Frustum;
import com.samrj.devil.gl.ShaderProgram;
import com.samrj.devil.math.Mat3;
import com.samrj.devil.math.Vec2;
import com.samrj.devil.math.Vec2i;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import net.angle.omnicraft.client.OmnicraftClient;
import net.angle.omnicraft.client.rendering.components.ChunkRenderer;
import net.angle.omnirendering.api.ComponentRenderingManager;
import net.angle.omnirendering.api.FrameBufferManager;
import net.angle.omnirendering.api.TextureVertexManager2D;
import net.angle.omnirendering.impl.BasicTextureVertexManager2D;
import org.lwjgl.opengl.GL11C;

/**
 *
 * @author angle
 */
@RequiredArgsConstructor
public class LightingManager {
    private final OmnicraftClient client;
    private final @Getter ComponentRenderingManager surfaceRenderer;
    private @Setter ChunkRenderer chunkRenderer;
    private final FrameBufferManager hdrBuffer;
    private final @Getter Mat3 screenMatrix = new Mat3();
    private TextureVertexManager2D screenBuffer;
    
    public void init() {
        screenBuffer = new BasicTextureVertexManager2D();
    }
    
    public void resize(int width, int height) {
        screenMatrix.setOrthographic(0, width, 0, height);
        
        screenBuffer.deleteVertices();
        
        screenBuffer.beginVariables(4, 6);
        screenBuffer.begin();
        screenBuffer.bufferVertices(new Vec2(-width/2, height/2), 
                                    new Vec2(width/2, -height/2));
        screenBuffer.upload();
    }
    
    public void prepareShadowPass() {
        GL11C.glDisable(GL11C.GL_CULL_FACE);
        GL11C.glEnable(GL11C.GL_DEPTH_TEST);
        GL11C.glDepthFunc(GL11C.GL_LESS);
        GL11C.glDisable(GL11C.GL_BLEND);
        GL11C.glDepthMask(true);
    }
    
    public void prepareLightingPass() {
        hdrBuffer.drawToFBO();
        Vec2i resolution = client.getResolution();
        GL11C.glViewport(0, 0, resolution.x, resolution.y);
        GL11C.glDisable(GL11C.GL_CULL_FACE);
        GL11C.glDisable(GL11C.GL_DEPTH_TEST);
        GL11C.glEnable(GL11C.GL_BLEND);
        GL11C.glBlendFunc(GL11C.GL_ONE, GL11C.GL_ONE);
        GL11C.glDepthMask(false);
    }
    
    public void drawScreenBuffer() {
        screenBuffer.draw();
    }

    public void renderShadows(ShaderProgram shader, Frustum shadowFrustum) {
        chunkRenderer.setVisibleFrustum(shadowFrustum);
        surfaceRenderer.render(shader);
    }
    
    public void destroy() {
        screenBuffer.destroy();
    }
}