/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.angle.omnicraft.client.rendering;

import com.samrj.devil.gl.DGL;
import com.samrj.devil.gl.DGLUtil;
import com.samrj.devil.gl.FBO;
import com.samrj.devil.gl.TextureCubemap;
import com.samrj.devil.math.Mat4;
import com.samrj.devil.math.Quat;
import com.samrj.devil.math.Vec3;
import java.util.function.Consumer;
import net.angle.omnilighting.api.PointLightComponent;
import org.lwjgl.opengl.GL11C;
import org.lwjgl.opengl.GL14C;
import org.lwjgl.opengl.GL30C;
import org.lwjgl.opengl.GL32C;

/**
 *
 * @author angle
 */
public class PointShadowCubeMap {
    
    public static final float Z_NEAR = 1.0f/16.0f;
    
    private int edgeLength;
    
    public final TextureCubemap texture;
    
    private final FBO fbo;
    
    private final Mat4 position = new Mat4();
    private final Mat4 frustum = new Mat4();
    
    private final Face[] faces;
    
    public PointShadowCubeMap(int edgeLength) {
        this.edgeLength = edgeLength;
        
        texture = DGL.genTextureCubemap();
        texture.bind();
        texture.parami(GL30C.GL_TEXTURE_COMPARE_MODE, GL30C.GL_COMPARE_REF_TO_TEXTURE);
        texture.parami(GL14C.GL_TEXTURE_COMPARE_FUNC, GL14C.GL_LEQUAL);
        texture.image(edgeLength, GL14C.GL_DEPTH_COMPONENT32);
        
        fbo = DGL.genFBO();
        DGL.bindFBO(fbo);
        fbo.readBuffer(GL11C.GL_NONE);
        fbo.drawBuffers(GL11C.GL_NONE);
        GL32C.glFramebufferTexture(GL30C.GL_FRAMEBUFFER, GL30C.GL_DEPTH_ATTACHMENT, texture.id, 0);
        
        faces = new Face[6];
        for (int i = 0; i < 6; i++) faces[i] = new Face(i);
    }
    
    public int getResolution() {
        return edgeLength;
    }
    
    public void setResolution(int edgeLength) {
        this.edgeLength = edgeLength;
        texture.image(edgeLength, GL14C.GL_DEPTH_COMPONENT32);
    }
    
    public Mat4[] getMatrices() {
        Mat4[] ret = new Mat4[6];
        for (int i = 0; i < faces.length; i++)
            ret[i] = faces[i].matrix;
        
        return ret;
    }
    
    public void render(PointLightComponent light, Consumer<PointShadowCubeMap> geomDrawFunc) {
        if (edgeLength <= 0) return;
        
        position.setTranslation(Vec3.negate(light.getPosition()));
        frustum.setFrustum(Z_NEAR, Z_NEAR, Z_NEAR, light.getBoundingRadius());
        
        GL11C.glViewport(0, 0, edgeLength, edgeLength);
        
        for (Face face : faces) face.prepareRender();
        
        DGL.bindFBO(fbo);
        geomDrawFunc.accept(this);
    }
    
    public void destroy() {
        DGL.delete(texture, fbo);
        for (Face face : faces) face.destroy();
    }
    
    public class Face {
        public final Mat4 matrix = new Mat4();
        private final Mat4 rotation;
        
        private Face(int face) {
            Quat rot = new Quat();
            DGLUtil.getCubemapFaceDir(face, rot);
            rotation = Mat4.rotation(rot.negate());
        }
        
        private void prepareRender()
        {
            matrix.set(frustum);
            matrix.mult(rotation);
            matrix.mult(position);
        }
        
        private void destroy() {
        }
    }
}