package net.angle.omnicraft.client.rendering.components;

import com.samrj.devil.math.Mat4;
import com.samrj.devil.math.Vec3;
import com.samrj.devil.math.Vec3i;
import lombok.RequiredArgsConstructor;
import net.angle.omnicraft.common.OmnicraftGameWorld;
import net.angle.omnientity.api.Entity;
import net.angle.omnirendering.api.OutlineVertexManager;
import net.angle.omnirendering.api.RenderComponent;
import net.angle.omnirendering.impl.BasicOutlineVertexManager;
import net.angle.omniworld.api.chunks.ChunkMap;
import net.angle.omniworld.api.space.CoordinateSystem;
import org.lwjgl.opengl.GL11C;

/**
 *
 * @author angle
 */
@RequiredArgsConstructor
public class BlockOutlineRenderer implements RenderComponent {
    private final Entity player;
    private final OmnicraftGameWorld world;
    private final OutlineShader outlineShader;
    private final OutlineVertexManager blockOutline = new BasicOutlineVertexManager();
    
    @Override
    public void init() {
        blockOutline.beginVariables(8, 24);
        blockOutline.begin();
        blockOutline.streamCubeOutline(new Vec3(-0.00125f), 0.5f + 0.0025f);
        blockOutline.upload();
    }
    
    @Override
    public void render() {
        Vec3i pickedCoord = world.raycast(player, 20);
        if (pickedCoord != null) {
            ChunkMap chunkMap = world.getChunkMap();
            CoordinateSystem coordinateSystem = chunkMap.getCoordinateSystem();
            Vec3 realCoordinates = new Vec3(pickedCoord);
            realCoordinates.mult(coordinateSystem.getRealEdgeLengthOfBlock());
            outlineShader.getShader().uniformMat4("u_model_matrix", Mat4.translation(realCoordinates));
            blockOutline.draw(GL11C.GL_LINES);
        }
    }

    @Override
    public void destroy(Boolean crashed) {
        blockOutline.destroy();
    }
}