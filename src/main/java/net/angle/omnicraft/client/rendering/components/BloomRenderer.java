package net.angle.omnicraft.client.rendering.components;

import com.samrj.devil.game.GameWindow;
import com.samrj.devil.gl.DGL;
import com.samrj.devil.gl.FBO;
import com.samrj.devil.gl.ShaderProgram;
import com.samrj.devil.gl.Texture2D;
import com.samrj.devil.math.Vec2i;
import net.angle.omnicraft.client.OmnicraftClient;
import net.angle.omnicraft.client.rendering.LightingManager;
import net.angle.omnirendering.api.TextureFrameBufferManager;
import net.angle.omnirendering.impl.BasicShaderProgramManager;
import net.angle.omnirendering.impl.BasicTextureFrameBufferManager;
import org.lwjgl.opengl.GL11C;
import static org.lwjgl.opengl.GL11C.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11C.GL_LINEAR;
import static org.lwjgl.opengl.GL11C.GL_TEXTURE_WRAP_S;
import static org.lwjgl.opengl.GL11C.GL_TEXTURE_WRAP_T;
import static org.lwjgl.opengl.GL12C.GL_CLAMP_TO_EDGE;
import static org.lwjgl.opengl.GL13C.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13C.GL_TEXTURE1;
import static org.lwjgl.opengl.GL13C.glActiveTexture;
import org.lwjgl.opengl.GL30C;
import static org.lwjgl.opengl.GL30C.GL_COLOR_ATTACHMENT0;
import static org.lwjgl.opengl.GL30C.GL_DRAW_FRAMEBUFFER;
import static org.lwjgl.opengl.GL30C.GL_READ_FRAMEBUFFER;
import static org.lwjgl.opengl.GL30C.GL_RGB16F;
import static org.lwjgl.opengl.GL30C.glBlitFramebuffer;

/**
 *
 * @author angle
 */
public class BloomRenderer extends BasicShaderProgramManager {
    private static final int MAX_TIERS = 10;
    private static final int SCALE = 4; //Higher is smaller
    
    private static Texture2D genTier(int width, int height) {
        Texture2D tier = DGL.genTex2D().image(width, height, GL_RGB16F);
        tier.bind();
        tier.parami(GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        tier.parami(GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        return tier;
    }
    
    private final OmnicraftClient client;
    private final TextureFrameBufferManager hdrBuffer, tempHDRBuffer;
    private final LightingManager lightingManager;
    
    private int tierCount;
    private final FBO sourceFBO, targetFBO;
    private final Texture2D[] tiers;

    public BloomRenderer(OmnicraftClient client, TextureFrameBufferManager hdrBuffer, LightingManager lightingManager) {
        super("bloom", client.getResourceManager());
        this.client = client;
        this.hdrBuffer = hdrBuffer;
        tempHDRBuffer = new BasicTextureFrameBufferManager("u_temp_hdr_buffer", null, -1, 13, GL30C.GL_RGBA16F, GL30C.GL_COLOR_ATTACHMENT0, hdrBuffer.getWidth(), hdrBuffer.getHeight());
        this.lightingManager = lightingManager;
        sourceFBO = DGL.genFBO();
        targetFBO = DGL.genFBO();
        tiers = new Texture2D[MAX_TIERS];
    }
    
    private void genTiers(int width, int height) {
        tiers[1] = genTier(width/2, height/2);
        for (int i=2; i<MAX_TIERS; i++) {
            Texture2D prev = tiers[i-1];
            tiers[i] = genTier(Math.max(prev.getWidth()/2, 1), Math.max(prev.getHeight()/2, 1));
        }
        updateTierCount(width, height);
    }
    
    private void updateTierCount(int width, int height) {   
        int size = Math.max(width, height);
        tierCount = Math.min(Math.max(Math.round((float)Math.log(size)/(float)Math.log(2.0)) - SCALE, 2), MAX_TIERS);
    }
    
    public void resize(int width, int height) {
        genTiers(width, height);
        tempHDRBuffer.resize(width, height);
    }
    
    @Override
    public void init() {
        DGL.bindFBO(targetFBO);
        targetFBO.drawBuffers(GL_COLOR_ATTACHMENT0);
        
        Vec2i resolution = GameWindow.getResolution();
        genTiers(resolution.x, resolution.y);
        
        super.init();
        ShaderProgram shader = getShader();
        DGL.useProgram(shader);
        shader.uniform1i("u_base_tex", 0);
        shader.uniform1i("u_blur_tex", 1);
        shader.uniform1fv("u_blur_factors", 0.005f, 0.028f, 0.082f, 0.185f, 0.4f, 0.185f, 0.082f, 0.028f, 0.005f);
    }
    
    @Override
    public void render() {
        super.render();
        lightingManager.prepareLightingPass();
        GL11C.glDisable(GL11C.GL_BLEND);
        
        tiers[0] = hdrBuffer.getTexture();
        
        for (int i=1; i<tierCount; i++) {
            Texture2D src = tiers[i - 1], tgt = tiers[i];
            
            DGL.bindFBO(sourceFBO);
            sourceFBO.texture2D(src, GL_COLOR_ATTACHMENT0);
            DGL.bindFBO(targetFBO);
            targetFBO.texture2D(tgt, GL_COLOR_ATTACHMENT0);
            DGL.bindFBO(sourceFBO, GL_READ_FRAMEBUFFER);
            DGL.bindFBO(targetFBO, GL_DRAW_FRAMEBUFFER);
            glBlitFramebuffer(
                    0, 0, src.getWidth(), src.getHeight(),
                    0, 0, tgt.getWidth(), tgt.getHeight(),
                    GL_COLOR_BUFFER_BIT, GL_LINEAR);
        }
        
        tiers[0].bind(GL_TEXTURE0);
        
        for (int i = tierCount - 1; i > 1; i--) {
            tiers[i].bind(GL_TEXTURE1);
            glActiveTexture(GL_TEXTURE0);
            tempHDRBuffer.drawToFBO();
            hdrBuffer.readFBO();
            getShader().uniform1b("u_horizontal", true);
            lightingManager.drawScreenBuffer();

            getShader().uniform1b("u_horizontal", false);
            hdrBuffer.drawToFBO();
            tempHDRBuffer.getTexture().bind(GL_TEXTURE0);
            tempHDRBuffer.readFBO();

            lightingManager.drawScreenBuffer();
        }
    }

    @Override
    public void destroy(Boolean crashed) {
        tempHDRBuffer.destroy();
        DGL.delete(sourceFBO, targetFBO);
        DGL.delete(tiers);
        super.destroy(crashed);
    }
}