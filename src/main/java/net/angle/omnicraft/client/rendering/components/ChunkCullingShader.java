package net.angle.omnicraft.client.rendering.components;

import net.angle.omnicraft.client.OmnicraftClient;
import net.angle.omnirendering.impl.BasicShaderProgramManager;

/**
 *
 * @author angle
 */
public class ChunkCullingShader extends BasicShaderProgramManager {
    
    public ChunkCullingShader(OmnicraftClient client) {
        super("chunk_culling", client.getResourceManager());
    }
}
