package net.angle.omnicraft.client.rendering.components;

import com.samrj.devil.geo3d.Box3;
import com.samrj.devil.geo3d.Frustum;
import com.samrj.devil.gl.ShaderProgram;
import net.angle.omnicraft.client.rendering.ChunkSurfaceBufferManager;
import com.samrj.devil.math.Vec2;
import com.samrj.devil.math.Vec2i;
import com.samrj.devil.math.Vec3;
import com.samrj.devil.math.Vec3i;
import com.samrj.devil.util.alloc.Allocator;
import java.util.HashMap;
import java.util.Objects;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
import lombok.Setter;
import net.angle.omniblocks.api.Block;
import net.angle.omnicraft.common.OmnicraftGameWorld;
import net.angle.omnicraft.client.OmnicraftClient;
import net.angle.omnicraft.client.rendering.ChunkVertexManager;
import net.angle.omniregistry.api.DatumRegistry;
import net.angle.omnirendering.api.OutlineVertexManager;
import net.angle.omnirendering.api.RenderComponent;
import net.angle.omnirendering.impl.BasicOutlineVertexManager;
import net.angle.omnisides.api.Side;
import net.angle.omniworld.api.space.BlockFace;
import net.angle.omniworld.api.chunks.Chunk;
import net.angle.omniworld.api.chunks.ChunkMap;
import net.angle.omniworld.api.chunks.VoxelComponent;
import net.angle.omniworld.api.space.CoordinateSystem;
import net.angle.omniworld.impl.space.PrimaryBlockFaces;

/**
 *
 * @author angle
 */
public class ChunkRenderer implements RenderComponent {
    private final ChunkMap chunkMap;
    private final VoxelComponent<Block> blocksComponent;
    private final VoxelComponent<Side>[] sideComponents;
    private @Setter Frustum visibleFrustum;
    protected final HashMap<Vec3i, ChunkSurfaceBufferManager> surfaceBuffers = new HashMap<>();
    private final BlockingDeque<Chunk> chunksToRender = new LinkedBlockingDeque<>();
    private final BlockingDeque<Chunk> chunksToRenderTextures = new LinkedBlockingDeque<>();
    private final BlockingDeque<Chunk> chunksToUnload = new LinkedBlockingDeque<>();
    private final OutlineVertexManager chunkOutline = new BasicOutlineVertexManager();
    private final ChunkVertexManager chunkVertexManager;
    
    public ChunkRenderer(OmnicraftClient client, OmnicraftGameWorld world, SurfaceGeometryShader shader, Frustum visibleFrustum) {
        super();
        this.chunkMap = world.getChunkMap();
        DatumRegistry<VoxelComponent> chunkComponents = (DatumRegistry<VoxelComponent>) world.getRegistries().getEntryByName("Chunk Components");
        blocksComponent = chunkComponents.getEntryByName("Blocks");
        sideComponents = new VoxelComponent[6];
        for (BlockFace blockFace : PrimaryBlockFaces.values())
            sideComponents[blockFace.getId()] = chunkComponents.getEntryByName(blockFace.getName() + " Sides");
        this.visibleFrustum = visibleFrustum;
        chunkVertexManager = new ChunkVertexManager(client, world, shader);
    }
    
    public void generateChunkOutline() {
        chunkOutline.beginVariables(8, 36);
        chunkOutline.begin();
        chunkOutline.streamCubeFaces(new Vec3(0), 8);
        chunkOutline.upload();
    }
    
    public void reloadChunk(Chunk chunk) {
        if (!chunksToRender.contains(chunk))
            chunksToRender.add(chunk);
    }

    @Override
    public void init() {
        chunkVertexManager.init();
        
        generateChunkOutline();
        
        chunkMap.addChunkAdditionCallback((chunk) -> {
            if (chunksToUnload.contains(chunk))
                chunksToUnload.remove(chunk);
            chunksToRender.add(chunk);
        });
        chunkMap.addChunkUpdateCallback((chunk) -> {
            reloadChunk(chunk);
        });
        chunkMap.addChunkRemovalCallback((chunk) -> {
            if (chunksToRender.contains(chunk))
                chunksToRender.remove(chunk);
            chunksToUnload.add(chunk);
        });
    }

    @Override
    public void render() {
        this.render(null);
    }

    @Override
    public void render(ShaderProgram shader) {
        if (Objects.isNull(shader)) {
            for (int i = 0; i < 16 && !chunksToRender.isEmpty(); i++)
                renderChunk(chunksToRender.poll());
            
            for (int i = 0; i < 16 && !chunksToUnload.isEmpty(); i++)
                unloadChunk(chunksToUnload.poll());

            for (int i = 0; i < 16 && !chunksToRenderTextures.isEmpty(); i++) {
                Chunk chunk = chunksToRenderTextures.poll();
                bufferAmbientLightingTexture(chunk);
                bufferBlockAndSideData(chunk);
            }
        }
        
        chunkVertexManager.draw(shader, visibleFrustum);
    }
    
    public void copyAndUploadChunkData(ChunkSurfaceBufferManager chunkManager) {
        chunkVertexManager.copyAndUploadChunkData(chunkManager);
    }
    
    public VoxelComponent<Side> getSideComponent(BlockFace face) {
        return sideComponents[face.getId()];
    }
    
    public boolean faceIsVisible(Block block, BlockFace face, Chunk container, Vec3i coord) {
        return sideIsVisible(block, face, container, coord);
    }
    
    public boolean sideIsVisible(Block block, BlockFace face, Chunk container, Vec3i coord) {
        return block.isVisibleThrough(container.get(blocksComponent, face.getPosition().step(new Vec3i(coord))));
    }
    
    private boolean checkMesh(Chunk chunk, BlockFace face, Vec3i coord, int width, int height, boolean transparent) {
        //Goes down relative to the block face, not the chunks y coordinate
        Vec3i workingCoordy = new Vec3i(coord);
        for (int i = 0; i < height; i++) {
            
            //Goes across relative to the block face, not the chunks x coordinate
            Vec3i workingCoordx = new Vec3i(workingCoordy);
            for (int j = 0; j < width; j++) {
                Block block = chunk.get(blocksComponent, workingCoordx);
                if (!block.isDrawable() ||
                        !faceIsVisible(block, face, chunk, workingCoordx) || block.isTransparent() != transparent) {
                    return false;
                }
                face.moveAcross(workingCoordx);
            }
            face.moveDown(workingCoordy);
        }
        return true;
    }
    
    private Vec2i greedyMeshExpansion(ChunkSurfaceBufferManager voxelBuffer, BlockFace face, Chunk chunk, Vec3i coord, boolean[][] meshed, int meshx, int meshy) {
        Block block = chunk.get(blocksComponent, coord);
        
        if (Objects.isNull(block) || !block.isDrawable() || !faceIsVisible(block, face, chunk, coord))
            return new Vec2i(1, 1);
        
        boolean expandDown = true, expandAcross = true;
        
        int width = 1, height = 1;
        
        Vec3i workingCoord = new Vec3i(coord);
        int workingEdgeLength = chunk.getEdgeLength() - 1;
        
        while (expandAcross && face.continueAcross(workingCoord, workingEdgeLength)) {
            face.moveAcross(workingCoord);
            if (!meshed[meshy + height - 1][meshx + width] && checkMesh(chunk, face, workingCoord, 1, height, block.isTransparent())) {
                width++;
            } else
                expandAcross = false;
        }
        
        workingCoord = new Vec3i(coord);
        
        while (expandDown && face.continueDown(workingCoord, workingEdgeLength)) {
            face.moveDown(workingCoord);
            if (!meshed[meshy + height][meshx + width - 1] && checkMesh(chunk, face, workingCoord, width, 1, block.isTransparent())) {
                height++;
            } else
                expandDown = false;
        }
        
        CoordinateSystem coordinateSystem = chunk.getChunkMap().getCoordinateSystem();
        voxelBuffer.bufferBlockFace(block.isTransparent() ? 1 : 0, new Vec3(chunk.getCoordinates()).add(new Vec3(coord)), face, new Vec2(width, height), 
                coordinateSystem.getBlockEdgeLengthOfChunk() + 2,
                coordinateSystem.getRealEdgeLengthOfBlock());
        
        return new Vec2i(width, height);
    }
    
    private void optimizeMeshFor(ChunkSurfaceBufferManager voxelBuffer, boolean[][] meshed, int j, int k, BlockFace face, Chunk chunk, Vec3i coord) {
        if (meshed[j][k] == false) {
            Vec2i dimensions = greedyMeshExpansion(voxelBuffer, face, chunk, coord, meshed, k, j);

            for (int l = 0; l < dimensions.y; l++) {
                for (int m = 0; m < dimensions.x; m++) {
                    meshed[j+l][k+m] = true;
                }
            }
        }
    }
    
    private void optimizeMeshes(ChunkSurfaceBufferManager voxelBuffer, BlockFace face, Chunk chunk) {
        int edgeLength = chunk.getEdgeLength();
        
        Vec3i coord1 = face.getStartingPosition();
        coord1.mult(edgeLength - 1);
        
        for (int i = 0; i < edgeLength; i++) {
            boolean[][] meshed = new boolean[edgeLength][edgeLength];
            
            face.iterateOver(edgeLength, (j, k) -> {
                Vec3i coord2 = new Vec3i(coord1);
                face.moveAcross(coord2, k);
                face.moveDown(coord2, j);
                
                optimizeMeshFor(voxelBuffer, meshed, j, k, face, chunk, coord2);
            });
            if (chunk.isHomogenous(blocksComponent))
                return;
            face.moveIn(coord1);
        }
    }
    
    //Buffers the optimized meshes for a chunk.
    private void bufferMeshes(ChunkSurfaceBufferManager voxelBuffer, Chunk chunk) {
        for (BlockFace face : PrimaryBlockFaces.values()) {
            optimizeMeshes(voxelBuffer, face, chunk);
        }
    }
    
    public void updateAdjacentChunks(Vec3i coordinates) {
        CoordinateSystem coordinateSystem = chunkMap.getCoordinateSystem();
        chunkMap.forEachChunkWithin(Vec3i.sub(coordinates, new Vec3i(coordinateSystem.getBlockEdgeLengthOfChunk())), new Vec3i(coordinateSystem.getBlockEdgeLengthOfChunk() * 3),
                (x, y, z) -> {
            Chunk adjacentChunk = chunkMap.getChunk(x, y, z);
            if (Objects.nonNull(adjacentChunk) && !chunksToRenderTextures.contains(adjacentChunk))
                chunksToRenderTextures.add(adjacentChunk);
        });
    }
    
    public void renderChunk(Chunk chunk) {
        Vec3i coordinates = chunk.getCoordinates();
        ChunkSurfaceBufferManager surfaceBuffer = surfaceBuffers.get(coordinates);
        if (Objects.isNull(surfaceBuffer)) {
            surfaceBuffer = new ChunkSurfaceBufferManager(this, chunk);
            surfaceBuffers.put(coordinates, surfaceBuffer);
        }
        surfaceBuffer.deleteVertices();
        surfaceBuffer.beginVariables();
        surfaceBuffer.begin();
        
        bufferMeshes(surfaceBuffer, chunk);
        
        CoordinateSystem coordinateSystem = chunkMap.getCoordinateSystem();
            
        if (Objects.isNull(surfaceBuffer.getAabb())) {
            int blockEdgeLengthOfChunk = coordinateSystem.getBlockEdgeLengthOfChunk();
            float realEdgeLengthOfBlock = coordinateSystem.getRealEdgeLengthOfBlock();
            Vec3 min = new Vec3(coordinates).mult(realEdgeLengthOfBlock);
            surfaceBuffer.setAabb(new Box3(min, Vec3.add(min, new Vec3(blockEdgeLengthOfChunk * realEdgeLengthOfBlock))));
        }
        
        surfaceBuffer.finish();
        
        updateAdjacentChunks(coordinates);
    }
    
    public void unloadChunk(Chunk chunk) {
        Vec3i coordinates = chunk.getCoordinates();
        chunkVertexManager.clearChunkData(coordinates);
        ChunkSurfaceBufferManager bufferManager = surfaceBuffers.get(coordinates);
        if (Objects.nonNull(bufferManager)) {
            bufferManager.destroy();
            surfaceBuffers.remove(coordinates, bufferManager);
        }
        
        updateAdjacentChunks(coordinates);
    }
    
    public void bufferAmbientLightingTexture(Chunk chunk) {
        chunkVertexManager.copyAndUploadChunkLightingData(chunk);
    }
    
    public void bufferBlockAndSideData(Chunk chunk) {
        ChunkSurfaceBufferManager surfaceBuffer = surfaceBuffers.get(chunk.getCoordinates());
        if (Objects.isNull(surfaceBuffer) || !surfaceBuffer.isDrawable())
            return;
        
        if (Objects.isNull(surfaceBuffer.getAabb())) {
            Vec3i coordinates = chunk.getCoordinates();
            CoordinateSystem coordinateSystem = chunkMap.getCoordinateSystem();
            int blockEdgeLengthOfChunk = coordinateSystem.getBlockEdgeLengthOfChunk();
            float realEdgeLengthOfBlock = coordinateSystem.getRealEdgeLengthOfBlock();
            Vec3 min = new Vec3(coordinates).mult(realEdgeLengthOfBlock);
            surfaceBuffer.setAabb(new Box3(min, Vec3.add(min, new Vec3(blockEdgeLengthOfChunk * realEdgeLengthOfBlock))));
        }
        
        chunkVertexManager.copyAndUploadChunkBlockData(surfaceBuffer.getChunk());
    }

    @Override
    public void destroy(Boolean bln) {
        chunkOutline.destroy();
        for (ChunkSurfaceBufferManager voxelBuffer : surfaceBuffers.values())
            voxelBuffer.destroy();
        chunkVertexManager.destroy();
    }

    public Allocator getVertexAllocator() {
        return chunkVertexManager.getVertexAllocator();
    }

    public Allocator getIndexAllocator() {
        return chunkVertexManager.getIndexAllocator();
    }
}