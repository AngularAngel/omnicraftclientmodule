package net.angle.omnicraft.client.rendering.components;

import com.samrj.devil.gl.ShaderProgram;
import lombok.RequiredArgsConstructor;
import net.angle.omnicraft.client.GPUTimer;
import net.angle.omnirendering.api.RenderComponent;

/**
 *
 * @author angle
 */
@RequiredArgsConstructor
public class ComponentTimer implements RenderComponent {
    private GPUTimer timer = new GPUTimer();
    private final String label;
    private final RenderComponent component;
    
    @Override
    public void init() {
        component.init();
    }
    
    @Override
    public void render() {
        timer.start();
        component.render();
        timer.stop(label);
    }
    
    @Override
    public void destroy(Boolean crashed) {
        component.destroy(crashed);
    }
}