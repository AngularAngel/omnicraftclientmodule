package net.angle.omnicraft.client.rendering.components;

import com.samrj.devil.game.GameWindow;
import com.samrj.devil.gui.DUI;
import com.samrj.devil.gui.LayoutColumns;
import com.samrj.devil.gui.LayoutRows;
import com.samrj.devil.gui.Text;
import com.samrj.devil.gui.Window;
import com.samrj.devil.math.Mat4;
import com.samrj.devil.math.Vec3;
import com.samrj.devil.math.Vec3i;
import net.angle.omnicraft.common.OmnicraftGameWorld;
import net.angle.omnientity.api.CameraComponent;
import net.angle.omnientity.api.Entity;
import net.angle.omnientity.api.PositionComponent;
import net.angle.omnilighting.api.PointLightComponent;
import net.angle.omniregistry.api.DatumRegistry;
import net.angle.omnirendering.api.OutlineVertexManager;
import net.angle.omnirendering.api.RenderComponent;
import net.angle.omnirendering.api.VertexShape;
import net.angle.omnirendering.impl.BasicOutlineVertexManager;
import net.angle.omniworld.api.chunks.Chunk;
import net.angle.omniworld.api.chunks.ChunkMap;
import net.angle.omniworld.api.chunks.VoxelComponent;
import net.angle.omniworld.api.space.CoordinateSystem;
import org.lwjgl.opengl.GL11C;

/**
 *
 * @author angle
 */
public class DebugWindow implements RenderComponent{
    private final Window window;
    private final LayoutRows rows;
    private final Text fpsNum;
    private final Text posDisplay;
    private final Text chunkCoordsDisplay;
    private final Text chunkPosDisplay;
    private final Text dirDisplay;
    private final Text lookDisplay;
    private final Text lightingDisplay;
    
    private final Entity player;
    private final PositionComponent positionComponent;
    private final CameraComponent cameraComponent;
    private final OmnicraftGameWorld world;
    private final VertexShape icosahedron;
    private final OutlineVertexManager chunkOutline = new BasicOutlineVertexManager();
    private final OutlineShader outlineShader;
    
    private int framesSinceUpdate;
    private float timeSinceUpdate;
    
    public DebugWindow(Entity player, OmnicraftGameWorld world, VertexShape icosahedron, OutlineShader outlineShader) {
        this.player = player;
        this.world = world;
        this.icosahedron = icosahedron;
        this.outlineShader = outlineShader;
        
        positionComponent = player.getComponent(PositionComponent.class);
        cameraComponent = player.getComponent(CameraComponent.class);
        
        window = new Window();
        window.setTitle("Debug Window");
        window.setCloseButtonVisible(false);
        rows = new LayoutRows();
        window.setContent(rows);
        
        fpsNum = addDisplayItem("FPS: ");
        posDisplay = addDisplayItem("POS: ");
        chunkCoordsDisplay = addDisplayItem("CHUNK COORDS: ");
        chunkPosDisplay = addDisplayItem("CHUNK POS: ");
        dirDisplay = addDisplayItem("DIR: ");
        lookDisplay = addDisplayItem("LOOK: ");
        lightingDisplay = addDisplayItem("LIGHT: ");
    }
    

    @Override
    public void init() {
        chunkOutline.beginVariables(8, 24);
        chunkOutline.begin();
        chunkOutline.streamCubeOutline(new Vec3(0.0125f), 8 - 0.025f);
        chunkOutline.upload();
    }
    
    
    public Text addDisplayItem(String name) {
        LayoutColumns columns = new LayoutColumns();
        rows.add(columns);
        
        Text title = new Text(name);
        columns.add(title);
        
        Text display = new Text("");
        columns.add(display);
        
        return display;
    }
    
    public boolean isVisible() {
        return window.isVisible();
    }
    
    public void toggle() {
        if (isVisible())
            DUI.hide(window);
        else
            DUI.show(window);
    }
    
    public void update() {
        if (!isVisible())
            return;
        
        Vec3 playerPosition = positionComponent.getPosition();
        
        fpsNum.setText("" + framesSinceUpdate * 2);
        posDisplay.setText(playerPosition.toString());
        ChunkMap chunkMap = world.getChunkMap();
        CoordinateSystem coordinateSystem = chunkMap.getCoordinateSystem();
        int[] coords = coordinateSystem.getChunkAndVoxelCoordsFromRealCoords(playerPosition);
        chunkCoordsDisplay.setText(new Vec3i(coords[0], coords[1], coords[2]).toString());
        chunkPosDisplay.setText(new Vec3i(coords[3], coords[4], coords[5]).toString());
        dirDisplay.setText(cameraComponent.getCamera().forward.toString());
        
        Vec3i pickedCoord = world.raycast(player, 20);
        if (pickedCoord != null)
            lookDisplay.setText(pickedCoord.toString());
        else
            lookDisplay.setText("None");
        
        Chunk chunk = chunkMap.getChunk(coords[0], coords[1], coords[2]);
        VoxelComponent<Long> lightingComponent = ((DatumRegistry<VoxelComponent>) world.getRegistries().getEntryByName("Chunk Components")).getEntryByName("Lighting");
        
        if (chunk != null) {
            lightingDisplay.setText(Long.toHexString((long) chunk.get(lightingComponent, coords[3], coords[4], coords[5])));
        } else {
            lightingDisplay.setText("No Chunk!");
        }
        
        window.setSizeFromContent();
        
        framesSinceUpdate = 0;
        timeSinceUpdate = 0f;
    }
    
    public void step(float dt) {
        if (timeSinceUpdate >= 0.5f)
            update();
        
        timeSinceUpdate += dt;
    }
    
    @Override
    public void render() {
        if (isVisible()) {
            framesSinceUpdate++;
            ChunkMap chunkMap = world.getChunkMap();
            CoordinateSystem coordinateSystem = chunkMap.getCoordinateSystem();
            Vec3 playerPosition = positionComponent.getPosition();
            int[] coords = coordinateSystem.getChunkAndVoxelCoordsFromRealCoords(playerPosition);
            Vec3 chunkWorldCoordinates = new Vec3(coords[0], coords[1], coords[2]).mult(coordinateSystem.getRealEdgeLengthOfBlock());
            outlineShader.getShader().uniformMat4("u_model_matrix", Mat4.translation(chunkWorldCoordinates));
            chunkOutline.draw(GL11C.GL_LINES);
            
            for (PointLightComponent pointLight : world.getPointLights()) {
                pointLight.prepOutlineShader(outlineShader.getShader());
                icosahedron.render(outlineShader.getShader());
            }
        }
    }

    @Override
    public void destroy(Boolean crashed) {
        chunkOutline.destroy();
    }
}
