package net.angle.omnicraft.client.rendering.components;

import com.samrj.devil.geo3d.Frustum;
import net.angle.omnicraft.client.rendering.CascadingShadowMap;
import net.angle.omnicraft.client.rendering.LightingManager;
import com.samrj.devil.gl.DGL;
import com.samrj.devil.gl.ShaderProgram;
import com.samrj.devil.graphics.Camera3D;
import com.samrj.devil.math.Mat4;
import com.samrj.devil.math.Vec3;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.angle.omnicraft.common.OmnicraftGameWorld;
import net.angle.omnicraft.client.OmnicraftClient;
import net.angle.omnilighting.api.DirectionalLightComponent;
import net.angle.omnientity.api.CameraComponent;
import net.angle.omnirendering.api.ShaderProgramManager;
import net.angle.omnirendering.api.TextureFrameBufferManager;
import net.angle.omnirendering.impl.BasicShaderProgramManager;
import org.lwjgl.opengl.GL11C;
import org.lwjgl.opengl.GL13C;

/**
 *
 * @author angle
 */
public class DirectionalLightRenderer extends BasicShaderProgramManager {
    private final OmnicraftGameWorld world;
    private final CameraComponent cameraComp;
    private final TextureFrameBufferManager sBuffer;
    private final LightingManager lightingManager;
    private final int shadowSize;
    
    private final ShaderProgramManager directionalShadowShader;
    
    private CascadingShadowMap cascadingShadows;

    public DirectionalLightRenderer(OmnicraftClient client, OmnicraftGameWorld world, CameraComponent cameraComp, TextureFrameBufferManager sBuffer, LightingManager lightingManager, int shadowSize) {
        super("direction_lighting", client.getResourceManager());
        this.world = world;
        this.cameraComp = cameraComp;
        this.sBuffer = sBuffer;
        this.lightingManager = lightingManager;
        this.shadowSize = shadowSize;
        directionalShadowShader = new BasicShaderProgramManager("simple_depth", client.getResourceManager());
    }
    
    @Override
    public void init() {
        super.init();
        directionalShadowShader.init();
        
        ShaderProgram shader = getShader();
        DGL.useProgram(shader);
        sBuffer.prepareShader(shader);
        shader.uniform1i("u_shadow_tex", 8);
        shader.uniform1b("u_has_shadows", true);

        try {
            cascadingShadows = new CascadingShadowMap(cameraComp, shadowSize);
        } catch (IOException ex) {
            Logger.getLogger(DirectionalLightRenderer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void renderDirectionalShadows(Mat4 matrix) {
        directionalShadowShader.render();
        directionalShadowShader.getShader().uniformMat4("u_shadow_view_matrix", matrix);
        GL11C.glClear(GL11C.GL_DEPTH_BUFFER_BIT);
        lightingManager.renderShadows(directionalShadowShader.getShader(), new Frustum(matrix));
    }
    
    private void drawDirectionalShadows(DirectionalLightComponent light) {
        lightingManager.prepareShadowPass();
        
        cascadingShadows.texture.unbind();
        
        cascadingShadows.render(cameraComp, light.getDirection(), shadowMap -> renderDirectionalShadows(shadowMap.matrix));
    }
    
    private void drawDirectionalLight(DirectionalLightComponent light) {
        drawDirectionalShadows(light);
        
        lightingManager.prepareLightingPass();
        
        cascadingShadows.texture.bind(GL13C.GL_TEXTURE8);
        
        DGL.useProgram(getShader());
        light.prepLightingShader(getShader());
        
        Camera3D camera = cameraComp.getCamera();
        
        getShader().uniformVec3v("u_shadow_positions",
                    Vec3.mult(camera.pos, cascadingShadows.map0.matrix),
                    Vec3.mult(camera.pos, cascadingShadows.map1.matrix),
                    Vec3.mult(camera.pos, cascadingShadows.map2.matrix),
                    Vec3.mult(camera.pos, cascadingShadows.map3.matrix),
                    Vec3.mult(camera.pos, cascadingShadows.map4.matrix),
                    Vec3.mult(camera.pos, cascadingShadows.map5.matrix));
        
        getShader().uniformMat4v("u_shadow_mats", cascadingShadows.map0.matrix,
                                                              cascadingShadows.map1.matrix,
                                                              cascadingShadows.map2.matrix,
                                                              cascadingShadows.map3.matrix,
                                                              cascadingShadows.map4.matrix,
                                                              cascadingShadows.map5.matrix);
        
        lightingManager.drawScreenBuffer();
    }
    
    @Override
    public void render() {
        super.render();
        getShader().uniform2f("u_fsq_size", 1, 1);
        
        for (DirectionalLightComponent light : world.getDirectionalLights())
            drawDirectionalLight(light);
    }
    
    @Override
    public void destroy(Boolean crashed) {
        cascadingShadows.destroy();
        super.destroy(crashed);
        directionalShadowShader.destroy(crashed);
    }
}