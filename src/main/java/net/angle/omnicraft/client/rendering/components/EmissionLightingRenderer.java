package net.angle.omnicraft.client.rendering.components;

import net.angle.omnicraft.client.rendering.LightingManager;
import com.samrj.devil.gl.DGL;
import net.angle.omnicraft.client.OmnicraftClient;
import net.angle.omnirendering.api.TextureFrameBufferManager;
import net.angle.omnirendering.impl.BasicShaderProgramManager;

/**
 *
 * @author angle
 */

public class EmissionLightingRenderer extends BasicShaderProgramManager {
    private final TextureFrameBufferManager sBuffer;
    private final LightingManager lightingManager;

    public EmissionLightingRenderer(OmnicraftClient client, TextureFrameBufferManager sBuffer, LightingManager lightingManager) {
        super("emission_lighting", client.getResourceManager());
        this.sBuffer = sBuffer;
        this.lightingManager = lightingManager;
    }
    
    @Override
    public void init() {
        super.init();
        DGL.useProgram(getShader());
        sBuffer.prepareShader(getShader());
    }

    @Override
    public void render() {
        super.render();
        lightingManager.prepareLightingPass();
        
        lightingManager.drawScreenBuffer();
    }
}