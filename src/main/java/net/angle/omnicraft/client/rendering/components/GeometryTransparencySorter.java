package net.angle.omnicraft.client.rendering.components;

import com.samrj.devil.gl.DGL;
import com.samrj.devil.math.Vec2i;
import net.angle.omnicraft.client.OmnicraftClient;
import net.angle.omnicraft.client.rendering.GeometryBuffer;
import net.angle.omnicraft.client.rendering.LightingManager;
import net.angle.omnirendering.impl.BasicShaderProgramManager;
import org.lwjgl.opengl.GL11C;

/**
 *
 * @author angle
 */
public class GeometryTransparencySorter extends BasicShaderProgramManager {
    private final OmnicraftClient client;
    private final LightingManager lightingManager;
    private final GeometryBuffer gBuffer;

    public GeometryTransparencySorter(OmnicraftClient client, LightingManager lightingManager, GeometryBuffer gBuffer) {
        super("geometry_transparency_sorting", client.getResourceManager());
        this.client = client;
        this.lightingManager = lightingManager;
        this.gBuffer = gBuffer;
    }
    
    @Override
    public void init() {
        super.init();
        
        DGL.useProgram(getShader());
        gBuffer.prepareShader(getShader());
    }
    
    @Override
    public void render() {
        super.render();
        Vec2i resolution = client.getResolution();
        GL11C.glViewport(0, 0, resolution.x, resolution.y);
        GL11C.glDisable(GL11C.GL_CULL_FACE);
        GL11C.glDisable(GL11C.GL_DEPTH_TEST);
        GL11C.glDepthMask(false);
        GL11C.glDisable(GL11C.GL_BLEND);
        
        lightingManager.drawScreenBuffer();
    }
}