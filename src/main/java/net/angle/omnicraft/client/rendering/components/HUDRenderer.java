package net.angle.omnicraft.client.rendering.components;

import com.samrj.devil.game.GameWindow;
import com.samrj.devil.gui.DUI;
import net.angle.omnirendering.api.RenderComponent;

/**
 *
 * @author angle
 */
public class HUDRenderer implements RenderComponent {

    @Override
    public void init() {
        GameWindow.getMouse().setGrabbed(true);
    }

    @Override
    public void render() {
        DUI.render();
    }

    @Override
    public void destroy(Boolean crashed) {
        GameWindow.getMouse().setGrabbed(false);}
}