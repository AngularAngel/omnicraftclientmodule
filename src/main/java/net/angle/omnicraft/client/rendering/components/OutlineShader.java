package net.angle.omnicraft.client.rendering.components;

import net.angle.omnicraft.client.OmnicraftClient;
import net.angle.omnirendering.api.RenderComponent;
import net.angle.omnirendering.impl.BasicShaderProgramManager;
import org.lwjgl.opengl.GL11C;

/**
 *
 * @author angle
 */
public class OutlineShader extends BasicShaderProgramManager {

    public OutlineShader(OmnicraftClient client) {
        super("outline", client.getResourceManager());
    }

    @Override
    public void render() {
        super.render();
        GL11C.glEnable(GL11C.GL_DEPTH_TEST);
        GL11C.glPolygonMode(GL11C.GL_FRONT_AND_BACK, GL11C.GL_LINE);
    }
    
    public final RenderComponent finisher = new RenderComponent() {
        @Override
        public void render() {
            GL11C.glPolygonMode(GL11C.GL_FRONT_AND_BACK, GL11C.GL_FILL);
        }
    };
}