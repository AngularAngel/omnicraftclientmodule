/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.angle.omnicraft.client.rendering.components;

import net.angle.omnicraft.client.rendering.PointShadowCubeMap;
import net.angle.omnicraft.client.rendering.LightingManager;
import com.samrj.devil.geo3d.Frustum;
import com.samrj.devil.gl.DGL;
import com.samrj.devil.gl.ShaderProgram;
import com.samrj.devil.math.Mat4;
import com.samrj.devil.math.Vec3;
import net.angle.omnicraft.common.OmnicraftGameWorld;
import net.angle.omnicraft.client.OmnicraftClient;
import net.angle.omnientity.api.Entity;
import net.angle.omnilighting.api.PointLightComponent;
import net.angle.omnirendering.api.ShaderProgramManager;
import net.angle.omnirendering.api.TextureFrameBufferManager;
import net.angle.omnirendering.api.VertexShape;
import net.angle.omnirendering.impl.BasicShaderProgramManager;
import org.lwjgl.opengl.GL11C;
import org.lwjgl.opengl.GL13C;

/**
 *
 * @author angle
 */
public class PointLightingRenderer extends BasicShaderProgramManager {
    private final OmnicraftGameWorld world;
    private final TextureFrameBufferManager sBuffer;
    private final LightingManager lightingManager;
    private final VertexShape icosahedron;
    private final Frustum visibleFrustum;
    private final int shadowSize;
    private final Entity[] renderingEntity;
    
    private final ShaderProgramManager pointShadowShader;
    
    private PointShadowCubeMap pointShadows;

    public PointLightingRenderer(OmnicraftClient client, OmnicraftGameWorld world, TextureFrameBufferManager sBuffer, LightingManager lightingManager, VertexShape icosahedron, Frustum visibleFrustum, int shadowSize, Entity[] renderingEntity) {
        super("point_lighting", client.getResourceManager());
        this.world = world;
        this.sBuffer = sBuffer;
        this.lightingManager = lightingManager;
        this.icosahedron = icosahedron;
        this.visibleFrustum = visibleFrustum;
        this.shadowSize = shadowSize;
        this.renderingEntity = renderingEntity;
        pointShadowShader = new BasicShaderProgramManager("cubemap_depth", client.getResourceManager());
    }
    
    @Override
    public void init() {
        super.init();
        pointShadowShader.init();
        
        ShaderProgram shader = getShader();
        DGL.useProgram(shader);
        sBuffer.prepareShader(shader);
        shader.uniform1i("u_shadow_tex", 8);
        shader.uniform1f("u_shadow_z_near", PointShadowCubeMap.Z_NEAR);
        shader.uniform1b("u_has_shadows", true);
        pointShadows = new PointShadowCubeMap(shadowSize);
    }
    
    private void renderPointShadows(PointLightComponent light, Mat4[] matrices) {
        pointShadowShader.render();
        pointShadowShader.getShader().uniformMat4v("u_view_matrices", matrices);
        GL11C.glClear(GL11C.GL_DEPTH_BUFFER_BIT);
        float boundingRadius = light.getBoundingRadius();
        Vec3 position = light.getPosition();
        lightingManager.renderShadows(pointShadowShader.getShader(), new Frustum(Mat4.orthographic(
                position.x - boundingRadius, position.x + boundingRadius,
                position.y - boundingRadius, position.y + boundingRadius,
                position.z - boundingRadius, position.z + boundingRadius)));
    }
    
    private void drawPointShadows(PointLightComponent light) {
        lightingManager.prepareShadowPass();
        
        pointShadows.texture.unbind();
        
        pointShadows.render(light, shadowCubeMap -> renderPointShadows(light, shadowCubeMap.getMatrices()));
    }
    
    private void drawPointLight(PointLightComponent light) {
        renderingEntity[0] = light.getEntity();
        if (!visibleFrustum.touching(light.getAabb()))
            return;
        
        drawPointShadows(light);
        
        lightingManager.prepareLightingPass();
        GL11C.glEnable(GL11C.GL_CULL_FACE);
        
        pointShadows.texture.bind(GL13C.GL_TEXTURE8);

        DGL.useProgram(getShader());
        light.prepLightingShader(getShader());
        
        icosahedron.render(getShader());
    }
    
    @Override
    public void render() {
        super.render();
        getShader().uniform1f("u_shadow_res", pointShadows.getResolution());
        
        for (PointLightComponent light : world.getPointLights()) {
            drawPointLight(light);
        }
    }
    
    @Override
    public void destroy(Boolean crashed) {
        pointShadows.destroy();
        super.destroy(crashed);
        pointShadowShader.destroy(crashed);
    }
}