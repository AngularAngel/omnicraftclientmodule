package net.angle.omnicraft.client.rendering.components;

import lombok.RequiredArgsConstructor;
import net.angle.omnirendering.api.FrameBufferManager;
import net.angle.omnirendering.api.RenderComponent;
import org.lwjgl.opengl.GL11C;

/**
 *
 * @author angle
 */
@RequiredArgsConstructor
public class RenderClearer implements RenderComponent {
    private final FrameBufferManager hdrBuffer;

    @Override
    public void init() {
        GL11C.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        GL11C.glClearDepth(1.0);
    }

    @Override
    public void render() {
        hdrBuffer.drawToFBO();
        
        GL11C.glClear(GL11C.GL_COLOR_BUFFER_BIT | GL11C.GL_DEPTH_BUFFER_BIT);
    }

    @Override
    public void destroy(Boolean crashed) {}
}