/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.angle.omnicraft.client.rendering.components;

import com.samrj.devil.gl.DGL;
import com.samrj.devil.math.Vec2;
import com.samrj.devil.math.Vec3;
import net.angle.omnicraft.client.OmnicraftClient;
import net.angle.omnicraft.client.rendering.GeometryBuffer;
import net.angle.omnirendering.api.TextureVertexManager3D;
import net.angle.omnirendering.impl.BasicShaderProgramManager;
import net.angle.omnirendering.impl.BasicTextureVertexManager3D;
import net.angle.omniworld.api.space.BlockFace;
import net.angle.omniworld.impl.space.PrimaryBlockFaces;
import org.lwjgl.opengl.GL11C;
import org.lwjgl.opengl.GL42C;
import org.lwjgl.opengl.GL43C;

/**
 *
 * @author angle
 */
public class Skybox extends BasicShaderProgramManager {
    private static final int SKYBOX_EDGE_LENGTH = 1000;
    private final TextureVertexManager3D vertexManager;
    private final OmnicraftClient client;
    private final GeometryBuffer gBuffer;

    public Skybox(OmnicraftClient client, GeometryBuffer gBuffer) {
        super("skybox", client.getResourceManager());
        this.vertexManager = new BasicTextureVertexManager3D();
        this.client = client;
        this.gBuffer = gBuffer;
    }
    
    @Override
    public void init() {
        super.init();
        DGL.useProgram(getShader());
        gBuffer.bindFragDataLocations(getShader());
        vertexManager.deleteVertices();
        vertexManager.beginVariables(24, 36);
        vertexManager.begin();
        for (BlockFace face : PrimaryBlockFaces.values())
            bufferFace(face);
        vertexManager.upload();
    }
    
    private void bufferFace(BlockFace face) {
        bufferFace(face, SKYBOX_EDGE_LENGTH);
    }
    
    private void bufferFace(BlockFace face, float edgeLength) {
        Vec2 dimensions = new Vec2(edgeLength, edgeLength);
        Vec3 drawStart = new Vec3(-edgeLength/2f, -edgeLength/2f, -edgeLength/2f);
        Vec3 orientFace = face.orientFaceInwards(dimensions);
        drawStart.add(face.getInwardsDrawStart(new Vec3()).mult(edgeLength));
        vertexManager.bufferFlatVertices(drawStart.x, drawStart.y, drawStart.z, orientFace.x, orientFace.y, orientFace.z);
    }
    
    @Override
    public void render() {
        super.render();
        GL11C.glDisable(GL11C.GL_CULL_FACE);
        
        vertexManager.draw();
        GL42C.glMemoryBarrier(GL43C.GL_SHADER_STORAGE_BARRIER_BIT);
        
        GL11C.glEnable(GL11C.GL_CULL_FACE);
    }
    
    @Override
    public void destroy(Boolean crashed) {
        super.destroy(crashed);
        vertexManager.destroy();
    }
}