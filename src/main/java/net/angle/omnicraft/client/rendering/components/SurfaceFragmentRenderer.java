package net.angle.omnicraft.client.rendering.components;

import com.samrj.devil.gl.DGL;
import com.samrj.devil.gl.ShaderProgram;
import com.samrj.devil.math.Vec2i;
import net.angle.omnicraft.client.OmnicraftClient;
import net.angle.omnicraft.client.rendering.LightingManager;
import net.angle.omnicraft.common.OmnicraftGameWorld;
import net.angle.omnirendering.api.TextureFrameBufferManager;
import net.angle.omnirendering.impl.BasicShaderProgramManager;
import org.lwjgl.opengl.GL11C;

/**
 *
 * @author angle
 */
public class SurfaceFragmentRenderer extends BasicShaderProgramManager {
    private final OmnicraftClient client;
    private final OmnicraftGameWorld world;
    private final TextureFrameBufferManager gBuffer, sBuffer;
    private final LightingManager lightingManager;

    public SurfaceFragmentRenderer(OmnicraftClient client, OmnicraftGameWorld world, TextureFrameBufferManager gBuffer, TextureFrameBufferManager sBuffer, LightingManager lightingManager) {
        super("surface", client.getResourceManager());
        this.client = client;
        this.world = world;
        this.gBuffer = gBuffer;
        this.sBuffer = sBuffer;
        this.lightingManager = lightingManager;
    }

    @Override
    public void init() {
        super.init();
        ShaderProgram shader = getShader();
        DGL.useProgram(shader);
        world.prepShader(shader);
        gBuffer.prepareShader(shader);
        sBuffer.bindFragDataLocations(shader);
        shader.uniform1i("u_ambient_light_tex1", 1);
        shader.uniform1i("u_ambient_light_tex2", 2);
    }

    @Override
    public void render() {
        super.render();
        Vec2i resolution = client.getResolution();
        GL11C.glViewport(0, 0, resolution.x, resolution.y);
        GL11C.glDisable(GL11C.GL_BLEND);
        GL11C.glDisable(GL11C.GL_CULL_FACE);
        GL11C.glEnable(GL11C.GL_DEPTH_TEST);
        GL11C.glDepthMask(true);
        
        lightingManager.drawScreenBuffer();
    }
}