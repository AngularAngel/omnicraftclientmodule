package net.angle.omnicraft.client.rendering.components;

import com.samrj.devil.gl.DGL;
import net.angle.omnicraft.client.OmnicraftClient;
import net.angle.omnicraft.client.rendering.GeometryBuffer;
import net.angle.omnirendering.impl.BasicShaderProgramManager;
import org.lwjgl.opengl.GL11C;

/**
 * Renders chunk surfaces to the gBuffer;
 * @author angle
 */
public class SurfaceGeometryShader extends BasicShaderProgramManager {
    private final GeometryBuffer gBuffer;

    public SurfaceGeometryShader(OmnicraftClient client, GeometryBuffer gBuffer) {
        super("surface_geometry", client.getResourceManager());
        this.gBuffer = gBuffer;
    }
    
    @Override
    public void init() {
        super.init();
        DGL.useProgram(getShader());
        gBuffer.bindFragDataLocations(getShader());
    }

    @Override
    public void render() {
        GL11C.glDisable(GL11C.GL_BLEND);
        GL11C.glDisable(GL11C.GL_CULL_FACE);
        super.render();
    }
}