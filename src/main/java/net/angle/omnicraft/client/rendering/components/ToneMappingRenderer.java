package net.angle.omnicraft.client.rendering.components;

import com.samrj.devil.gl.DGL;
import com.samrj.devil.gl.ShaderProgram;
import net.angle.omnicraft.client.OmnicraftClient;
import net.angle.omnicraft.client.rendering.LightingManager;
import net.angle.omnirendering.api.TextureFrameBufferManager;
import net.angle.omnirendering.impl.BasicShaderProgramManager;
import org.lwjgl.opengl.GL11C;

/**
 *
 * @author angle
 */
public class ToneMappingRenderer extends BasicShaderProgramManager {
    private final TextureFrameBufferManager hdrBuffer;
    private final LightingManager lightingManager;

    public ToneMappingRenderer(OmnicraftClient client, TextureFrameBufferManager hdrBuffer, LightingManager lightingManager) {
        super("tone_mapping", client.getResourceManager());
        this.hdrBuffer = hdrBuffer;
        this.lightingManager = lightingManager;
    }
    
    @Override
    public void init() {
        super.init();
        
        ShaderProgram shader = getShader();
        DGL.useProgram(shader);
        hdrBuffer.prepareShader(shader);
        shader.uniform1f("u_exposure", 1);
    }
    
    
    @Override
    public void render() {
        super.render();
        lightingManager.prepareLightingPass();
        GL11C.glDisable(GL11C.GL_BLEND);
        
        DGL.bindFBO(null);
        hdrBuffer.readFBO();
        
        lightingManager.drawScreenBuffer();
    }
}