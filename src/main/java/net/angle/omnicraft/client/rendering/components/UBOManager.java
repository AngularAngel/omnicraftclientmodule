package net.angle.omnicraft.client.rendering.components;

import com.samrj.devil.gl.BufferObject;
import com.samrj.devil.gl.DGL;
import com.samrj.devil.graphics.Camera3D;
import com.samrj.devil.graphics.ViewFrustum;
import com.samrj.devil.math.Mat3;
import com.samrj.devil.math.Mat4;
import com.samrj.devil.math.Vec4;
import java.nio.ByteBuffer;
import java.util.List;
import lombok.RequiredArgsConstructor;
import net.angle.omnicraft.client.OmnicraftClient;
import net.angle.omnicraft.client.rendering.LightingManager;
import net.angle.omnicraft.common.OmnicraftGameWorld;
import net.angle.omnicraft.server.OmnicraftServer;
import net.angle.omnientity.api.CameraComponent;
import net.angle.omnilighting.api.DirectionalLightComponent;
import net.angle.omnirendering.api.RenderComponent;
import net.angle.omniworld.api.space.CoordinateSystem;
import org.lwjgl.opengl.GL31C;
import org.lwjgl.system.MemoryStack;

/**
 *
 * @author angle
 */
@RequiredArgsConstructor
public class UBOManager implements RenderComponent {
    
    private static final int LIGHT_LENGTH = 16;
    
    //(5 Mat4s, 2 Vec4[LIGHT_LENGTH], 2 Vec4s, 1 vec2, 1 ivec2, 3 floats, and 1 int
    private static final int UBO_SIZE = 5*16*Float.BYTES + 2*4*Float.BYTES*LIGHT_LENGTH + 2*4*Float.BYTES + 
                                        2*Float.BYTES + 2*Integer.BYTES + 3*Float.BYTES + 2*Integer.BYTES;
    private static final int UBO_BINDING = 0;
    
    private final OmnicraftClient client;
    private final OmnicraftGameWorld gameWorld;
    private final CameraComponent cameraComponent;
    private final LightingManager lightingManager;
    private BufferObject uniformBuffer;
    
    @Override
    public void init() {
        uniformBuffer = DGL.genBufferObject(GL31C.GL_UNIFORM_BUFFER);
        uniformBuffer.bufferData(UBO_SIZE, GL31C.GL_DYNAMIC_DRAW);
        uniformBuffer.bindBufferBase(UBO_BINDING);
    }
    
    @Override
    public void render() {
        Camera3D camera = cameraComponent.getCamera();
        ViewFrustum frustum = cameraComponent.getFrustum();
        CoordinateSystem coordinateSystem = gameWorld.getChunkMap().getCoordinateSystem();
        try (MemoryStack stack = MemoryStack.stackPush()) { //See ubo.glsl for where these all end up.
            ByteBuffer buf = stack.malloc(UBO_SIZE);
            camera.viewMat.write(buf); //u_view_matrix
            frustum.projMat.write(buf); //u_proj_matrix
            new Mat4(Mat3.rotation(cameraComponent.getCamera().dir)).write(buf); //u_inv_dir_matrix
            new Mat4(lightingManager.getScreenMatrix()).write(buf); //u_screen_matrix
        
            List<DirectionalLightComponent> lights = gameWorld.getEntities().getComponents(DirectionalLightComponent.class);
            Vec4[] light_directions = new Vec4[LIGHT_LENGTH];
            Vec4[] light_colors = new Vec4[LIGHT_LENGTH];
            int i = 0;

            for (DirectionalLightComponent light : lights) {
                light_directions[i] = new Vec4(light.getDirection(), 0);
                light_colors[i++] = new Vec4(light.getColor(), 0);
            }

            for (; i < 16; i++) {
                light_directions[i] = new Vec4();
                light_colors[i] = new Vec4();
            }
            for (Vec4 light_direction : light_directions)
                light_direction.write(buf); //u_light_directions
            for (Vec4 light_color : light_colors)
                light_color.write(buf); //u_light_colors
            new Vec4(camera.pos, 1).write(buf); //u_camera_pos
            //These two form u_max_dir.
            buf.putFloat(frustum.getHSlope());
            buf.putFloat(frustum.getVSlope());
            client.getResolution().write(buf); //u_screen_size
            buf.putFloat(frustum.zFar); //u_z_far
            //Subtract a very small number here to prevent
            //having a sliver where it hits the full length on the far edges, and causing lines on the far edge of the texture.
            buf.putFloat((OmnicraftGameWorld.TEXELS_PER_METER * coordinateSystem.getRealEdgeLengthOfBlock()) - 0.00001f); //u_texels_per_block
            buf.putFloat(1.0f / OmnicraftGameWorld.TEXELS_PER_METER * coordinateSystem.getRealEdgeLengthOfBlock()); //u_texel_length
            buf.putInt(coordinateSystem.getBlockEdgeLengthOfChunk()); //u_chunk_block_length
            buf.putInt(OmnicraftServer.RENDER_DISTANCE); //u_render_distance
            buf.flip();
            uniformBuffer.bufferSubData(0, buf);
        }
    }
    
    @Override
    public void destroy(Boolean crashed) {
        DGL.delete(uniformBuffer);
    }
}
