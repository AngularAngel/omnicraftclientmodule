package net.angle.omnicraft.client.screens;

import com.samrj.devil.game.GameWindow;
import com.samrj.devil.geo3d.Frustum;
import com.samrj.devil.gl.ShaderProgram;
import com.samrj.devil.gui.Button;
import com.samrj.devil.gui.DUI;
import com.samrj.devil.gui.LayoutRows;
import com.samrj.devil.gui.Window;
import com.samrj.devil.math.Mat4;
import com.samrj.devil.math.Vec2;
import com.samrj.devil.math.Vec2i;
import java.io.IOException;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.angle.omniclient.api.Screen;
import net.angle.omniclient.impl.AbstractScreen;
import net.angle.omnicraft.client.ClientSession;
import net.angle.omnicraft.common.OmnicraftGameWorld;
import net.angle.omnicraft.client.OmnicraftClient;
import net.angle.omnicraft.client.OmnicraftConsole;
import net.angle.omnicraft.client.SettingsMenu;
import net.angle.omnicraft.client.rendering.components.DebugWindow;
import net.angle.omnicraft.client.rendering.components.SurfaceFragmentRenderer;
import net.angle.omnicraft.client.rendering.components.ChunkRenderer;
import net.angle.omnicraft.client.rendering.components.DirectionalLightRenderer;
import net.angle.omnicraft.client.rendering.GeometryBuffer;
import net.angle.omnicraft.client.rendering.components.HUDRenderer;
import net.angle.omnicraft.client.rendering.LightingManager;
import net.angle.omnicraft.client.rendering.SurfaceBuffer;
import net.angle.omnicraft.client.rendering.components.BlockOutlineRenderer;
import net.angle.omnicraft.client.rendering.components.BloomRenderer;
import net.angle.omnicraft.client.rendering.components.PointLightingRenderer;
import net.angle.omnicraft.client.rendering.components.RenderClearer;
import net.angle.omnicraft.client.rendering.components.Skybox;
import net.angle.omnicraft.client.rendering.components.SurfaceGeometryShader;
import net.angle.omnicraft.client.rendering.components.EmissionLightingRenderer;
import net.angle.omnicraft.client.rendering.components.OutlineShader;
import net.angle.omnicraft.client.rendering.components.ToneMappingRenderer;
import net.angle.omnicraft.client.rendering.components.GeometryTransparencySorter;
import net.angle.omnicraft.client.rendering.components.UBOManager;
import net.angle.omnientity.api.CameraComponent;
import net.angle.omnientity.api.Component;
import net.angle.omnientity.api.RenderingComponent;
import net.angle.omnientity.api.PlayerControllerComponent;
import net.angle.omnientity.api.ControllerComponent;
import net.angle.omnientity.api.Entity;
import net.angle.omnirendering.impl.BasicComponentRenderingManager;
import net.angle.omnirendering.api.RenderComponent;
import net.angle.omnirendering.api.ComponentRenderingManager;
import net.angle.omnirendering.api.TextureFrameBufferManager;
import net.angle.omnirendering.api.VertexShape;
import net.angle.omnirendering.impl.BasicTextureFrameBufferManager;
import net.angle.omnirendering.impl.ShaderResource;
import net.angle.omnirendering.impl.Icosahedron;
import net.angle.omniworld.api.chunks.Chunk;
import net.angle.omniworld.api.chunks.VoxelComponentContainer;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.opengl.GL11C;
import org.lwjgl.opengl.GL30C;

/**
 *
 * @author angle
 */
public class GameScreen extends AbstractScreen<OmnicraftClient> implements Screen<OmnicraftClient> {
    private final ClientSession clientSession;
    private final ComponentRenderingManager renderingManager;
    private final GeometryBuffer gBuffer;
    private final SurfaceBuffer sBuffer;
    private final TextureFrameBufferManager hdrBuffer;
    private final LightingManager lightingManager;
    private CameraComponent cameraComponent;
    private PlayerControllerComponent controllerComponent;
    private VertexShape icosahedron;
    private ComponentRenderingManager surfaceGeometryRenderer;
    private BloomRenderer bloomRenderer;
    private SurfaceGeometryShader surfaceGeometryShader;
    private DebugWindow debugWindow;
    private OmnicraftConsole console;
    private SettingsMenu settingsMenu;
    private Window escapeMenu;
    private ChunkRenderer chunkRenderer;
    
    public GameScreen(OmnicraftClient client, ClientSession clientSession) {
        super(client);
        this.clientSession = clientSession;
        Vec2i resolution = GameWindow.getResolution();
        hdrBuffer = new BasicTextureFrameBufferManager("u_hdr_buffer", null, -1, 0, GL30C.GL_RGBA16F, GL30C.GL_COLOR_ATTACHMENT0, resolution.x, resolution.y);
        gBuffer = new GeometryBuffer(resolution);
        sBuffer = new SurfaceBuffer(resolution);
        renderingManager = new BasicComponentRenderingManager();
        lightingManager = new LightingManager(client, new BasicComponentRenderingManager(), hdrBuffer);
    }
    
    public void initConsole(OmnicraftGameWorld gameWorld) {
        console = new OmnicraftConsole();
        
        console.window.setCloseCallback((t) -> {
            unpause();
        });
        console.addCommand(new OmnicraftConsole.Command("listEntities", "Lists all entities.") {
            @Override
            public String apply(String inputString) {
                StringBuilder output = new StringBuilder("Listing all entities!\n");
                
                for (Entity entity : gameWorld.getEntities().getAllEntries()){
                    output.append("\n" + entity);
                }
                
                return output.toString();
            }
        });
        console.addCommand(new OmnicraftConsole.Command("detailEntity", "Details an entity, specificed by id.") {
            @Override
            public String apply(String inputString) {
                if (inputString.length() == 0)
                    return "No entity specified!";
                
                int id = Integer.decode(inputString.substring(1));
                Entity entity = gameWorld.getEntities().getEntryById(id);
                
                if (Objects.isNull(entity))
                    return "No such entity!";
                
                StringBuilder output = new StringBuilder("Detailing entity: " + entity + "\n");
                
                output.append("\nEntity components: ");
                for (Component component : entity.getComponents()){
                    output.append("\n" + component);
                }
                
                return output.toString();
            }
        });
        console.addCommand(new OmnicraftConsole.Command("detailChunk", "Details a chunk, specified by coordinates.") {
            @Override
            public String apply(String inputString) {
                if (inputString.length() == 0)
                    return "No chunk specificed!";
                inputString = inputString.substring(1);
                
                String[] tokens = inputString.split(" ");
                
                if (tokens.length < 3)
                    return "No chunk specificed!";
                if (tokens[0].length() < 1 || tokens[1].length() < 1 || tokens[2].length() < 1)
                    return "Chunk improperly specificed!";
                
                Chunk chunk = gameWorld.getChunkMap().getChunk(Integer.decode(tokens[0]), Integer.decode(tokens[1]), Integer.decode(tokens[2]));
                
                StringBuilder output = new StringBuilder("Detailing Chunk: " + chunk + "\n");
                if (Objects.nonNull(chunk)) {
                    output.append("\nChunk components: ");
                    for (VoxelComponentContainer component : chunk.getComponents()){
                        output.append("\n" + component);
                    }
                }
                return output.toString();
            }
        });
        console.addCommand(new OmnicraftConsole.Command("reloadChunk", "Reloads a chunk, specified by coordinates.") {
            @Override
            public String apply(String inputString) {
                if (inputString.length() == 0)
                    return "No chunk specificed!";
                inputString = inputString.substring(1);
                
                String[] tokens = inputString.split(" ");
                
                if (tokens.length < 3)
                    return "No chunk specificed!";
                if (tokens[0].length() < 1 || tokens[1].length() < 1 || tokens[2].length() < 1)
                    return "Chunk improperly specificed!";
                
                Chunk chunk = gameWorld.getChunkMap().getChunk(Integer.decode(tokens[0]), Integer.decode(tokens[1]), Integer.decode(tokens[2]));
                if (Objects.nonNull(chunk)) {
                    chunkRenderer.reloadChunk(chunk);
                
                    return "Updated chunk!";
                } else
                    return "Chunk was null!";
            }
        });
    }
    
    public void initEscapeMenu() {
        escapeMenu = new Window();
        escapeMenu.setTitleBarVisible(false);
        
        LayoutRows rows = new LayoutRows();
        escapeMenu.setContent(rows);
        
        Button button = new Button("Resume");
        rows.add(button);
        
        button.setCallback((t) -> {
            toggleEscapeMenu();
        });
        
        button = new Button("Settings");
        rows.add(button);
        
        button.setCallback((t) -> {
            settingsMenu.setVisible(true);
            DUI.hide(escapeMenu);
        });
        
        button = new Button("Quit");
        rows.add(button);
        
        button.setCallback((t) -> {
            getClient().endSinglePlayerGame();
            DUI.hide(escapeMenu);
        });
        
        escapeMenu.setSizeFromContent();
    }
    
    @Override
    public void init() {
        try {
            ShaderResource.init();
        } catch (IOException ex) {
            Logger.getLogger(GameScreen.class.getName()).log(Level.SEVERE, null, ex);
        }
        OmnicraftGameWorld gameWorld = clientSession.getGameWorld();
        Entity player = clientSession.getPlayer();
        OutlineShader outlineShader = new OutlineShader(getClient());
        icosahedron = new Icosahedron();
        icosahedron.init();
        debugWindow = new DebugWindow(player, gameWorld, icosahedron, outlineShader);
        cameraComponent = player.getComponent(CameraComponent.class);
        controllerComponent = (PlayerControllerComponent) player.getComponent(ControllerComponent.class);
        lightingManager.init();
        Vec2i resolution = GameWindow.getResolution();
        lightingManager.resize(resolution.x, resolution.y);
        int shadowSize = Math.min(GL11C.glGetInteger(GL11C.GL_MAX_TEXTURE_SIZE), 2048);
        
        surfaceGeometryShader = new SurfaceGeometryShader(getClient(), gBuffer);
        
        surfaceGeometryShader.init();
        
        Frustum visibleFrustum = new Frustum();
        
        Entity[] renderingEntity = new Entity[1];
        ShaderProgram[] curShader = new ShaderProgram[1];
        
        surfaceGeometryRenderer = lightingManager.getSurfaceRenderer();
        chunkRenderer = new ChunkRenderer(getClient(), gameWorld, surfaceGeometryShader, visibleFrustum);
        lightingManager.setChunkRenderer(chunkRenderer);
        surfaceGeometryRenderer.addRenderComponent(chunkRenderer);
        surfaceGeometryRenderer.addRenderComponent(new RenderComponent() {
            @Override
            public void render(ShaderProgram shader) {
                if (Objects.isNull(shader)) {
                    curShader[0] = surfaceGeometryShader.getShader();
                    surfaceGeometryShader.render();
                } else
                    curShader[0] = shader;
                
                clientSession.getGameWorld().getEntities().getComponents(RenderingComponent.class).forEach((renderingComponent) -> {
                    if (renderingComponent.getEntity() != renderingEntity[0])
                        renderingComponent.render(curShader[0]);
                });
            }
        });
        
        renderingManager.addRenderComponents(new RenderClearer(hdrBuffer),
                gBuffer.drawStarter,
                new UBOManager(getClient(), gameWorld, cameraComponent, lightingManager),
                new Skybox(getClient(), gBuffer),
                new RenderComponent() {
                    @Override
                    public void render(ShaderProgram shader) {
                        renderingEntity[0] = clientSession.getPlayer();
                        Mat4 viewProjMat = cameraComponent.getViewProjMat();
                        visibleFrustum.set(viewProjMat);
                        chunkRenderer.setVisibleFrustum(visibleFrustum);
                    }
                },
                surfaceGeometryRenderer,
                sBuffer.drawStarter,
                gBuffer.drawFinisher,
                new GeometryTransparencySorter(getClient(), lightingManager, gBuffer),
                new SurfaceFragmentRenderer(getClient(), gameWorld, gBuffer, sBuffer, lightingManager),
                sBuffer.drawFinisher,
                new RenderComponent() {
                    @Override
                    public void render(ShaderProgram shader) {
                        renderingEntity[0] = null;
                    }
                },
                new EmissionLightingRenderer(getClient(), sBuffer, lightingManager),
                new DirectionalLightRenderer(getClient(), gameWorld, cameraComponent, sBuffer, lightingManager, shadowSize),
                new PointLightingRenderer(getClient(), gameWorld, sBuffer, lightingManager, icosahedron, visibleFrustum, shadowSize, renderingEntity),
                bloomRenderer = new BloomRenderer(getClient(), hdrBuffer, lightingManager),
                new ToneMappingRenderer(getClient(), hdrBuffer, lightingManager),
                outlineShader,
                new BlockOutlineRenderer(player, gameWorld, outlineShader),
                debugWindow,
                outlineShader.finisher,
                new HUDRenderer()
        );
        renderingManager.init();
        
        initConsole(gameWorld);
        
        settingsMenu = new SettingsMenu(getClient());
        
        settingsMenu.window.setCloseCallback((t) -> {
            unpause();
        });
        
        initEscapeMenu();
    }
    
    private void pause() {
        GameWindow.getMouse().setGrabbed(false);
        clientSession.pause();
    }
    
    private void unpause() {
        if (console.isVisible() || settingsMenu.window.isVisible() || escapeMenu.isVisible())
            return;
        GameWindow.getMouse().setGrabbed(true);
        clientSession.unpause();
        controllerComponent.updateMouse(GameWindow.getMouse().getPos());
    }
    
    private void toggleConsole() {
        boolean openingConsole = !console.isVisible();
        console.setVisible(openingConsole);
        if (openingConsole) {
            pause();
        }
    }
    
    private void toggleSettings() {
        boolean openingSettings = !settingsMenu.window.isVisible();
        settingsMenu.setVisible(openingSettings);
        if (openingSettings) {
            pause();
        }
    }
    
    private void toggleEscapeMenu() {
        if (!escapeMenu.isVisible()) {
            if (!clientSession.isPaused()) {
                DUI.show(escapeMenu.setPosCenterViewport());
                pause();
            }
        } else {
            DUI.hide(escapeMenu);
            unpause();
        }
    }
    
    @Override
    public void afterInput() {
        if (console.isVisible())
            console.afterInput();
    }
    
    @Override
    public void key(int key, int action, int mods) {
        if (console.isVisible())
            console.key(key, action, mods);
        
        switch (action) {
            case GLFW.GLFW_PRESS -> {
                switch (key) {
                    case GLFW.GLFW_KEY_ESCAPE -> {
                        if (console.isVisible())
                            toggleConsole();
                        else if (settingsMenu.window.isVisible())
                            toggleSettings();
                        else
                            toggleEscapeMenu();
                    }
                    case GLFW.GLFW_KEY_F3 -> debugWindow.toggle();
                    case GLFW.GLFW_KEY_GRAVE_ACCENT -> {
                        if (!console.hasFocus())
                            toggleConsole();
                    }
                }
            }
        }
    }

    @Override
    public void resize(int width, int height) {
        lightingManager.resize(width, height);
        gBuffer.resize(width, height);
        bloomRenderer.resize(width, height);
    }
    
    @Override
    public void step(float dt) {
        debugWindow.step(dt);
    }
    
    @Override
    public void mouseMoved(float x, float y) {
        if (!clientSession.isPaused())
            controllerComponent.mouseMoved(new Vec2(x, y));
    }
    
    @Override
    public void render() {
        renderingManager.render();
    }
    
    @Override
    public void destroy(Boolean crashed) {
        if (debugWindow.isVisible())
            debugWindow.toggle();
        surfaceGeometryShader.destroy(crashed);
        lightingManager.destroy();
        surfaceGeometryRenderer.destroy(crashed);
        renderingManager.destroy(crashed);
        hdrBuffer.destroy();
        icosahedron.destroy(crashed);
        gBuffer.destroy();
        sBuffer.destroy();
    }
}