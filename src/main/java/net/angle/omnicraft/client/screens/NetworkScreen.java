package net.angle.omnicraft.client.screens;

import com.samrj.devil.gui.Button;
import com.samrj.devil.gui.DUI;
import com.samrj.devil.gui.LayoutRows;
import com.samrj.devil.gui.TextField;
import net.angle.omniclient.impl.AbstractMenuScreen;
import net.angle.omnicraft.client.OmnicraftClient;
import org.lwjgl.glfw.GLFW;

/**
 *
 * @author angle
 */
public class NetworkScreen extends AbstractMenuScreen<OmnicraftClient> {

    public NetworkScreen(OmnicraftClient client) {
        super(client);
    }
    
    @Override
    public void populateTitleWindow() {
        populateTitleWindow("Network Game");
    }
    
    @Override
    public void key(int key, int action, int mods) {
        if (key == GLFW.GLFW_KEY_ESCAPE && action == GLFW.GLFW_PRESS)
            getClient().changeScreen(new TitleScreen(getClient()));
    }

    @Override
    public void populateMenuWindow() {
        LayoutRows rows = new LayoutRows();
        menuWindow.setContent(rows);
        
        TextField ipAddressField = new TextField();
        rows.add(ipAddressField);
        
        Button button = new Button("Join");
        rows.add(button);
        
        button.setCallback((t) -> {
            
            String string = ipAddressField.get();
            if (string.contains(":")) {
                String[] tokens = string.split(":");
                getClient().startMultiPlayerGame(tokens[0], Integer.getInteger(tokens[1]));
            } else
                getClient().startMultiPlayerGame(string);
        });
    }
}