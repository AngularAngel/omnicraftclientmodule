/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.angle.omnicraft.client.screens;

import net.angle.omniclient.impl.AbstractMenuScreen;
import com.samrj.devil.gui.LayoutRows;
import com.samrj.devil.gui.TextWithFont;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.angle.omniclient.impl.PropertiedFontResource;
import net.angle.omnicraft.client.OmnicraftClient;
import org.lwjgl.glfw.GLFW;

/**
 *
 * @author angle
 */
public class TitleScreen extends AbstractMenuScreen<OmnicraftClient> {
    
    public TitleScreen(OmnicraftClient client) {
        super(client);
    }

    @Override
    public void populateTitleWindow() {
        try {
            populateTitleWindow(new TextWithFont(getClient().getTitle().toUpperCase(), getClient().getResourceManager().load(PropertiedFontResource.TYPE, "main_title_font", this)));
        } catch (IOException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void populateMenuWindow() {
        LayoutRows rows = new LayoutRows();
        menuWindow.setContent(rows);
        
        rows.add(getTransitionButton("Local Game", WorldSelectScreen::new));
        rows.add(getTransitionButton("Network Game", NetworkScreen::new));
    }
    
    @Override
    public void key(int key, int action, int mods) {
        if (key == GLFW.GLFW_KEY_ESCAPE && action == GLFW.GLFW_PRESS)
            getClient().beginShutdown();
    }

    @Override
    public void destroy(Boolean crashed) {
        super.destroy(crashed);
        getClient().getResourceManager().unloadResource("main_title_font", this);
    }
}