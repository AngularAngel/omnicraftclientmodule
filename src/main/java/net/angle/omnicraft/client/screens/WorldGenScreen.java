/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.angle.omnicraft.client.screens;

import com.samrj.devil.gui.Button;
import com.samrj.devil.gui.LayoutRows;
import net.angle.omniclient.impl.AbstractMenuScreen;
import net.angle.omnicraft.client.OmnicraftClient;
import org.lwjgl.glfw.GLFW;

/**
 *
 * @author angle
 */
public class WorldGenScreen extends AbstractMenuScreen<OmnicraftClient> {
    public WorldGenScreen(OmnicraftClient client) {
        super(client);
    }
    
    @Override
    public void populateTitleWindow() {
        populateTitleWindow("Generate World");
    }

    @Override
    public void populateMenuWindow() {
        LayoutRows rows = new LayoutRows();
        menuWindow.setContent(rows);
        
        Button button = new Button("Generate");
        rows.add(button);
        
        button.setCallback((t) -> {
            getClient().startSinglePlayerGame();
        });
    }
    
    @Override
    public void key(int key, int action, int mods) {
        if (key == GLFW.GLFW_KEY_ESCAPE && action == GLFW.GLFW_PRESS)
            getClient().changeScreen(new WorldSelectScreen(getClient()));
    }
}