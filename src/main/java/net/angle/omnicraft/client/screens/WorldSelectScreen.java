/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.angle.omnicraft.client.screens;

import net.angle.omniclient.impl.AbstractMenuScreen;
import com.samrj.devil.gui.Button;
import com.samrj.devil.gui.LayoutColumns;
import net.angle.omnicraft.client.OmnicraftClient;
import org.lwjgl.glfw.GLFW;

/**
 *
 * @author angle
 */
public class WorldSelectScreen extends AbstractMenuScreen<OmnicraftClient> {

    public WorldSelectScreen(OmnicraftClient client) {
        super(client);
    }
    
    @Override
    public void populateTitleWindow() {
        populateTitleWindow("Select World");
    }
    
    @Override
    public void populateMenuWindow() {
        LayoutColumns columns = new LayoutColumns();
        menuWindow.setContent(columns);
        
        Button button = new Button("Play");
        columns.add(button);
        
        button.setCallback((t) -> {
            getClient().startSinglePlayerGame();
        });
        
        columns.add(getTransitionButton("New", WorldGenScreen::new));
    }
    
    @Override
    public void key(int key, int action, int mods) {
        if (key == GLFW.GLFW_KEY_ESCAPE && action == GLFW.GLFW_PRESS)
            getClient().changeScreen(new TitleScreen(getClient()));
    }
}