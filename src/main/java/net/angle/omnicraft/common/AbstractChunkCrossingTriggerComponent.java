package net.angle.omnicraft.common;

import com.samrj.devil.math.Vec3i;
import lombok.Getter;
import net.angle.omnientity.api.Entity;
import net.angle.omnientity.api.PositionComponent;
import net.angle.omnientity.impl.AbstractComponent;
import net.angle.omniworld.api.chunks.ChunkMap;

/**
 *
 * @author angle
 */
public abstract class AbstractChunkCrossingTriggerComponent extends AbstractComponent implements ChunkCrossingTriggerComponent {
    private final @Getter ChunkMap chunkMap;
    private final Vec3i lastChunkPosition = new Vec3i(Integer.MAX_VALUE);
    private PositionComponent positionComponent;

    public AbstractChunkCrossingTriggerComponent(ChunkMap chunkMap, Entity entity) {
        super(entity);
        this.chunkMap = chunkMap;
    }

    @Override
    public Vec3i getLastChunkPosition() {
        return new Vec3i(lastChunkPosition);
    }
    
    @Override
    public void setLastChunkPosition(Vec3i lastChunkPosition) {
        this.lastChunkPosition.set(lastChunkPosition);
    }
    
    @Override
    public void init() {
        super.init();
        positionComponent = getEntity().getComponent(PositionComponent.class);
    }

    @Override
    public Vec3i getCurrentChunkPosition() {
        Vec3i coords = chunkMap.getCoordinateSystem().getChunkCoordsFromRealCoords(positionComponent.getPosition());
        coords.y = 0;
        return coords;
    }
}