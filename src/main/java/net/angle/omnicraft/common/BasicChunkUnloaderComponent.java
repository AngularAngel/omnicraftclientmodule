package net.angle.omnicraft.common;

import lombok.Getter;
import lombok.Setter;
import net.angle.omnientity.api.Entity;
import net.angle.omniworld.api.chunks.ChunkMap;

/**
 *
 * @author angle
 */
public class BasicChunkUnloaderComponent extends AbstractChunkCrossingTriggerComponent implements ChunkUnloaderComponent {
    private @Getter @Setter int unloadingDistance;
    
    public BasicChunkUnloaderComponent(ChunkMap chunkMap, int unloadingDistance, Entity entity) {
        super(chunkMap, entity);
        this.unloadingDistance = unloadingDistance;
    }
}