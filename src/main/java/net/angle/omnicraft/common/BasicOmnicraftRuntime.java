package net.angle.omnicraft.common;

import java.util.HashMap;
import java.util.Map;
import net.angle.omniblocks.impl.OmniBlocksModuleImpl;
import net.angle.omniclient.impl.OmniClientModuleImpl;
import net.angle.omnientity.impl.OmniEntityModuleImpl;
import net.angle.omnilighting.impl.OmniLightingModuleImpl;
import net.angle.omnimodule.api.OmniModule;
import net.angle.omninetwork.impl.OmniNetworkModuleImpl;
import net.angle.omniregistry.impl.OmniRegistryModuleImpl;
import net.angle.omniresource.impl.OmniResourceModuleImpl;
import net.angle.omniserialization.impl.OmniSerializationModuleImpl;
import net.angle.omnisides.impl.OmniSidesModuleImpl;
import net.angle.omniworld.impl.OmniWorldModuleImpl;

/**
 *
 * @author angle
 */
public class BasicOmnicraftRuntime implements OmnicraftRuntime {
    
    private final Map<Class<? extends OmniModule>, OmniModule> services = new HashMap<>();
    
    public BasicOmnicraftRuntime() {
        addServices(new OmniClientModuleImpl(),
                    new OmniResourceModuleImpl(),
                    new OmniRegistryModuleImpl(),
                    new OmniWorldModuleImpl(),
                    new OmniEntityModuleImpl(),
                    new OmniBlocksModuleImpl(),
                    new OmniSidesModuleImpl(),
                    new OmniLightingModuleImpl(),
                    new OmniSerializationModuleImpl(),
                    new OmniNetworkModuleImpl()
        );
    }
    
    private void addService(OmniModule service) {
        services.put(service.getInterfaceClass(), service);
    }
    
    private void addServices(OmniModule... services) {
        for (OmniModule service : services)
            addService(service);
    }
    
    @Override
    public <T> T getService(Class<T> type) {
        return (T) services.get(type);
    }
}