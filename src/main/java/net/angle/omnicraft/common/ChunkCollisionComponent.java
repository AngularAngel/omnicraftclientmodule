package net.angle.omnicraft.common;

import com.samrj.devil.geo3d.Box3;
import com.samrj.devil.math.Vec3;
import com.samrj.devil.math.Vec3i;
import static java.lang.Float.NEGATIVE_INFINITY;
import static java.lang.Float.POSITIVE_INFINITY;
import static java.lang.Math.max;
import static java.lang.Math.min;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import net.angle.omniblocks.api.Block;
import net.angle.omnientity.api.CollisionComponent;
import net.angle.omnientity.api.Entity;
import net.angle.omnientity.impl.AbstractComponent;
import net.angle.omniworld.api.chunks.ChunkMap;
import net.angle.omniworld.api.chunks.VoxelComponent;
import net.angle.omniworld.api.space.CollisionContact;
import net.angle.omniworld.impl.space.BasicCollisionContact;

/**
 *
 * @author angle
 */
public class ChunkCollisionComponent extends AbstractComponent implements CollisionComponent {

    private ChunkMap chunkMap;
    private VoxelComponent<Block> blockComponent;
    private Vec3 dimensions;
    
    public ChunkCollisionComponent(Entity entity, ChunkMap chunkMap, VoxelComponent<Block> blockComponent, Vec3 dimensions) {
        super(entity);
        this.chunkMap = chunkMap;
        this.blockComponent = blockComponent;
        this.dimensions = dimensions;
    }
    
    public void voxelContactCheck(Vec3i worldCoord, Box3 collisionBox, Vec3 deltaMove, List<CollisionContact> contacts) {
        if (!chunkMap.get(blockComponent, worldCoord).isSolid()) return;
        
        Vec3 invEntry = new Vec3(), invExit = new Vec3();
        Vec3 entry = new Vec3(), exit = new Vec3();
        
        Vec3 realCoords = chunkMap.getCoordinateSystem().getRealCoordsFromWorldCoords(worldCoord);
        Box3 voxelBox = new Box3(realCoords, new Vec3(0.5f).add(realCoords));
        
        if (deltaMove.x > 0.0f) { 
          invEntry.x = voxelBox.min.x - collisionBox.max.x;  
          invExit.x = voxelBox.max.x - collisionBox.min.x;
        } else { 
          invEntry.x = voxelBox.max.x - collisionBox.min.x;  
          invExit.x = voxelBox.min.x - collisionBox.max.x;  
        }
        
        if (deltaMove.y > 0.0f) { 
          invEntry.y = voxelBox.min.y - collisionBox.max.y;  
          invExit.y = voxelBox.max.y - collisionBox.min.y;
        } else { 
          invEntry.y = voxelBox.max.y - collisionBox.min.y;  
          invExit.y = voxelBox.min.y - collisionBox.max.y;  
        }
        
        if (deltaMove.z > 0.0f) { 
          invEntry.z = voxelBox.min.z - collisionBox.max.z;  
          invExit.z = voxelBox.max.z - collisionBox.min.z;
        } else { 
          invEntry.z = voxelBox.max.z - collisionBox.min.z;  
          invExit.z = voxelBox.min.z - collisionBox.max.z;  
        }
        
        boolean xNotValid = deltaMove.x == 0;
        boolean yNotValid = deltaMove.y == 0;
        boolean zNotValid = deltaMove.z == 0;
        
        entry.x = xNotValid ? NEGATIVE_INFINITY : invEntry.x / deltaMove.x;
        entry.y = yNotValid ? NEGATIVE_INFINITY : invEntry.y / deltaMove.y;
        entry.z = zNotValid ? NEGATIVE_INFINITY : invEntry.z / deltaMove.z;
        
        exit.x = xNotValid ? POSITIVE_INFINITY : invExit.x / deltaMove.x;
        exit.y = yNotValid ? POSITIVE_INFINITY : invExit.y / deltaMove.y;
        exit.z = zNotValid ? POSITIVE_INFINITY : invExit.z / deltaMove.z;
        
        float tEntry = max(max(entry.x, entry.y), entry.z), tExit = min(min(exit.x, exit.y), exit.z);
        
        if (tEntry < 0f || tEntry > tExit) {
            return;
        }
        Vec3 collisionNormal = new Vec3();
        if (entry.x == tEntry) {
            collisionNormal.x = deltaMove.x > 0 ? -1 : 1;
        } else if (entry.y == tEntry) {
            collisionNormal.y = deltaMove.y > 0 ? -1 : 1;
        } else {
            collisionNormal.z = deltaMove.z > 0 ? -1 : 1;
        }
        contacts.add(new BasicCollisionContact(new Vec3(worldCoord), collisionNormal, tEntry));
    }

    @Override
    public Vec3 check(Vec3 position, Vec3 moveDir, float dt) {
        Vec3 halfDim = new Vec3(dimensions).div(2);
        Box3 collisionBox = new Box3(new Vec3(position).sub(halfDim), halfDim.add(position));
        Vec3 deltaMove = new Vec3(moveDir).mult(dt);
        
        Box3 checkBox = new Box3();
        Box3.sweep(collisionBox, deltaMove, checkBox);
        
        Vec3i worldCoordMin = chunkMap.getCoordinateSystem().getWorldCoordsFromRealCoords(checkBox.min).sub(new Vec3i(1));
        Vec3i worldCoordMax = chunkMap.getCoordinateSystem().getWorldCoordsFromRealCoords(checkBox.max);
        List<CollisionContact> contacts = new ArrayList<>();
        
        Vec3i worldCoord = new Vec3i();
        
        for (worldCoord.x = worldCoordMin.x; worldCoord.x <= worldCoordMax.x; worldCoord.x++)
            for (worldCoord.y = worldCoordMin.y; worldCoord.y <= worldCoordMax.y; worldCoord.y++)
                for (worldCoord.z = worldCoordMin.z; worldCoord.z <= worldCoordMax.z; worldCoord.z++)
                    voxelContactCheck(worldCoord, collisionBox, deltaMove, contacts);
        
        Collections.sort(contacts);

        Vec3 min = new Vec3(NEGATIVE_INFINITY), max = new Vec3(POSITIVE_INFINITY);

        float elapsedTime = 0f;
        
        for (CollisionContact contact : contacts) {
            Vec3 contactPos = contact.getPosition();
            Vec3 contactNormal = contact.getNormal();
            float time = contact.getTime();
            if (contactPos.x <= min.x || contactPos.y <= min.y || contactPos.z <= min.z ||
                contactPos.x >= max.x || contactPos.y >= max.y || contactPos.z >= max.z) {
                continue;
            }
            float remainingTime = time - elapsedTime;
            position.add(new Vec3(deltaMove).mult(remainingTime));
            elapsedTime += remainingTime;
            
            if (contactNormal.x != 0) {
                min.x = deltaMove.x < 0 ? max(min.x, contactPos.x) : min.x;
                max.x = deltaMove.x < 0 ? max.x : min(max.x, contactPos.x);
                deltaMove.x = 0f;
            } else if (contactNormal.y != 0) {
                min.y = deltaMove.y < 0 ? max(min.y, contactPos.y) : min.y;
                max.y = deltaMove.y < 0 ? max.y : min(max.y, contactPos.y);
                deltaMove.y = 0f;
            } else if (contactNormal.z != 0) {
                min.z = deltaMove.z < 0 ? max(min.z, contactPos.z) : min.z;
                max.z = deltaMove.z < 0 ? max.z : min(max.z, contactPos.z);
                deltaMove.z = 0f;
            }
        }
        
        float trem = 1f - elapsedTime;
        position.add(new Vec3(deltaMove).mult(trem));
        
        return position;
    }
}