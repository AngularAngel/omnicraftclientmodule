package net.angle.omnicraft.common;

import com.google.common.collect.ImmutableList;
import com.samrj.devil.math.Vec3i;
import java.util.List;
import net.angle.omnientity.api.Component;
import net.angle.omnientity.api.SteppableComponent;
import net.angle.omniworld.api.chunks.ChunkMap;

/**
 *
 * @author angle
 */
public interface ChunkCrossingTriggerComponent extends SteppableComponent {
    @Override
    public default List<Class<? extends Component>> getComponentInterfaceList() {
        return ImmutableList.of(ChunkCrossingTriggerComponent.class, SteppableComponent.class, Component.class);
    }
    public ChunkMap getChunkMap();
    public Vec3i getCurrentChunkPosition();
    public default boolean inDifferentChunk(Vec3i position) {
        return !getLastChunkPosition().equals(position);
    }
    public Vec3i getLastChunkPosition();
    public void setLastChunkPosition(Vec3i newPosition);
    public void trigger(Vec3i position);
    @Override
    public default void step(float dt) {
        Vec3i position = getCurrentChunkPosition();
        if (inDifferentChunk(position)) {
            trigger(position);
            setLastChunkPosition(position);
        }
    }
}