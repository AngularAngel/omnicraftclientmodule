package net.angle.omnicraft.common;

import com.google.common.collect.ImmutableList;
import java.util.List;
import net.angle.omnientity.api.Component;
import net.angle.omniworld.api.chunks.Chunk;

/**
 *
 * @author angle
 */
public interface ChunkObserverComponent extends Component {
    @Override
    public default List<Class<? extends Component>> getComponentInterfaceList() {
        return ImmutableList.of(ChunkObserverComponent.class, Component.class);
    }
    public int getObservationDistance();
    public void setObservationDistance(int observationDistance);
    public void chunkLoaded(Chunk chunk);
}