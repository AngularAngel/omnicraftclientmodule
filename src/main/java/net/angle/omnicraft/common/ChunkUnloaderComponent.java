package net.angle.omnicraft.common;

import com.google.common.collect.ImmutableList;
import com.samrj.devil.math.Vec3i;
import java.util.List;
import net.angle.omnientity.api.Component;
import net.angle.omnientity.api.SteppableComponent;
import net.angle.omnientity.api.UnpausableComponent;
import net.angle.omniworld.api.chunks.Chunk;

/**
 * 
 * @author angle
 */
public interface ChunkUnloaderComponent extends ChunkCrossingTriggerComponent, UnpausableComponent {
    @Override
    public default List<Class<? extends Component>> getComponentInterfaceList() {
        return ImmutableList.of(ChunkUnloaderComponent.class, ChunkCrossingTriggerComponent.class, UnpausableComponent.class, SteppableComponent.class, Component.class);
    }
    public int getUnloadingDistance();
    public void setUnloadingDistance(int renderDistance);
    public default boolean shouldUnloadChunk(Chunk chunk) {
        return chunk.axialDist(getCurrentChunkPosition()) > getUnloadingDistance() * getChunkMap().getCoordinateSystem().getBlockEdgeLengthOfChunk();
    }
    
    public default void unloadChunk(Chunk chunk) {
        getChunkMap().removeChunk(chunk);
    }
    
    @Override
    public default void trigger(Vec3i position) {
        getChunkMap().forEachChunk((chunk) -> {
            if (shouldUnloadChunk(chunk))
                unloadChunk(chunk);
        });
    }
}