package net.angle.omnicraft.common;

import com.samrj.devil.geo3d.Box3i;
import com.samrj.devil.geo3d.VoxelTrace;
import com.samrj.devil.graphics.Camera3D;
import com.samrj.devil.math.Vec3;
import com.samrj.devil.math.Vec3i;
import java.util.List;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.angle.omniblocks.api.Block;
import net.angle.omniblocks.api.OmniBlocksModule;
import net.angle.omnicraft.server.worldgen.BasicVoxelTypeWorldgen;
import net.angle.omnientity.api.CameraComponent;
import net.angle.omnientity.api.Entity;
import net.angle.omnientity.api.PausableEntityEnvironment;
import net.angle.omnientity.api.PositionComponent;
import net.angle.omnientity.impl.BasicPausableEntityEnvironment;
import net.angle.omnilighting.api.DirectionalLightComponent;
import net.angle.omnilighting.api.OmniLightingModule;
import net.angle.omniregistry.api.RegistryCollection;
import net.angle.omniworld.api.chunks.VoxelComponent;
import net.angle.omniworld.impl.chunks.BasicChunkMap;
import net.angle.omniworld.impl.space.BasicCoordinateSystem;
import net.angle.omniworld.impl.space.PrimaryBlockFaces;
import net.angle.omnilighting.api.PointLightComponent;
import net.angle.omniregistry.api.DatumRegistry;
import net.angle.omniregistry.impl.BasicRegistryCollection;
import net.angle.omniregistry.impl.BasicTextureDefinitionRegistry;
import net.angle.omnisides.api.OmniSidesModule;
import net.angle.omniworld.api.OmniWorldModule;
import net.angle.omniworld.api.space.CoordinateSystem;
import net.angle.omniworld.impl.BasicGameWorld;
import org.lwjgl.opengl.GL13C;

/**
 *
 * @author angle
 */
@RequiredArgsConstructor
public class OmnicraftGameWorld extends BasicGameWorld {
    private final OmnicraftRuntime runtime;
    private final boolean clientside;
    private @Getter PausableEntityEnvironment entities;
    private @Getter DatumRegistry<VoxelComponent> chunkComponents;
    private @Getter VoxelComponent<Block> blocksComponent;
    
    public static final int TEXELS_PER_METER = 16;
    
    public boolean isPaused() {
        return entities.isPaused();
    }
    
    public void pause() {
        entities.pause();
    }
    
    public void unpause() {
        entities.unpause();
    }
    
    public void populateRegistries() {
        RegistryCollection registries = new BasicRegistryCollection();
        setRegistries(registries);
        OmniWorldModule worldModule = runtime.getService(OmniWorldModule.class);
        worldModule.populateRegistries(registries);
        
        registries.addEntry(new BasicTextureDefinitionRegistry<>("Texture Definitions", registries, GL13C.GL_TEXTURE0, "u_texture_palette"));
        
        OmniBlocksModule blocksModule = runtime.getService(OmniBlocksModule.class);
        blocksModule.populateRegistries(registries);
        
        OmniSidesModule sidesModule = runtime.getService(OmniSidesModule.class);
        sidesModule.populateRegistries(registries, PrimaryBlockFaces.values());
        
        OmniLightingModule lightingModule = runtime.getService(OmniLightingModule.class);
        lightingModule.populateRegistries(registries);
        
        if (!clientside) {
            BasicVoxelTypeWorldgen.apply(registries);
        }
        
        entities = (PausableEntityEnvironment) registries.addEntry(new BasicPausableEntityEnvironment("Entities", registries));
    }
    
    public void createChunkMap() {
        chunkComponents = (DatumRegistry<VoxelComponent>) getRegistries().getEntryByName("Chunk Components");
        blocksComponent = chunkComponents.getEntryByName("Blocks");
        setChunkMap(new BasicChunkMap(new BasicCoordinateSystem(16, 0.5f), 
                chunkComponents.getAllEntries()));
    }
    
    @Override
    public void init() {
        populateRegistries();
        createChunkMap();
        super.init();
    }
    
    public Vec3i raycast(Vec3i min, Vec3i max, Vec3 startPos, Vec3 dir) {
        VoxelTrace trace = new VoxelTrace(new Box3i(min, max), startPos, dir);
        while (trace.hasNext()) {
            Vec3i coord = trace.next();
            Block block = getChunkMap().get(blocksComponent, coord);
            if (block != null && block.isDrawable()) 
                return coord;
        }
        return null;
    }
    
    public Vec3i raycast(Entity player, int range) {
        CoordinateSystem coordinateSystem = getChunkMap().getCoordinateSystem();
        Vec3 playerPos = player.getComponent(PositionComponent.class).getPosition().div(coordinateSystem.getRealEdgeLengthOfBlock());
        Vec3i playerBlockPos = new Vec3i((int) playerPos.x, (int) playerPos.y, (int) playerPos.z);
        Camera3D camera = player.getComponent(CameraComponent.class).getCamera();
        return raycast(new Vec3i(-range).add(playerBlockPos), new Vec3i(range).add(playerBlockPos), playerPos, Vec3.mult(camera.forward, coordinateSystem.getRealEdgeLengthOfBlock()));
    }
    
    public Block pickBlock(Entity player, int range) {
        Vec3i pickedCoord = raycast(player, range);
        
        if (pickedCoord != null)
            return getChunkMap().get(blocksComponent, pickedCoord);
        else
            return null;
    }
    
    @Override
    public void destroy(Boolean crashed) {
        entities.destroy();
        super.destroy(crashed);
    }
    
    public <E extends Entity> E addEntity(E e) {
        return entities.addEntry(e);
    }

    @Override
    public void step(float dt) {
        super.step(dt);
        entities.step(dt);
    }

    public List<DirectionalLightComponent> getDirectionalLights() {
        return entities.getComponents(DirectionalLightComponent.class);
    }
    
    public List<PointLightComponent> getPointLights() {
        return entities.getComponents(PointLightComponent.class);
    }
}