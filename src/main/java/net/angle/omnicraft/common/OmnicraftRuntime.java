package net.angle.omnicraft.common;

import java.util.logging.Level;
import java.util.logging.Logger;
import net.angle.omniblocks.api.OmniBlocksModule;
import net.angle.omniclient.api.OmniClientModule;
import net.angle.omnicraft.client.OmnicraftClient;
import net.angle.omnicraft.client.screens.TitleScreen;
import net.angle.omnientity.api.OmniEntityModule;
import net.angle.omninetwork.api.OmniNetworkModule;
import net.angle.omniregistry.api.OmniRegistryModule;
import net.angle.omniresource.api.OmniResourceModule;
import net.angle.omniserialization.api.ObjectSerializerRegistry;
import net.angle.omnisides.api.OmniSidesModule;
import net.angle.omniworld.api.OmniWorldModule;

/**
 *
 * @author angle
 */
public interface OmnicraftRuntime {

    public <T> T getService(Class<T> type);
    
    public default void createAndStartRuntime() {
        OmniClientModule clientService = getService(OmniClientModule.class);
        OmniResourceModule resourceService = getService(OmniResourceModule.class);
        OmnicraftClient client = clientService.setClient(new OmnicraftClient(resourceService.getSharedResourceManager("Client"), "Omnicraft", this));
        client.run(new TitleScreen(client));
    }
    
    public default void prepSerializerRegistry(ObjectSerializerRegistry serializerRegistry) {
        getService(OmniNetworkModule.class).prepSerializerRegistry(serializerRegistry);
        getService(OmniRegistryModule.class).prepSerializerRegistry(serializerRegistry);
        getService(OmniEntityModule.class).prepSerializerRegistry(serializerRegistry);
        getService(OmniWorldModule.class).prepSerializerRegistry(serializerRegistry);
        getService(OmniBlocksModule.class).prepSerializerRegistry(serializerRegistry);
        getService(OmniSidesModule.class).prepSerializerRegistry(serializerRegistry);
        
        try {
            serializerRegistry.registerObject(PlayerConnectedDatagram.class.getConstructor(int.class, boolean.class, int.class), (t) -> {
                return new Object[]{t.getPlayerId(), t.isChunkUnloader(), t.getPriority()};
            });
        } catch (NoSuchMethodException | SecurityException ex) {
            Logger.getLogger(OmnicraftRuntime.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
