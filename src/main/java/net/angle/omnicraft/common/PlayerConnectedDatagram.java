package net.angle.omnicraft.common;

import lombok.Getter;
import lombok.ToString;
import net.angle.omnicraft.client.ClientSession;
import net.angle.omninetwork.impl.AbstractDatagram;

/**
 *
 * @author angle
 */
@ToString
public class PlayerConnectedDatagram extends AbstractDatagram<ClientSession> {
    private final @Getter int playerId;
    private final @Getter boolean chunkUnloader;
    
    public PlayerConnectedDatagram(int playerId, boolean chunkUnloader, int priority) {
        super(priority);
        this.playerId = playerId;
        this.chunkUnloader = chunkUnloader;
    }
    
    @Override
    public void execute(ClientSession target) {
        target.initializePlayer(playerId, chunkUnloader);
    }

    @Override
    public Class<ClientSession> getTargetClass() {
        return ClientSession.class;
    }
}