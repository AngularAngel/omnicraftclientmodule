package net.angle.omnicraft.server;

import com.samrj.devil.math.Vec3i;
import java.util.Objects;
import lombok.Getter;
import net.angle.omnicraft.common.AbstractChunkCrossingTriggerComponent;
import net.angle.omnicraft.common.BasicChunkUnloaderComponent;
import net.angle.omnicraft.common.ChunkUnloaderComponent;
import net.angle.omnicraft.server.worldgen.ChunkLoader;
import net.angle.omnientity.api.Entity;
import net.angle.omniworld.api.chunks.Chunk;
import net.angle.omniworld.api.chunks.ChunkMap;
import net.angle.omniworld.impl.space.CubeIterator;

/**
 *
 * @author angle
 */
public class BasicChunkLoaderComponent extends BasicChunkUnloaderComponent implements ChunkLoaderComponent {
    private final @Getter CubeIterator chunkIterator;
    private final ChunkLoader chunkLoader;
    private @Getter int loadingMargin;
    
    public BasicChunkLoaderComponent(ChunkLoader chunkLoader, int unloadingDistance, int loadingMargin, CubeIterator chunkIterator, ChunkMap chunkMap, Entity entity) {
        super(chunkMap, unloadingDistance, entity);
        this.chunkLoader = chunkLoader;
        this.chunkIterator = chunkIterator;
        this.loadingMargin = loadingMargin;
    }

    public BasicChunkLoaderComponent(ChunkLoader chunkLoader, int unloadingDistance, int loadingMargin, ChunkMap chunkMap, Entity entity) {
        this(chunkLoader, unloadingDistance, loadingMargin, new CubeIterator(0, (x, y, z) -> {}), chunkMap, entity);
        
        chunkIterator.setEdgeDistance(getLoadingDistance());
        chunkIterator.setOperation((x, y, z) -> {
            Vec3i coords = new Vec3i(x, y, z).mult(chunkMap.getCoordinateSystem().getBlockEdgeLengthOfChunk());
            Chunk chunk = chunkMap.getChunk(coords);
            if (Objects.isNull(chunk)) {
                chunkLoader.loadChunk(coords);
            }
        });
    }

    @Override
    public void setLoadingMargin(int loadingMargin) {
        this.loadingMargin = loadingMargin ;
        chunkIterator.setEdgeDistance(getLoadingDistance());
    }

    @Override
    public boolean shouldUnloadChunk(Chunk chunk) {
        return !chunkLoader.chunkWithinAnyLoaderComponentLoadingDistance(chunk);
    }

    @Override
    public void trigger(Vec3i position) {
        super.trigger(position);
        chunkIterator.iterate(Vec3i.div(position, getChunkMap().getCoordinateSystem().getBlockEdgeLengthOfChunk()));
    }
}