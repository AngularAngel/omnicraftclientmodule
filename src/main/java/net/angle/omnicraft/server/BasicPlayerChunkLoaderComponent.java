package net.angle.omnicraft.server;

import com.google.common.collect.ImmutableList;
import com.samrj.devil.math.Vec3i;
import java.util.List;
import java.util.Objects;
import net.angle.omnicraft.common.ChunkObserverComponent;
import net.angle.omnicraft.common.ChunkUnloaderComponent;
import net.angle.omnicraft.server.worldgen.ChunkLoader;
import net.angle.omnientity.api.Component;
import net.angle.omnientity.api.Entity;
import net.angle.omnientity.api.SteppableComponent;
import net.angle.omnientity.api.UnpausableComponent;
import net.angle.omniregistry.api.Registry;
import net.angle.omniworld.api.chunks.Chunk;
import net.angle.omniworld.api.chunks.ChunkMap;
import net.angle.omniworld.api.chunks.VoxelComponent;
import net.angle.omniworld.impl.space.CubeIterator;

/**
 *
 * @author angle
 */
public class BasicPlayerChunkLoaderComponent extends BasicChunkLoaderComponent implements ChunkObserverComponent {
    private final PlayerConnection playerConnection;
    
    public BasicPlayerChunkLoaderComponent(PlayerConnection playerConnection, ChunkLoader chunkLoader, int unloadingDistance, int loadingMargin, Registry<VoxelComponent> voxelComponents, ChunkMap chunkMap, Entity entity) {
        super(chunkLoader, unloadingDistance, loadingMargin, new CubeIterator(0, (x, y, z) -> {}), chunkMap, entity);
        this.playerConnection = playerConnection;
        
        getChunkIterator().setEdgeDistance(getLoadingDistance());
        getChunkIterator().setOperation((x, y, z) -> {
            Vec3i coords = new Vec3i(x, y, z).mult(chunkMap.getCoordinateSystem().getBlockEdgeLengthOfChunk());
            Chunk chunk = chunkMap.getChunk(coords);
            if (Objects.nonNull(chunk)) {
                playerConnection.checkAndSendChunk(chunk);
            } else {
                chunkLoader.loadChunk(coords);
            }
        });
    }
    
    @Override
    public List<Class<? extends Component>> getComponentInterfaceList() {
        return ImmutableList.of(ChunkObserverComponent.class, ChunkLoaderComponent.class, ChunkUnloaderComponent.class, UnpausableComponent.class, SteppableComponent.class, Component.class);
    }
    
    @Override
    public void chunkLoaded(Chunk chunk) {
        if (chunkWithinLoadingDistance(chunk)) {
            playerConnection.unrenderChunk(chunk.getCoordinates());
            playerConnection.checkAndSendChunk(chunk);
        }
    }

    @Override
    public int getObservationDistance() {
        return getUnloadingDistance();
    }

    @Override
    public void setObservationDistance(int observationDistance) {
        setUnloadingDistance(observationDistance);
    }

    @Override
    public void trigger(Vec3i position) {
        super.trigger(position);
        playerConnection.removeUncheckedChunks();
    }
}