package net.angle.omnicraft.server;

import com.google.common.collect.ImmutableList;
import com.samrj.devil.math.Vec3i;
import java.util.List;
import net.angle.omnicraft.common.ChunkUnloaderComponent;
import net.angle.omnientity.api.Component;
import net.angle.omnientity.api.SteppableComponent;
import net.angle.omnientity.api.UnpausableComponent;
import net.angle.omniworld.api.chunks.Chunk;

/**
 *
 * @author angle
 */
public interface ChunkLoaderComponent extends ChunkUnloaderComponent {
    @Override
    public default List<Class<? extends Component>> getComponentInterfaceList() {
        return ImmutableList.of(ChunkLoaderComponent.class, ChunkUnloaderComponent.class, UnpausableComponent.class, SteppableComponent.class, Component.class);
    }
    public int getLoadingMargin();
    public void setLoadingMargin(int loadingDistance);
    public default int getLoadingDistance() {
        return getUnloadingDistance() + getLoadingMargin();
    }
    public default boolean chunkWithinLoadingDistance(Chunk chunk) {
        return chunk.axialDist(getCurrentChunkPosition()) <= getUnloadingDistance() * getChunkMap().getCoordinateSystem().getBlockEdgeLengthOfChunk();
    }
}