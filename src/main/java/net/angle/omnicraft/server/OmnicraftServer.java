package net.angle.omnicraft.server;

import net.angle.omnicraft.server.worldgen.ChunkLoader;
import com.samrj.devil.game.GameWindow;
import com.samrj.devil.game.step.StepDynamicSplit;
import com.samrj.devil.game.step.TimeStepper;
import com.samrj.devil.game.sync.SleepHybrid;
import com.samrj.devil.game.sync.SleepMethod;
import com.samrj.devil.game.sync.Sync;
import com.samrj.devil.math.Vec3;
import com.samrj.devil.math.Vec3i;
import io.netty.channel.ChannelHandlerContext;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.Getter;
import net.angle.omnicraft.common.ChunkCollisionComponent;
import net.angle.omnicraft.common.OmnicraftGameWorld;
import net.angle.omnicraft.common.OmnicraftRuntime;
import net.angle.omnientity.api.Component;
import net.angle.omnientity.api.Entity;
import net.angle.omnientity.api.EntityEnvironment;
import net.angle.omnientity.api.OmniEntityModule;
import net.angle.omnientity.api.PositionComponent;
import net.angle.omnientity.api.UpdateableComponent;
import net.angle.omnientity.impl.BasicEntity;
import net.angle.omnientity.impl.BasicMotionComponent;
import net.angle.omnientity.impl.BasicPositionComponent;
import net.angle.omnientity.impl.BasicRenderingComponent;
import net.angle.omnientity.impl.BasicTimerComponent;
import net.angle.omnigeneration.api.GenerationManager;
import net.angle.omnigeneration.api.process.GenerationProcedure;
import net.angle.omnigeneration.api.process.GenerationRequest;
import net.angle.omnigeneration.impl.BasicGenerationManager;
import net.angle.omnilighting.api.OmniLightingModule;
import net.angle.omninetwork.impl.BasicNettyServer;
import net.angle.omniregistry.api.Registry;
import net.angle.omniregistry.api.RegistryCollection;
import net.angle.omniserialization.api.OmniSerializationModule;
import net.angle.omniserialization.api.ObjectSerializerRegistry;
import net.angle.omniworld.api.chunks.Chunk;
import net.angle.omniworld.impl.chunks.BasicChunkGenerationRequest;
import net.angle.omnilighting.impl.BasicDirectionalLightComponent;
import net.angle.omnilighting.impl.BasicPointLightComponent;
import net.angle.omnilighting.impl.DirectionalLightControllerComponent;
import net.angle.omniserialization.api.ClassIDRegistry;
import net.angle.omniworld.api.chunks.VoxelComponent;

/**
 *
 * @author angle
 */
public class OmnicraftServer extends Thread {
    public static final int RENDER_DISTANCE = 7;
    private final @Getter OmnicraftRuntime runtime;
    private final @Getter WorldType worldType = new WorldType.FloatingIsland();
    
    private final @Getter OmnicraftGameWorld gameWorld;
    private final GenerationProcedure<Chunk, Vec3i> worldgenProcedure;
    private final @Getter GenerationManager<Chunk, Vec3i> generationManager;
    private final @Getter ChunkLoader chunkLoader;
    private final List<PlayerConnection> playerConnections = Collections.synchronizedList(new ArrayList<>());
    private final ObjectSerializerRegistry serializerRegistry;
    private final BasicNettyServer nettyServer;
    
    private final SleepMethod sleeper = new SleepHybrid();
    private final TimeStepper stepper = new StepDynamicSplit(1.0f/120.0f, 1.0f/30.0f);
    private Sync sync;
    
    private long lastFrameTime;
    @Getter private volatile boolean running = false;
    private final int stepLimit = 120;

    public OmnicraftServer(OmnicraftRuntime runtime) {
        this.runtime = runtime;
        gameWorld = new OmnicraftGameWorld(runtime, false);
        generationManager = new BasicGenerationManager<>() {
            @Override
            public void removeRequest(Vec3i identifier) {
                super.removeRequest(identifier);
            
                gameWorld.getChunkMap().removeChunk(identifier);
            }
        };
        chunkLoader = new ChunkLoader(this);
        serializerRegistry = runtime.getService(OmniSerializationModule.class).getSerializerRegistry();
        nettyServer = new BasicNettyServer(serializerRegistry) {
            @Override
            public void playerConnected(ChannelHandlerContext ctx) {
                initializePlayer(ctx);
            }
        };
        worldgenProcedure = worldType.getWorldgenProcedure(this);
    }
    
    public void initWorldGen() {
        chunkLoader.init();
        worldgenProcedure.init();
    }
    
    public void init() {
        setName("Server Thread");
        setDaemon(true);
        gameWorld.init();
        gameWorld.getChunkMap().addChunkUpdateCallback((t) -> {
            for (PlayerConnection playerConnection : playerConnections) {
                playerConnection.chunkUpdated(t);
            }
        });
        
        EntityEnvironment entities = gameWorld.getEntities();
        
        ClassIDRegistry componentInterfaceRegistry = entities.getComponentInterfaceRegistry();
        runtime.getService(OmniEntityModule.class).prepComponentInterfaceRegistry(componentInterfaceRegistry);
        runtime.getService(OmniLightingModule.class).prepComponentInterfaceRegistry(componentInterfaceRegistry);
        
        ClassIDRegistry componentClassRegistry = entities.getComponentClassRegistry();
        runtime.getService(OmniEntityModule.class).prepComponentClassRegistry(componentClassRegistry);
        runtime.getService(OmniLightingModule.class).prepComponentClassRegistry(componentClassRegistry);
        
        Map<Class<? extends Component>, Function<Entity, Component>> componentConstructors = entities.getComponentConstructors();
        runtime.getService(OmniEntityModule.class).prepComponentConstructors(componentConstructors);
        runtime.getService(OmniLightingModule.class).prepComponentConstructors(componentConstructors);
        
        Entity clock = new BasicEntity(entities);
        clock.addComponent(new BasicTimerComponent(clock));
        entities.addEntry(clock);
        
        Entity light = new BasicEntity(entities);
        BasicDirectionalLightComponent dirLight = light.addComponent(new BasicDirectionalLightComponent(light));
        light.addComponent(new DirectionalLightControllerComponent(light, (time) -> {
            time /= 20;
            return new Vec3(0, (float) (Math.sin(time)), (float) (Math.cos(time))).normalize();
        }));
        dirLight.setColor(new Vec3(0.75f, 1, 0.85f));
        //dirLight.setDirection(new Vec3(0, 1, 1).normalize());
        entities.addEntry(light);
        
        light = new BasicEntity(entities);
        dirLight = light.addComponent(new BasicDirectionalLightComponent(light));
        light.addComponent(new DirectionalLightControllerComponent(light, (time) -> {
            time /= 10;
            return new Vec3(-(float) (Math.sin(time)), (float) (Math.sin(time)), (float) (Math.cos(time))).normalize();
        }));
        dirLight.setColor(new Vec3(1.05f, 0.5f, 0.65f));
        entities.addEntry(light);
        
        light = new BasicEntity(entities);
        light.addComponent(new BasicPositionComponent(light)).setPosition(new Vec3(4, 9, 12));
        BasicRenderingComponent renderingComponent = light.addComponent(new BasicRenderingComponent(light));
        renderingComponent.setRenderingData(new Object[]{0, new Object[]{1.5f}});
        renderingComponent.setHasUpdates(true);
        BasicPointLightComponent pointLight = light.addComponent(new BasicPointLightComponent(light));
        pointLight.setColor(new Vec3(40, 1, 0.85f));
        pointLight.setLinearAttenuation(0.25f);
        pointLight.setQuadraticAttenuation(0.25f);
        entities.addEntry(light);
        
        light = new BasicEntity(entities);
        light.addComponent(new BasicPositionComponent(light)).setPosition(new Vec3(-20, 9, 60));
        renderingComponent = light.addComponent(new BasicRenderingComponent(light));
        renderingComponent.setRenderingData(new Object[]{0, new Object[]{0.5f}});
        renderingComponent.setHasUpdates(true);
        pointLight = light.addComponent(new BasicPointLightComponent(light));
        pointLight.setColor(new Vec3(0.95f, 40, 1.55f));
        pointLight.setLinearAttenuation(1.5f);
        pointLight.setQuadraticAttenuation(1.5f);
        entities.addEntry(light);
        
        light = new BasicEntity(entities);
        light.addComponent(new BasicPositionComponent(light)).setPosition(new Vec3(-2, 9, 45));
        renderingComponent = light.addComponent(new BasicRenderingComponent(light));
        renderingComponent.setRenderingData(new Object[]{0, new Object[]{1f}});
        renderingComponent.setHasUpdates(true);
        pointLight = light.addComponent(new BasicPointLightComponent(light));
        pointLight.setColor(new Vec3(0.95f, 1, 40.55f));
        pointLight.setLinearAttenuation(1.5f);
        pointLight.setQuadraticAttenuation(1.5f);
        entities.addEntry(light);
        
        initWorldGen();
        
        getRuntime().prepSerializerRegistry(serializerRegistry);
        
        nettyServer.addDatagramTarget(EntityEnvironment.class, entities);
        nettyServer.init();
        
        start();
    }
    
    public GenerationRequest<Chunk, Vec3i> generateChunk(int x, int y, int z) {
        return generateChunk(new Vec3i(x, y, z));
    }
    
    public GenerationRequest<Chunk, Vec3i> generateChunk(Vec3i coordinates) {
        GenerationRequest<Chunk, Vec3i> request = generationManager.getRequest(coordinates);
        if (Objects.nonNull(request)) {
            if (request.isComplete()) {
                worldgenProcedure.resubmit(request);
            }

            return request;
        } else {
            request = new BasicChunkGenerationRequest(generationManager, coordinates);
            worldgenProcedure.beginGeneration(request);
            chunkLoader.chunkLoading(coordinates);
            return request;
        }
    }
    
    public void initializePlayer(ChannelHandlerContext ctx) {
        EntityEnvironment entities = gameWorld.getEntities();
        Entity entity = new BasicEntity(entities);
        PositionComponent positionComponent = entity.addComponent(new BasicPositionComponent(entity));
        positionComponent.setPosition(new Vec3(-9, 11, 9));
        positionComponent.setHasUpdates(true);
        BasicRenderingComponent renderingComponent = entity.addComponent(new BasicRenderingComponent(entity));
        renderingComponent.setRenderingData(new Object[]{0, new Object[]{0.5f}});
        renderingComponent.setHasUpdates(true);
        entity.addComponent(new BasicMotionComponent(entity));
        entity.addComponent(new ChunkCollisionComponent(entity, gameWorld.getChunkMap(), gameWorld.getBlocksComponent(), new Vec3(0.5f)));
        
        PlayerConnection player = new PlayerConnection(ctx.channel(), gameWorld, entity);
        if (worldType.shouldAddChunkLoader())
            entity.addComponent(new BasicPlayerChunkLoaderComponent(player, chunkLoader, RENDER_DISTANCE, worldType.getGenerationMargin(), (Registry<VoxelComponent>) gameWorld.getRegistries().getEntryByName("Chunk Components"), gameWorld.getChunkMap(), entity));
        
        entity.init();
        player.sendInitialDatagrams(worldType.shouldAddChunkLoader());
        
        gameWorld.addEntity(entity);
        playerConnections.add(player);
        
        if (!worldType.shouldAddChunkLoader()) {
            Entity staticChunkLoader = new BasicEntity(entities);
            positionComponent = staticChunkLoader.addComponent(new BasicPositionComponent(staticChunkLoader));
            positionComponent.setPosition(new Vec3());
            staticChunkLoader.addComponent(new BasicPlayerChunkLoaderComponent(player, chunkLoader, RENDER_DISTANCE, worldType.getGenerationMargin(), (Registry<VoxelComponent>) gameWorld.getRegistries().getEntryByName("Chunk Components"), gameWorld.getChunkMap(), staticChunkLoader));
            staticChunkLoader.init();
            gameWorld.addEntity(staticChunkLoader);
        }
    }
    
    public void sendUpdatesToClients(float dt) {
        if (nettyServer.isRunning()) {
            EntityEnvironment entities = gameWorld.getEntities();
            for (PlayerConnection player : playerConnections) {
                entities.getAllEntries().forEach((entity) -> {
                    player.checkAndSendEntityCreation(entity);
                    player.checkAndSendEntityUpdates(entity);
                });
            }
            entities.getComponents(UpdateableComponent.class).forEach((component) -> {
                component.setHasUpdates(false);
            });
        }
    }
    
    public void step(float dt) {
        try {
            chunkLoader.step(dt);
            nettyServer.readUpdates();
            gameWorld.step(dt);
            sendUpdatesToClients(dt);
        } catch(Exception ex) {
            Logger.getLogger(OmnicraftServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void run() {
        long lastFrameStart = System.nanoTime() - GameWindow.getFrameTargetNano();
        running = true;
        sync = new Sync(stepLimit, sleeper);
        nettyServer.start();
        try {
            while (running) {
                long frameStart = System.nanoTime();
                lastFrameTime = frameStart - lastFrameStart;
                float dt = (float)(lastFrameTime/1_000_000_000.0);
                stepper.step(dt, this::step);
                lastFrameStart = frameStart;

                if (stepLimit > 0)
                    sync.sync();
            }
        } catch (InterruptedException ex) {
            if (!running) return;
            Logger.getLogger(OmnicraftServer.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException("Thread interrupted: ", ex);
        }
    }
    
    @Override
    public void interrupt() {
            nettyServer.destroy();
            generationManager.shutdown();
            running = false; //Make sure the Thread stops in all cases.
            super.interrupt();
            gameWorld.destroy(false);
    }
    
    public RegistryCollection<Registry> getRegistries() {
        return gameWorld.getRegistries();
    }
}