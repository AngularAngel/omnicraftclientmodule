package net.angle.omnicraft.server;

import com.samrj.devil.math.Vec3i;
import io.netty.channel.Channel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import net.angle.omnicraft.common.OmnicraftGameWorld;
import net.angle.omnicraft.common.PlayerConnectedDatagram;
import net.angle.omnicraft.server.worldgen.ChunkLoader;
import net.angle.omnientity.api.Entity;
import net.angle.omnientity.api.PositionComponent;
import net.angle.omnientity.api.MotionComponent;
import net.angle.omnientity.api.UpdateableComponent;
import net.angle.omnientity.api.RenderingComponent;
import net.angle.omnientity.impl.BasicCreateEntityDatagramAction;
import net.angle.omnientity.impl.BasicCreateComponentDatagramAction;
import net.angle.omnientity.impl.BasicEntityEnvironmentDatagram;
import net.angle.omnientity.impl.BasicGetEntityDatagramAction;
import net.angle.omnientity.impl.BasicModifyComponentDatagramAction;
import net.angle.omnientity.impl.BasicMotionComponent;
import net.angle.omnientity.impl.BasicPositionComponent;
import net.angle.omnientity.impl.BasicRenderingComponent;
import net.angle.omnilighting.api.DirectionalLightComponent;
import net.angle.omnilighting.api.PointLightComponent;
import net.angle.omnilighting.impl.BasicDirectionalLightComponent;
import net.angle.omnilighting.impl.BasicPointLightComponent;
import net.angle.omninetwork.api.Datagram;
import net.angle.omninetwork.api.DatagramAction;
import net.angle.omniregistry.api.Datum;
import net.angle.omniregistry.api.Registry;
import net.angle.omniregistry.impl.BasicRegistryDatagram;
import net.angle.omniregistry.impl.BasicSetRegistryDataDatagramAction;
import net.angle.omniserialization.api.ClassIDRegistry;
import net.angle.omniworld.api.chunks.Chunk;
import net.angle.omniworld.api.chunks.VoxelComponent;
import net.angle.omniworld.impl.chunks.datagrams.BasicGameWorldDatagram;
import net.angle.omniworld.impl.chunks.datagrams.BasicGetChunkDatagramAction;

/**
 *
 * @author angle
 */
public class PlayerConnection {
    public final Entity entity;
    private final Channel networkChannel;
    private final Registry<VoxelComponent> voxelComponents;
    private final Set<Entity> sentEntities = new HashSet<>();
    private final Set<Vec3i> renderedChunks = Collections.synchronizedSet(new HashSet<>());
    private final Set<Vec3i> checkedChunks = new HashSet<>();
    private final OmnicraftGameWorld gameWorld;

    public PlayerConnection(Channel channel, OmnicraftGameWorld gameWorld, Entity entity) {
        this.networkChannel = channel;
        this.gameWorld = gameWorld;
        this.voxelComponents = (Registry<VoxelComponent>) gameWorld.getRegistries().getEntryByName("Chunk Components");
        this.entity = entity;
    }
    
    public void sendInitialDatagrams(boolean chunkLoader) {
        send(new PlayerConnectedDatagram(entity.getId(), chunkLoader, 1));
        sentEntities.add(entity);
        
        Registry textureDefinitions = (Registry) gameWorld.getRegistries().getEntryByName("Texture Definitions");
        send(new BasicRegistryDatagram(new BasicSetRegistryDataDatagramAction(textureDefinitions.getId(), (Datum[]) textureDefinitions.getAllEntries().toArray(Datum[]::new)), 1));
        
        Registry blocks = (Registry) gameWorld.getRegistries().getEntryByName("Blocks");
        send(new BasicRegistryDatagram(new BasicSetRegistryDataDatagramAction(blocks.getId(), (Datum[]) blocks.getAllEntries().toArray(Datum[]::new)), 1));
        
        Registry sides = (Registry) gameWorld.getRegistries().getEntryByName("Sides");
        send(new BasicRegistryDatagram(new BasicSetRegistryDataDatagramAction(sides.getId(), (Datum[]) sides.getAllEntries().toArray(Datum[]::new)), 1));
    }
    
    public void send(Datagram datagram) {
        networkChannel.writeAndFlush(datagram);
    }
    
    public void sendChunk(Chunk chunk) {
        List<DatagramAction> actions = new ArrayList<>();
        for (VoxelComponent component : voxelComponents.getAllEntries()) {
            DatagramAction setActionForChunk = component.getSetActionForChunk(chunk);
            if (Objects.nonNull(setActionForChunk))
                actions.add(setActionForChunk);
        }
        if (!actions.isEmpty())
            send(new BasicGameWorldDatagram(new BasicGetChunkDatagramAction(actions.toArray(DatagramAction[]::new), chunk.getCoordinates()), 1));
    }
    
    public void checkAndSendChunk(Chunk chunk) {
        Vec3i coordinates = chunk.getCoordinates();
        if (!renderedChunks.contains(coordinates)) {
            sendChunk(chunk);
            renderedChunks.add(coordinates);
        }
        checkedChunks.add(coordinates);
    }
    
    public void unrenderChunk(Vec3i coords) {
        renderedChunks.remove(coords);
    }
    
    public void chunkUpdated(Chunk chunk) {
        if (renderedChunks.contains(chunk.getCoordinates())) 
            sendChunk(chunk);
    }
    
    public void removeUncheckedChunks() {
        for (Vec3i coords : renderedChunks.toArray(Vec3i[]::new)) {
            if (!checkedChunks.contains(coords))
                unrenderChunk(coords);
        }
        
        checkedChunks.clear();
    }
    
    private void checkAndSendEntityComponent(Class<? extends UpdateableComponent> componentClass, Class<? extends UpdateableComponent> interfaceClass, Entity other, List<DatagramAction> actions) {
        ClassIDRegistry componentClassRegistry = gameWorld.getEntities().getComponentClassRegistry();
        ClassIDRegistry componentInterfaceRegistry = gameWorld.getEntities().getComponentInterfaceRegistry();
        if (other.hasComponent(interfaceClass)) {
            actions.add(new BasicCreateComponentDatagramAction(componentClassRegistry.getID(componentClass)));
            actions.add(new BasicModifyComponentDatagramAction(componentInterfaceRegistry.getID(interfaceClass),
                    other.getComponent(interfaceClass).getUpdate()));
        }
    }
    
    public void checkAndSendEntityCreation(Entity other) {
        if (!sentEntities.contains(other) && (!other.hasComponent(PositionComponent.class) ||
                other.getComponent(PositionComponent.class).getPosition().dist(entity.getComponent(PositionComponent.class).getPosition()) < 16 * 6)) {
            List<DatagramAction> actions = new ArrayList<>();
            ClassIDRegistry componentClassRegistry = gameWorld.getEntities().getComponentClassRegistry();
            checkAndSendEntityComponent(BasicPositionComponent.class, PositionComponent.class, other, actions);
            if (other.hasComponent(MotionComponent.class)) {
                actions.add(new BasicCreateComponentDatagramAction(componentClassRegistry.getID(BasicMotionComponent.class)));
            }
            checkAndSendEntityComponent(BasicDirectionalLightComponent.class, DirectionalLightComponent.class, other, actions);
            checkAndSendEntityComponent(BasicPointLightComponent.class, PointLightComponent.class, other, actions);
            checkAndSendEntityComponent(BasicRenderingComponent.class, RenderingComponent.class, other, actions);
            send(new BasicEntityEnvironmentDatagram(new BasicCreateEntityDatagramAction(other.getId(), actions.toArray(DatagramAction[]::new)), 1));
            sentEntities.add(other);
        }
    }
    
    private void getComponentUpdate(Entity entity, Class<? extends UpdateableComponent> cls, List<DatagramAction> actions) {
        UpdateableComponent component = entity.getComponent(cls);
        if (Objects.nonNull(component) && component.hasUpdates()) {
            ClassIDRegistry componentInterfaceRegistry = gameWorld.getEntities().getComponentInterfaceRegistry();
            actions.add(new BasicModifyComponentDatagramAction(componentInterfaceRegistry.getID(cls), component.getUpdate()));
        }
    }
    
    public void checkAndSendEntityUpdates(Entity other) {
        if (sentEntities.contains(other)) {
            List<DatagramAction> actions = new ArrayList<>();
            ClassIDRegistry componentInterfaceRegistry = gameWorld.getEntities().getComponentInterfaceRegistry();
            for (Class<?> cls : componentInterfaceRegistry.getClasses())
                getComponentUpdate(other, (Class<? extends UpdateableComponent>) cls, actions);
            send(new BasicEntityEnvironmentDatagram(new BasicGetEntityDatagramAction(other.getId(), actions.toArray(DatagramAction[]::new)), 0));
        }
            
    }
}