package net.angle.omnicraft.server;

import com.samrj.devil.math.Vec3i;
import net.angle.omnicraft.server.worldgen.FloatingIslandWorldgen;
import net.angle.omnicraft.server.worldgen.PillarPlainWorldgen;
import net.angle.omnigeneration.api.process.GenerationProcedure;
import net.angle.omniworld.api.chunks.Chunk;

/**
 *
 * @author angle
 */
public interface WorldType {
    public boolean shouldAddChunkLoader();
    public int getLightingRadius();
    public default int getGenerationMargin() {
        return getLightingRadius() * 2;
    }
    GenerationProcedure<Chunk, Vec3i> getWorldgenProcedure(OmnicraftServer server);
    
    public static class FloatingIsland implements WorldType {
        @Override
        public boolean shouldAddChunkLoader() {
            return false;
        }

        @Override
        public int getLightingRadius() {
            return 4;
        }

        @Override
        public GenerationProcedure<Chunk, Vec3i> getWorldgenProcedure(OmnicraftServer server) {
            return new FloatingIslandWorldgen(server.getGenerationManager(), server, server.getChunkLoader());
        }
    }
    
    public static class PillarPlain implements WorldType {
        @Override
        public boolean shouldAddChunkLoader() {
            return true;
        }

        @Override
        public int getLightingRadius() {
            return 6;
        }

        @Override
        public GenerationProcedure<Chunk, Vec3i> getWorldgenProcedure(OmnicraftServer server) {
            return new PillarPlainWorldgen(server.getGenerationManager(), server, server.getChunkLoader());
        }
    }
}