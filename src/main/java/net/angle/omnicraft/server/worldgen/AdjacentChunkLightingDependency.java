package net.angle.omnicraft.server.worldgen;

import com.samrj.devil.math.Vec3i;
import net.angle.omnigeneration.api.GenerationManager;
import net.angle.omnigeneration.api.process.GenerationRequest;
import net.angle.omnigeneration.api.process.GenerationStep;
import net.angle.omniworld.api.chunks.Chunk;
import net.angle.omniworld.impl.chunks.AdjacentChunkDependency;

/**
 *
 * @author angle
 */
public class AdjacentChunkLightingDependency extends AdjacentChunkDependency {
    private final int lightingDepth;

    public AdjacentChunkLightingDependency(int lightingHeight, Vec3i offset, GenerationStep<Chunk, Vec3i> step, GenerationManager<Chunk, Vec3i> generationManager) {
        super(offset, step, generationManager);
        this.lightingDepth = lightingHeight;
    }
    
    @Override
    public boolean isCompleted(GenerationRequest<Chunk, Vec3i> request) {
        return getCoords(request.getIdentifier()).y < lightingDepth || super.isCompleted(request);
    }
    
}
