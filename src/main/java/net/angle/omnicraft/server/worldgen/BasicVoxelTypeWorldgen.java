package net.angle.omnicraft.server.worldgen;

import com.samrj.devil.math.Vec3;
import com.samrj.devil.math.Vec4;
import net.angle.omniblocks.api.Block;
import net.angle.omniblocks.impl.BasicBlockBuilder;
import net.angle.omniregistry.api.Registry;
import net.angle.omniregistry.api.RegistryCollection;
import net.angle.omniregistry.api.RenderableBuilder;
import net.angle.omniregistry.api.TextureDefinition;
import net.angle.omniregistry.api.TextureDefinitionRegistry;
import net.angle.omniregistry.impl.BasicTextureDefinition;
import net.angle.omnisides.api.Side;
import net.angle.omnisides.impl.BasicSide;
import net.angle.omnisides.impl.BasicSideBuilder;
import static net.angle.omnicraft.server.worldgen.TextureDefinitionPatterns.*;

/**
 *
 * @author angle
 */
public class BasicVoxelTypeWorldgen {
    public static void apply(RegistryCollection registries) {
        TextureDefinitionRegistry<TextureDefinition> textureRegistry = (TextureDefinitionRegistry<TextureDefinition>) registries.getEntryByName("Texture Definitions");
                
        textureRegistry.addEntry(new BasicTextureDefinition("Debug",
                                                            textureRegistry,
                                                            combine(sqCheckerboard(new Vec4(COLOR, SET, 0, 0), 4, new Vec4(0, 0, 0, 1), new Vec4(1, 0, 0.5f, 1)),
                                                                    sqCheckerboard(new Vec4(SPECULAR, SET, 0, 0), 4, new Vec4(1, 0, 0, 0), new Vec4(0, 0, 0, 0)),
                                                                    sqCheckerboard(new Vec4(ROUGHNESS + FINAL, SET, 0, 0), 4, new Vec4(0.5f, 0, 0, 0), new Vec4(1, 0, 0, 0)))));
        textureRegistry.addEntry(new BasicTextureDefinition("Empty Side",
                                                            textureRegistry,
                                                            empty(new Vec4(FINAL, 0, 0, 0))));
        textureRegistry.addEntry(new BasicTextureDefinition("Stone",
                                                            textureRegistry,
                                                            combine(mlSimplex(new Vec4(COLOR, SET, 0, 0), 0.5f,
                                                                              new Vec4[]{new Vec4(1024, 1024, 1024, 0), new Vec4(32, 2.5f, 32, 0.666f), new Vec4(0, 32, 0, 0), new Vec4(3, 8, 6, 0.333f), new Vec4(0), new Vec4(0.025f, 0, 0, 0)},//Color Noises!
                                                                              new Vec4(0.15f, 0.15f, 0.15f, 1), new Vec4(0.2f, 0.2f, 0.2f, 1), new Vec4(0.225f, 0.225f, 0.225f, 1), new Vec4(0.3f, 0.3f, 0.3f, 1), new Vec4(0.35f, 0.35f, 0.35f, 1)),//Color Values!
                                                                    randomRange(new Vec4(NORMAL, ADD, 0, 0), 0.0125f), //Normal Variation
                                                                    mlSimplex(new Vec4(SPECULAR, SET, 0, 0), 0.5f,
                                                                              new Vec4[]{new Vec4(1024, 1024, 1024, 0), new Vec4(32, 2.5f, 32, 0.666f), new Vec4(0, 32, 0, 0), new Vec4(3, 8, 6, 0.333f), new Vec4(0), new Vec4(0.025f, 0, 0, 0)}, //Specular Noises!
                                                                              new Vec4(50 / 255f, 0, 0, 0), new Vec4(100 / 255f, 0, 0, 0), new Vec4(150 / 255f, 0, 0, 0), new Vec4(200 / 255f, 0, 0, 0), new Vec4(1, 0, 0, 0)), //Specular Values!
                                                                    mlSimplex(new Vec4(ROUGHNESS + FINAL, SET, 0, 0), 0.85f,
                                                                              new Vec4[]{new Vec4(1024, 1024, 1024, 0), new Vec4(32, 2.5f, 32, 0.666f), new Vec4(0, 32, 0, 0), new Vec4(3, 8, 6, 0.333f), new Vec4(0), new Vec4(0.025f, 0, 0, 0)}, //Roughness Noises!
                                                                              new Vec4(50 / 255f, 0, 0, 0), new Vec4(100 / 255f, 0, 0, 0), new Vec4(150 / 255f, 0, 0, 0), new Vec4(200 / 255f, 0, 0, 0), new Vec4(1, 0, 0, 0))))); //Roughness Values!
        textureRegistry.addEntry(new BasicTextureDefinition("Dirt",
                                                            textureRegistry,
                                                            combine(mlSimplex(new Vec4(COLOR, SET, 0, 0), 0.45f,
                                                                              new Vec4[]{new Vec4(1024, 1024, 1024, 0), new Vec4(28, 4.5f, 28, 0.666f), new Vec4(2, 32, 2, 0), new Vec4(3, 8, 6, 0.333f), new Vec4(0), new Vec4(0.165f, 0, 0, 0)},//Color Noises!
                                                                              new Vec4(85 / 255f, 25 / 255f, 0, 1), new Vec4(100 / 255f, 50 / 255f, 0, 1), new Vec4(100 / 255f, 100 / 255f, 0, 1), new Vec4(85 / 255f, 80 / 255f, 75 / 255f, 1), new Vec4(100 / 255f, 100 / 255f, 100 / 255f, 1)), //Color Values!
                                                                    randomRange(new Vec4(NORMAL, ADD, 0, 0), 0.0125f), //Normal Variation
                                                                    randomList(new Vec4(ROUGHNESS + FINAL, SET, 0, 0), new Vec4(200 / 255f, 0, 0, 0), new Vec4(1, 0, 0, 0)))));
        textureRegistry.addEntry(new BasicTextureDefinition("Sand",
                                                            textureRegistry,
                                                            combine(randomList(new Vec4(COLOR, SET, 0, 0), new Vec4(200 / 255f, 0, 0, 1), new Vec4(200 / 255f, 200 / 255f, 0, 1), new Vec4(200 / 255f, 200 / 255f, 0, 1)),
                                                                    randomRange(new Vec4(NORMAL, ADD, 0, 0), 0.125f),
                                                                    randomList(new Vec4(ROUGHNESS + FINAL, SET, 0, 0), new Vec4(200 / 255f, 0, 0, 0), new Vec4(1, 0, 0, 0)))));
        textureRegistry.addEntry(new BasicTextureDefinition("White",
                                                            textureRegistry,
                                                            combine(mlSimplex(new Vec4(COLOR, SET, 0, 0), -0.65f,
                                                                              new Vec4[]{new Vec4(1024, 1024, 1024, 0), new Vec4(28, 4.5f, 28, 0.666f), new Vec4(2, 32, 2, 0), new Vec4(3, 8, 6, 0.333f), new Vec4(0), new Vec4(0.165f, 0, 0, 0)},//Color Noises!
                                                                              new Vec4(0.5f, 0.5f, 0.5f, 1), new Vec4(0.75f, 0.75f, 0.75f, 1), new Vec4(1, 1, 1, 1)), //Color Values!
                                                                    solid(new Vec4(ROUGHNESS, SET, 0, 0), new Vec4(1, 0, 0, 0)),
                                                                    mlSimplex(new Vec4(LIGHT + FINAL, SET, 0, 0), 0.45f,
                                                                              new Vec4[]{new Vec4(1024, 1024, 1024, 0), new Vec4(28, 4.5f, 28, 0.666f), new Vec4(2, 32, 2, 0), new Vec4(3, 8, 6, 0.333f), new Vec4(0), new Vec4(0.165f, 0, 0, 0)},//Color Noises!
                                                                              new Vec4(0.25f, 0.25f, 0.25f, 1), new Vec4(0.5f, 0.5f, 0.5f, 1), new Vec4(0.75f, 0.75f, 0.75f, 1)))));
        textureRegistry.addEntry(new BasicTextureDefinition("Red Crystal",
                                                            textureRegistry,
                                                            combine(solid(new Vec4(COLOR, SET, 0, 0), new Vec4(128, 8, 8, 200).div(255f)),
                                                                    randomRange(new Vec4(NORMAL, ADD, 0, 0), 0.125f),
                                                                    solid(new Vec4(SPECULAR, 0, 0, 0), new Vec4(1, 0, 0, 0)),
                                                                    solid(new Vec4(ROUGHNESS + FINAL, SET, 0, 0), new Vec4(64 / 255f, 0, 0, 0)))));
        textureRegistry.addEntry(new BasicTextureDefinition("Blue Crystal",
                                                            textureRegistry,
                                                            combine(solid(new Vec4(COLOR, SET, 0, 0), new Vec4(8, 8, 128, 200).div(255f)),
                                                                    randomRange(new Vec4(NORMAL, ADD, 0, 0), 0.125f),
                                                                    solid(new Vec4(SPECULAR, SET, 0, 0), new Vec4(1, 0, 0, 0)),
                                                                    solid(new Vec4(ROUGHNESS + FINAL, 0, 0, 0), new Vec4(64 / 255f, 0, 0, 0)))));
        textureRegistry.addEntry(new BasicTextureDefinition("Red Crystal Ray Marching",
                                                            textureRegistry,
                                                            flatRayMarching(textureRegistry.getEntryByName("Red Crystal").getId())));
        textureRegistry.addEntry(new BasicTextureDefinition("Blue Crystal Ray Marching",
                                                            textureRegistry,
                                                            flatRayMarching(textureRegistry.getEntryByName("Blue Crystal").getId())));
        textureRegistry.addEntry(new BasicTextureDefinition("Wood",
                                                            textureRegistry,
                                                            combine(mlSimplex(new Vec4(COLOR, SET, 0, 0), 0.5f,
                                                                              new Vec4[]{new Vec4(1024, 1024, 1024, 0), new Vec4(32, 2.5f, 32, 0.666f), new Vec4(0, 32, 0, 0), new Vec4(3, 8, 6, 0.333f), new Vec4(0), new Vec4(0.025f, 0, 0, 0)},//Color Noises!
                                                                              new Vec4(0.35f, 0.35f, 0.15f, 1), new Vec4(0.4f, 0.4f, 0.2f, 1), new Vec4(0.425f, 0.425f, 0.225f, 1), new Vec4(0.5f, 0.5f, 0.3f, 1), new Vec4(0.65f, 0.65f, 0.35f, 1)),//Color Values!
                                                                    randomRange(new Vec4(NORMAL, ADD, 0, 0), 0.0125f), //Normal Variation
                                                                    mlSimplex(new Vec4(SPECULAR, SET, 0, 0), 0.5f,
                                                                              new Vec4[]{new Vec4(1024, 1024, 1024, 0), new Vec4(32, 2.5f, 32, 0.666f), new Vec4(0, 32, 0, 0), new Vec4(3, 8, 6, 0.333f), new Vec4(0), new Vec4(0.025f, 0, 0, 0)}, //Specular Noises!
                                                                              new Vec4(50 / 255f, 0, 0, 0), new Vec4(100 / 255f, 0, 0, 0), new Vec4(150 / 255f, 0, 0, 0), new Vec4(200 / 255f, 0, 0, 0), new Vec4(1, 0, 0, 0)), //Specular Values!
                                                                    mlSimplex(new Vec4(ROUGHNESS + FINAL, SET, 0, 0), 0.85f,
                                                                              new Vec4[]{new Vec4(1024, 1024, 1024, 0), new Vec4(32, 2.5f, 32, 0.666f), new Vec4(0, 32, 0, 0), new Vec4(3, 8, 6, 0.333f), new Vec4(0), new Vec4(0.025f, 0, 0, 0)}, //Roughness Noises!
                                                                              new Vec4(50 / 255f, 0, 0, 0), new Vec4(100 / 255f, 0, 0, 0), new Vec4(150 / 255f, 0, 0, 0), new Vec4(200 / 255f, 0, 0, 0), new Vec4(1, 0, 0, 0))))); //Roughness Values!
        textureRegistry.addEntry(new BasicTextureDefinition("Leaves",
                                                            textureRegistry,
                                                            combine(valueNoise(new Vec4(COLOR, SET, 0, 0), 105 / 255f, new Vec4(8, 128, 8, 255).div(255f)),
                                                                    //thresholdValueNoise(new Vec4(COLOR, THRESHOLDSET, 0, 0), 0.75f, new Vec4(1, 0, 0, 0)),
                                                                    randomRange(new Vec4(NORMAL, ADD, 0, 0), 0.125f),
                                                                    //valueNoise(new Vec4(SPECULAR, SET, 0, 0), 155 / 255f, new Vec4(0.5f, 0, 0, 0)),
                                                                    valueNoise(new Vec4(ROUGHNESS + FINAL, SET, 0, 0), 155 / 255f, new Vec4(64 / 255f, 0, 0, 0)))));
        textureRegistry.addEntry(new BasicTextureDefinition("Leaves Ray Marching",
                                                            textureRegistry,
                                                            multipleConditionRayMarching(
                                                                    valueNoise1DRayMarching(new Vec3(1, 0, 0), 3, 0.45f, textureRegistry.getEntryByName("Leaves").getId()),
                                                                    valueNoise1DRayMarching(new Vec3(-1, 0, 0), 7, 0.45f, textureRegistry.getEntryByName("Leaves").getId()),
                                                                    valueNoise1DRayMarching(new Vec3(0, 1, 0), 5, 0.45f, textureRegistry.getEntryByName("Leaves").getId()),
                                                                    valueNoise1DRayMarching(new Vec3(0, -1, 0), 9, 0.45f, textureRegistry.getEntryByName("Leaves").getId()),
                                                                    valueNoise1DRayMarching(new Vec3(0, 0, 1), 1, 0.45f, textureRegistry.getEntryByName("Leaves").getId()),
                                                                    valueNoise1DRayMarching(new Vec3(0, 0, -1), 11, 0.45f, textureRegistry.getEntryByName("Leaves").getId()),
                                                                    internalBlockRayMarching(4f, 2f, 6f, new Vec4(0, 0, 0, textureRegistry.getEntryByName("Wood").getId()))
                                                            )));
        textureRegistry.addEntry(new BasicTextureDefinition("Grass",
                                                            textureRegistry,
                                                            transparentValueNoise(new Vec4(COLOR + FINAL, MIX, 0, 0), 205 / 255f, new Vec4(0, 128 / 255f, 0, 1))));
        textureRegistry.addEntry(new BasicTextureDefinition("Moss",
                                                            textureRegistry,
                                                            transparentValueNoise(new Vec4(COLOR + FINAL, MIX, 0, 0), 105 / 255f, new Vec4(0, 128 / 255f, 0, 1))));

        Registry<Block> blocksRegistry = (Registry<Block>) registries.getEntryByName("Blocks");
        Registry<Side> sidesRegistry = (Registry<Side>) registries.getEntryByName("Sides");

        BasicBlockBuilder blockBuilder = new BasicBlockBuilder();
        blockBuilder.setName("Empty Block");
        blockBuilder.setRegistry(blocksRegistry);
        blockBuilder.setTextureDefinitionID(textureRegistry.getEntryByName("Debug").getId());
        blockBuilder.setTransparent(true);
        blockBuilder.setDrawable(false);
        blockBuilder.setSolid(false);
        blocksRegistry.addEntry(blockBuilder.build());

        RenderableBuilder<BasicSide> sideBuilder = new BasicSideBuilder();
        sideBuilder.setName("Empty Side");
        sideBuilder.setRegistry(sidesRegistry);
        sideBuilder.setTextureDefinitionID(textureRegistry.getEntryByName("Empty Side").getId());
        sideBuilder.setTransparent(true);
        sideBuilder.setDrawable(false);
        sidesRegistry.addEntry(sideBuilder.build());

        blockBuilder = new BasicBlockBuilder();
        blockBuilder.setName("Dirt Block");
        blockBuilder.setRegistry(blocksRegistry);
        blockBuilder.setSolid(true);
        blockBuilder.setTextureDefinitionID(textureRegistry.getEntryByName("Dirt").getId());
        blocksRegistry.addEntry(blockBuilder.build());

        blockBuilder = new BasicBlockBuilder();
        blockBuilder.setName("Stone Block");
        blockBuilder.setRegistry(blocksRegistry);
        blockBuilder.setSolid(true);
        blockBuilder.setTextureDefinitionID(textureRegistry.getEntryByName("Stone").getId());
        blocksRegistry.addEntry(blockBuilder.build());

        blockBuilder = new BasicBlockBuilder();
        blockBuilder.setName("Sand Block");
        blockBuilder.setRegistry(blocksRegistry);
        blockBuilder.setSolid(true);
        blockBuilder.setTextureDefinitionID(textureRegistry.getEntryByName("Sand").getId());
        blocksRegistry.addEntry(blockBuilder.build());

        blockBuilder = new BasicBlockBuilder();
        blockBuilder.setName("White Block");
        blockBuilder.setRegistry(blocksRegistry);
        blockBuilder.setSolid(true);
        blockBuilder.setTextureDefinitionID(textureRegistry.getEntryByName("White").getId());
        blocksRegistry.addEntry(blockBuilder.build());

        blockBuilder = new BasicBlockBuilder();
        blockBuilder.setName("Red Crystal Block");
        blockBuilder.setTransparent(true);
        blockBuilder.setRegistry(blocksRegistry);
        blockBuilder.setSolid(true);
        blockBuilder.setTextureDefinitionID(textureRegistry.getEntryByName("Red Crystal Ray Marching").getId());
        blocksRegistry.addEntry(blockBuilder.build());

        blockBuilder = new BasicBlockBuilder();
        blockBuilder.setName("Blue Crystal Block");
        blockBuilder.setTransparent(true);
        blockBuilder.setRegistry(blocksRegistry);
        blockBuilder.setSolid(true);
        blockBuilder.setTextureDefinitionID(textureRegistry.getEntryByName("Blue Crystal Ray Marching").getId());
        blocksRegistry.addEntry(blockBuilder.build());

        blockBuilder = new BasicBlockBuilder();
        blockBuilder.setName("Leaf Block");
        blockBuilder.setTransparent(true);
        blockBuilder.setRegistry(blocksRegistry);
        blockBuilder.setSolid(false);
        blockBuilder.setTextureDefinitionID(textureRegistry.getEntryByName("Leaves Ray Marching").getId());
        blocksRegistry.addEntry(blockBuilder.build());

        sideBuilder = new BasicSideBuilder();
        sideBuilder.setName("Grass Side");
        sideBuilder.setRegistry(sidesRegistry);
        sideBuilder.setTextureDefinitionID(textureRegistry.getEntryByName("Grass").getId());
        sidesRegistry.addEntry(sideBuilder.build());

        sideBuilder = new BasicSideBuilder();
        sideBuilder.setName("Moss Side");
        sideBuilder.setRegistry(sidesRegistry);
        sideBuilder.setTextureDefinitionID(textureRegistry.getEntryByName("Moss").getId());
        sidesRegistry.addEntry(sideBuilder.build());
    }
}
