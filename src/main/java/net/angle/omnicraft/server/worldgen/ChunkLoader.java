package net.angle.omnicraft.server.worldgen;

import com.samrj.devil.math.Vec3i;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import net.angle.omnicraft.common.ChunkObserverComponent;
import net.angle.omnicraft.server.ChunkLoaderComponent;
import net.angle.omnicraft.server.OmnicraftServer;
import net.angle.omniworld.api.chunks.Chunk;
import net.angle.omniworld.api.chunks.ChunkMap;

/**
 * This class is responsible for loading and unloading chunks, as directed by other classes.
 * @author angle
 */
public class ChunkLoader {
    private ChunkMap chunkMap;
    private final OmnicraftServer server;
    private final Set<Vec3i> chunksToLoad = Collections.synchronizedSet(new HashSet<>());
    private final Set<Vec3i> chunksLoading = Collections.synchronizedSet(new HashSet<>());

    public ChunkLoader(OmnicraftServer server) {
        this.server = server;
    }
    
    public void init() {
        this.chunkMap = server.getGameWorld().getChunkMap();
    }
    
    public boolean chunkWithinAnyLoaderComponentLoadingDistance(Chunk chunk) {
        for (ChunkLoaderComponent chunkLoaderComponent : server.getGameWorld().getEntities().getComponents(ChunkLoaderComponent.class)) {
            if (chunkLoaderComponent.chunkWithinLoadingDistance(chunk))
                return true;
        }
        return false;
    }

    public void unloadChunk(Chunk chunk) {
        Vec3i coords = chunk.getCoordinates();
        //chunkMap.removeChunk(chunk);
        if (chunksLoading.contains(coords))
            chunksLoading.remove(coords);
        if (chunksToLoad.contains(coords))
            chunksToLoad.remove(coords);
    }
    
    public void loadChunk(Vec3i coords) {
        if (!chunksLoading.contains(coords))
            chunksToLoad.add(coords);
    }
    
    public void chunkLoading(Vec3i coordinate) {
        chunksLoading.add(coordinate);
    }
    
    public void chunkLoaded(Chunk chunk) {
        for (ChunkObserverComponent chunkObserverComponent : server.getGameWorld().getEntities().getComponents(ChunkObserverComponent.class)) {
            chunkObserverComponent.chunkLoaded(chunk);
        }
        chunksLoading.remove(chunk.getCoordinates());
        chunksToLoad.remove(chunk.getCoordinates());
    }
    
    public synchronized void step(float dt) {
        for (Vec3i coordinate : chunksToLoad.toArray(Vec3i[]::new)) {
            if (!chunksLoading.contains(coordinate)) {
                server.generateChunk(coordinate);
            }
        }
        chunksToLoad.clear();
    }
}