package net.angle.omnicraft.server.worldgen;

import com.samrj.devil.math.Vec3i;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.angle.omniblocks.api.Block;
import net.angle.omnigeneration.api.process.GenerationRequest;
import net.angle.omnigeneration.impl.SimpleGenerationStep;
import net.angle.omnilighting.api.LightingChannel;
import net.angle.omniregistry.api.DatumRegistry;
import net.angle.omniworld.api.chunks.Chunk;
import net.angle.omniworld.api.chunks.VoxelComponent;
import net.angle.omniworld.api.space.BlockFace;

/**
 *
 * @author angle
 */
@RequiredArgsConstructor
public class DiagonalChunkLightingGeneration extends SimpleGenerationStep<Chunk, Vec3i> {
    private final DatumRegistry<VoxelComponent> chunkComponents;
    private final @Getter LightingChannel channel;
    private final boolean top;
    private final int lightingHeight, blockEdgeLengthOfChunk;
    private final @Getter DiagonalChunkLightingGeneration secondHalf;
    private VoxelComponent<Block> blocksComponent;
    private VoxelComponent<Long> lightingComponent;
    private long saturatedChannels;

    public DiagonalChunkLightingGeneration(DatumRegistry<VoxelComponent> chunkComponents, LightingChannel channel, boolean top, int lightingHeight, int blockEdgeLengthOfChunk) {
        super();
        this.chunkComponents = chunkComponents;
        this.channel = channel;
        this.top = top;
        this.lightingHeight = lightingHeight;
        this.blockEdgeLengthOfChunk = blockEdgeLengthOfChunk;
        this.secondHalf = new DiagonalChunkLightingGeneration(chunkComponents, channel, !top, lightingHeight, blockEdgeLengthOfChunk, this);
    }
    
    @Override
    public void init() {
        blocksComponent = chunkComponents.getEntryByName("Blocks");
        lightingComponent = chunkComponents.getEntryByName("Lighting");
        saturatedChannels = channel.saturateChannel(0);
    }
    
    private boolean aboveIsLit(int x, int z, Vec3i aboveCoords, Chunk aboveChunk) {
        Vec3i above = new Vec3i(x, 0, z);
        return aboveCoords.y >= lightingHeight || ((aboveChunk.get(lightingComponent, above) & saturatedChannels) == saturatedChannels &&
                (aboveChunk.get(lightingComponent, above.sub(channel.getDiagonalDir())) & saturatedChannels) == saturatedChannels);
    }
    
    private boolean sideIsLit(Vec3i sideVoxel, Chunk sideChunk) {
        return (sideChunk.get(lightingComponent, sideVoxel) & saturatedChannels) == saturatedChannels && (sideChunk.getCoordinates().y >= lightingHeight || (sideChunk.get(lightingComponent, new Vec3i(0, 1, 0).add(sideVoxel)) & saturatedChannels) == saturatedChannels);
    }
    
    private void lightDiagonally(Chunk chunk, Vec3i voxelCoords, boolean topNext) {
        while (voxelCoords.y >= 0 && voxelCoords.x >= 0 && voxelCoords.x < blockEdgeLengthOfChunk && voxelCoords.z >= 0 && voxelCoords.z < blockEdgeLengthOfChunk) {
            if (!chunk.get(blocksComponent, voxelCoords).isTransparent())
                break;
            
            chunk.set(lightingComponent, voxelCoords, chunk.get(lightingComponent, voxelCoords) | saturatedChannels);
            if (topNext)
                voxelCoords.y--;
            else
                voxelCoords.sub(channel.getDiagonalDir());
            topNext = !topNext;
        }
    }
    
    @Override
    public void process(GenerationRequest<Chunk, Vec3i> request) {
        Chunk chunk = request.getResult();
        Vec3i identifier = request.getIdentifier();
        Chunk neigboringChunk = null;
        if (top) {
            identifier.y += blockEdgeLengthOfChunk;
            if (identifier.y <= lightingHeight)
                neigboringChunk = request.getGenerationManager().getRequest(identifier).getResult();
        } else {
            identifier.add(channel.getDiagonalDir().mult(blockEdgeLengthOfChunk));
            neigboringChunk = request.getGenerationManager().getRequest(identifier).getResult();
        }
        if (top) {
            for (int x = 0; x < blockEdgeLengthOfChunk; x++)
                for (int z = 0; z < blockEdgeLengthOfChunk; z++) {
                    if (aboveIsLit(x, z, identifier, neigboringChunk))
                        lightDiagonally(chunk, new Vec3i(x, blockEdgeLengthOfChunk - 1, z), false);
                }
        } else {
            BlockFace face = channel.getDiagonalFace();
            Vec3i acrossCoords = face.moveIn(face.getStartingPosition().mult(blockEdgeLengthOfChunk - 1), blockEdgeLengthOfChunk - 1);
            for (int i = 0; i < blockEdgeLengthOfChunk; i++) {
                Vec3i downwardCoords = new Vec3i(acrossCoords);
                for (int j = 0; j < blockEdgeLengthOfChunk; j++) {
                    if (sideIsLit(face.moveIn(new Vec3i(downwardCoords), -(blockEdgeLengthOfChunk - 1)), neigboringChunk))
                        lightDiagonally(chunk, new Vec3i(downwardCoords), true);

                    face.moveDown(downwardCoords);

                }
                face.moveAcross(acrossCoords);
            }
        }
        
        request.getResult().getChunkMap().chunkUpdated(request.getResult());
    }
}