package net.angle.omnicraft.server.worldgen;

import com.samrj.devil.math.Vec3i;
import lombok.RequiredArgsConstructor;
import net.angle.omniblocks.api.Block;
import net.angle.omnigeneration.api.process.GenerationRequest;
import net.angle.omnigeneration.impl.SimpleGenerationStep;
import net.angle.omniregistry.api.DatumRegistry;
import net.angle.omnisides.api.Side;
import net.angle.omniworld.api.chunks.Chunk;
import net.angle.omniworld.api.chunks.VoxelComponent;

/**
 *
 * @author angle
 */
@RequiredArgsConstructor
public class FloatingIslandChunkGeneration extends SimpleGenerationStep<Chunk, Vec3i> {
    private final DatumRegistry<VoxelComponent> chunkComponents;
    private final DatumRegistry<Block> blocksRegistry;
    private final DatumRegistry<Side> sidesRegistry;
    private final int radius;

    @Override
    public void process(GenerationRequest<Chunk, Vec3i> request) {
        Vec3i coordinates = request.getIdentifier();
        Chunk chunk = request.getResult();
        VoxelComponent blocksComponent = chunkComponents.getEntryByName("Blocks");
        if (coordinates.squareLength() < radius * radius) {
            if (coordinates.y < 0) {
                chunk.setAll(blocksComponent, blocksRegistry.getEntryByName("Stone Block"));
                chunk.setAll(chunkComponents.getEntryByName("Top Sides"), sidesRegistry.getEntryByName("Empty Side"));
            } else if (coordinates.y == 0) {
                chunk.setAll(blocksComponent, blocksRegistry.getEntryByName("Dirt Block"));
                chunk.setAll(chunkComponents.getEntryByName("Top Sides"), sidesRegistry.getEntryByName("Grass Side"));
            } else if (coordinates.x == 16 && coordinates.y == 16 && coordinates.z == 16) {
                
                Block leafBlock = blocksRegistry.getEntryByName("Leaf Block");
            
                for (int i = 0; i < 4; i += 4) {
                    for (int j = 0; j < 2; j += 2) {
                        chunk.set(blocksComponent, i + 3, 0, j + 6, leafBlock);
                        chunk.set(blocksComponent, i + 3, 1, j + 6, leafBlock);
                        chunk.set(blocksComponent, i + 3, 2, j + 6, leafBlock);
                        chunk.set(blocksComponent, i + 3, 3, j + 6, leafBlock);
                        chunk.set(blocksComponent, i + 3, 4, j + 6, leafBlock);
                    }
                }
                
                Block redCrystalBlock = blocksRegistry.getEntryByName("Red Crystal Block");
                Block blueCrystalBlock = blocksRegistry.getEntryByName("Blue Crystal Block");

                for (int i = 0; i < 4; i += 4) {
                    for (int j = 0; j < 2; j += 2) {
                        chunk.set(blocksComponent, i + 6, 0, j + 7, redCrystalBlock);
                        chunk.set(blocksComponent, i + 6, 1, j + 7, redCrystalBlock);
                        chunk.set(blocksComponent, i + 6, 2, j + 7, redCrystalBlock);
                        chunk.set(blocksComponent, i + 6, 3, j + 7, redCrystalBlock);
                        chunk.set(blocksComponent, i + 6, 4, j + 7, redCrystalBlock);

                        chunk.set(blocksComponent, i + 7, 0, j + 7, blueCrystalBlock);
                        chunk.set(blocksComponent, i + 7, 1, j + 7, blueCrystalBlock);
                        chunk.set(blocksComponent, i + 7, 2, j + 7, blueCrystalBlock);
                        chunk.set(blocksComponent, i + 7, 3, j + 7, blueCrystalBlock);
                        chunk.set(blocksComponent, i + 7, 4, j + 7, blueCrystalBlock);
                    }
                }
            }
        }
    }
}
