package net.angle.omnicraft.server.worldgen;

import com.samrj.devil.math.Vec3i;
import lombok.RequiredArgsConstructor;
import net.angle.omniblocks.api.Block;
import net.angle.omnigeneration.impl.SimpleGenerationStep;
import net.angle.omnigeneration.api.process.GenerationRequest;
import net.angle.omniregistry.api.DatumRegistry;
import net.angle.omnisides.api.Side;
import net.angle.omniworld.api.chunks.Chunk;
import net.angle.omniworld.api.chunks.VoxelComponent;

/**
 *
 * @author angle
 */
@RequiredArgsConstructor
public class HollowSphereChunkGeneration extends SimpleGenerationStep<Chunk, Vec3i> {
    private final DatumRegistry<VoxelComponent> chunkComponents;
    private final DatumRegistry<Block> blocksRegistry;
    private final DatumRegistry<Side> sidesRegistry;
    private final int innerRadius, outerRadius;

    @Override
    public void process(GenerationRequest<Chunk, Vec3i> request) {
        Vec3i coordinates = request.getIdentifier();
        coordinates.y -= 16;
        Chunk chunk = request.getResult();
        if (coordinates.squareLength() > innerRadius * innerRadius && coordinates.squareLength() < outerRadius * outerRadius) {
            chunk.setAll(chunkComponents.getEntryByName("Blocks"), blocksRegistry.getEntryByName("Stone Block"));
            chunk.setAll(chunkComponents.getEntryByName("Top Sides"), sidesRegistry.getEntryByName("Empty Side"));
        }
    }
}