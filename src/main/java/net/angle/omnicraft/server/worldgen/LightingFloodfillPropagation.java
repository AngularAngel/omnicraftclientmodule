package net.angle.omnicraft.server.worldgen;

import com.samrj.devil.geo3d.Box3i;
import com.samrj.devil.math.Vec3i;
import java.util.ArrayDeque;
import java.util.HashSet;
import java.util.Objects;
import java.util.Queue;
import java.util.Set;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.angle.omniblocks.api.Block;
import net.angle.omnigeneration.api.process.GenerationRequest;
import net.angle.omnigeneration.impl.SimpleGenerationStep;
import net.angle.omnilighting.api.LightingChannel;
import net.angle.omniregistry.api.DatumRegistry;
import net.angle.omniworld.api.chunks.Chunk;
import net.angle.omniworld.api.chunks.VoxelComponent;
import net.angle.omniworld.api.space.Direction;
import net.angle.omniworld.impl.space.PrimaryDirections;

/**
 *
 * @author angle
 */
@RequiredArgsConstructor
public class LightingFloodfillPropagation extends SimpleGenerationStep<Chunk, Vec3i> {
    private final DatumRegistry<VoxelComponent> chunkComponents;
    private VoxelComponent<Block> blocksComponent;
    private VoxelComponent<Long> lightingComponent;
    private final @Getter LightingChannel channel;
    private final long decrease;
    
    
    @Override
    public void init() {
        blocksComponent = chunkComponents.getEntryByName("Blocks");
        lightingComponent = chunkComponents.getEntryByName("Lighting");
    }
    
    public void populateInitialSpread(Chunk chunk, Queue<LightingNode> increaseQueue) {
        chunk.forEachWithin(1, (x, y, z) -> {
            propagate(increaseQueue, null, chunk, new Vec3i(x, y, z), channel.getValue(chunk.get(lightingComponent, x, y, z)));
        });
    }
    
    @Override
    public void process(GenerationRequest<Chunk, Vec3i> request) {
        Chunk chunk = request.getResult();
        
        if ((chunk.isHomogenous(blocksComponent) && !chunk.get(blocksComponent, 0, 0, 0).isTransparent()) ||
            (chunk.isHomogenous(lightingComponent) && channel.getValue(chunk.get(lightingComponent, 0, 0, 0)) >= channel.getSaturationValue()))
            return;
        
        Queue<LightingNode> increaseQueue = new ArrayDeque<>(0);
        Queue<LightingNode> overflowQueue = new ArrayDeque<>(0);
        
        populateInitialSpread(chunk, increaseQueue);
        
        while (!increaseQueue.isEmpty())
            increaseQueue.poll().increaseAndPropagate(chunk, increaseQueue, overflowQueue);
        
        request.getResult().getChunkMap().chunkUpdated(request.getResult());
        
        Set<Chunk> otherChunks = new HashSet<>();
        
        while (!overflowQueue.isEmpty()) {
            Chunk otherChunk = chunk.getChunkFor(overflowQueue.poll().pos);
            if (Objects.nonNull(otherChunk))
                otherChunks.add(otherChunk);
        }
        
        for (Chunk otherChunk : otherChunks) {
            GenerationRequest<Chunk, Vec3i> otherRequest = request.getGenerationManager().getRequest(otherChunk.getCoordinates());
            if (Objects.nonNull(otherRequest))
                otherRequest.submitTicket(getTicket(otherRequest));
        }
    }
    
    public void propagate(Queue<LightingNode> increaseQueue, Queue<LightingNode> overflowQueue, Chunk chunk, Vec3i pos, long lightingValue) {
        if (lightingValue > decrease)
            for (Direction direction : PrimaryDirections.values()) {
                Vec3i dirPos = direction.step(new Vec3i(pos));
                if (chunk.containsCoordinates(dirPos)) {
                    if (chunk.get(blocksComponent, pos).isTransparent())
                        increaseQueue.add(new LightingNode(dirPos, lightingValue - decrease));
                } else if (Objects.nonNull(overflowQueue))
                    overflowQueue.add(new LightingNode(dirPos, lightingValue - decrease));
            }
    }
    
    @RequiredArgsConstructor
    public class LightingNode {
        private final Vec3i pos;
        public final long lightingValue;
        
        public void increaseAndPropagate(Chunk chunk, Queue<LightingNode> increaseQueue, Queue<LightingNode> overflowQueue) {
            Long existingLight = chunk.get(lightingComponent, pos);
            if (channel.getValue(existingLight) < lightingValue) {
                chunk.set(lightingComponent, pos, channel.setValue(existingLight, lightingValue));
                
                propagate(increaseQueue, overflowQueue, chunk, pos, lightingValue);
            }
        }
    }
}