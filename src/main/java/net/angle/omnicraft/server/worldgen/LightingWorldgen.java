package net.angle.omnicraft.server.worldgen;

import com.samrj.devil.math.Vec3i;
import java.util.ArrayList;
import java.util.List;
import net.angle.omnicraft.common.OmnicraftGameWorld;
import net.angle.omnicraft.server.OmnicraftServer;
import net.angle.omnigeneration.api.GenerationManager;
import net.angle.omnigeneration.api.process.GenerationProcedure;
import net.angle.omnigeneration.api.process.GenerationRequest;
import net.angle.omnigeneration.api.process.GenerationStep;
import net.angle.omnigeneration.impl.AbstractGenerationStep;
import net.angle.omnigeneration.impl.BasicGenerationTicket;
import net.angle.omnigeneration.impl.SimpleGenerationStep;
import net.angle.omnilighting.api.LightingChannel;
import net.angle.omnilighting.impl.PrimaryLightingChannels;
import net.angle.omniregistry.api.DatumRegistry;
import net.angle.omniregistry.api.Registry;
import net.angle.omniregistry.api.RegistryCollection;
import net.angle.omniworld.api.chunks.Chunk;
import net.angle.omniworld.api.chunks.ChunkMap;
import net.angle.omniworld.api.chunks.VoxelComponent;
import net.angle.omniworld.api.space.Direction;
import net.angle.omniworld.impl.chunks.AdjacentChunkDependency;
import net.angle.omniworld.impl.space.PrimaryDirections;

/**
 *
 * @author angle
 */
public class LightingWorldgen {
    public static void apply (OmnicraftServer server, GenerationProcedure<Chunk, Vec3i> worldgen, int lightingHeight, int lightingDepth,
            SimpleGenerationStep<Chunk, Vec3i> beforeLighting) {
        
        OmnicraftGameWorld gameWorld = server.getGameWorld();
        RegistryCollection<Registry> registries = server.getRegistries();
        DatumRegistry<VoxelComponent> chunkComponents = registries.getEntryByName("Chunk Components");
        ChunkMap existingChunkMap = gameWorld.getChunkMap();
        int blockEdgeLengthOfChunk = existingChunkMap.getCoordinateSystem().getBlockEdgeLengthOfChunk();
        
        GenerationManager generationManager = worldgen.getGenerationManager();
        
        SimpleGenerationStep<Chunk, Vec3i> verticalLightingGenerationStep = new VerticalChunkLightingGeneration(chunkComponents, lightingHeight, blockEdgeLengthOfChunk);
        
        
        List<GenerationStep<Chunk, Vec3i>> lightingSteps = new ArrayList<>();
        lightingSteps.add(verticalLightingGenerationStep);
        
        DiagonalChunkLightingGeneration[] topDiagonals = new DiagonalChunkLightingGeneration[PrimaryLightingChannels.SKY_DIAGONAL_CHANNELS.length];
        
        int i = 0;
        for (LightingChannel channel : PrimaryLightingChannels.SKY_DIAGONAL_CHANNELS) {
            topDiagonals[i] = new DiagonalChunkLightingGeneration(chunkComponents, channel, true, lightingHeight, blockEdgeLengthOfChunk);
            lightingSteps.add(topDiagonals[i]);
            lightingSteps.add(topDiagonals[i].getSecondHalf());
            i++;
        }
        
        VoxelComponent<Long> lightingComponent = chunkComponents.getEntryByName("Lighting");
        
        GenerationStep<Chunk, Vec3i> initialLightingGenerationStep = new AbstractGenerationStep<Chunk, Vec3i>() {
            @Override
            public void process(GenerationRequest<Chunk, Vec3i> request) {
                if (request.getIdentifier().y >= lightingHeight) {
                    request.getResult().setAll(lightingComponent, PrimaryLightingChannels.SKY.saturateChannel(0l));
                    for (GenerationStep<Chunk, Vec3i> lightingStep : lightingSteps) {
                        request.addCompletedStep(lightingStep);
                    }
                }
                request.getResult().getChunkMap().chunkUpdated(request.getResult());
            }

            @Override
            public void beginNextSteps(GenerationRequest<Chunk, Vec3i> request) {
                if (request.getIdentifier().y < lightingHeight) {
                    for (GenerationStep<Chunk, Vec3i> lightingStep : lightingSteps) {
                        request.submitTicket(new BasicGenerationTicket(lightingStep, request));
                    }
                }
            }
        };
        
        initialLightingGenerationStep.addDependency(beforeLighting);
        beforeLighting.addDependentStep(initialLightingGenerationStep);
        
        Vec3i above = new Vec3i(0, blockEdgeLengthOfChunk, 0);
        
        verticalLightingGenerationStep.addDependency(initialLightingGenerationStep);
        verticalLightingGenerationStep.addDependency(new AdjacentChunkDependency(above, verticalLightingGenerationStep, generationManager));
        
        LightingFloodfillPropagation verticalFloodfill = new LightingFloodfillPropagation(chunkComponents, PrimaryLightingChannels.VERTICAL, 8);
        
        lightingSteps.add(verticalFloodfill);
        
        verticalFloodfill.addDependency(verticalLightingGenerationStep);
        for (Direction direction : PrimaryDirections.values()) {
            Vec3i adjacent = direction.getVec3i().mult(blockEdgeLengthOfChunk);
            verticalFloodfill.addDependency(new AdjacentChunkLightingDependency(lightingDepth, adjacent, verticalLightingGenerationStep, generationManager));
        }
        
        verticalLightingGenerationStep.addDependentStep(verticalFloodfill);
        
        LightingFloodfillPropagation[] diagonalFloodfills = new LightingFloodfillPropagation[PrimaryLightingChannels.SKY_DIAGONAL_CHANNELS.length];
        
        i = 0;
        for (DiagonalChunkLightingGeneration diagonal : topDiagonals) {
            DiagonalChunkLightingGeneration secondHalf = diagonal.getSecondHalf();
            Vec3i adjacent = diagonal.getChannel().getDiagonalDir().mult(blockEdgeLengthOfChunk);
            Vec3i aboveDiag = Vec3i.add(above, adjacent);
            
            diagonal.addDependency(new AdjacentChunkDependency(above, secondHalf, generationManager));
            diagonal.addDependency(new AdjacentChunkDependency(aboveDiag, secondHalf, generationManager));
            diagonal.addDependency(new AdjacentChunkDependency(aboveDiag, diagonal, generationManager));
            
            secondHalf.addDependency(new AdjacentChunkDependency(adjacent, diagonal, generationManager));
            secondHalf.addDependency(new AdjacentChunkDependency(aboveDiag, diagonal, generationManager));
            secondHalf.addDependency(new AdjacentChunkDependency(aboveDiag, secondHalf, generationManager));
            
            diagonalFloodfills[i] = new LightingFloodfillPropagation(chunkComponents, diagonal.getChannel(), 8);
        
            lightingSteps.add(diagonalFloodfills[i]);

            diagonalFloodfills[i].addDependency(diagonal);
            diagonalFloodfills[i].addDependency(secondHalf);
            for (Direction direction : PrimaryDirections.values()) {
                adjacent = direction.getVec3i().mult(blockEdgeLengthOfChunk);
                diagonalFloodfills[i].addDependency(new AdjacentChunkLightingDependency(lightingDepth, adjacent, diagonal, generationManager));
                diagonalFloodfills[i].addDependency(new AdjacentChunkLightingDependency(lightingDepth, adjacent, secondHalf, generationManager));
            }

            diagonal.addDependentStep(diagonalFloodfills[i]);
            secondHalf.addDependentStep(diagonalFloodfills[i]);
            i++;
        }
        
        worldgen.addStep(initialLightingGenerationStep);
        worldgen.addStep(verticalLightingGenerationStep);
        for (DiagonalChunkLightingGeneration diagonal : topDiagonals) {
            worldgen.addStep(diagonal);
            worldgen.addStep(diagonal.getSecondHalf());
        }
        for (LightingFloodfillPropagation diagonalFloodfill: diagonalFloodfills)
            worldgen.addStep(diagonalFloodfill);
        worldgen.addStep(verticalFloodfill);
    }
}