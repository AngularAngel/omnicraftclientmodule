package net.angle.omnicraft.server.worldgen;

import com.samrj.devil.math.Vec3i;
import lombok.RequiredArgsConstructor;
import net.angle.omniblocks.api.Block;
import net.angle.omnigeneration.impl.SimpleGenerationStep;
import net.angle.omnigeneration.api.process.GenerationRequest;
import net.angle.omniregistry.api.DatumRegistry;
import net.angle.omnisides.api.Side;
import net.angle.omniworld.api.chunks.Chunk;
import net.angle.omniworld.api.chunks.VoxelComponent;

/**
 *
 * @author angle
 */
@RequiredArgsConstructor
public class PillarPlainChunkGeneration extends SimpleGenerationStep<Chunk, Vec3i> {
    private final DatumRegistry<VoxelComponent> chunkComponents;
    private final DatumRegistry<Block> blocksRegistry;
    private final DatumRegistry<Side> sidesRegistry;
    private final int PillarHeight;
    private final int PillarSpacing;

    @Override
    public void process(GenerationRequest<Chunk, Vec3i> request) {
        Vec3i coordinates = request.getIdentifier();
        Chunk chunk = request.getResult();
        VoxelComponent blocksComponent = chunkComponents.getEntryByName("Blocks");
        if (coordinates.y == 0 || (coordinates.x % PillarSpacing == 0 && coordinates.z % PillarSpacing == 0 && coordinates.y <= PillarHeight)) {
            if (coordinates.y > 0 && coordinates.y < PillarHeight) {
                chunk.setAll(blocksComponent, blocksRegistry.getEntryByName("Stone Block"));
            } else {
                chunk.setAll(blocksComponent, blocksRegistry.getEntryByName("Dirt Block"));
                chunk.setAll(chunkComponents.getEntryByName("Top Sides"), sidesRegistry.getEntryByName("Grass Side"));
            }
            return;
        } else if (coordinates.y == 16) {

            chunk.set(blocksComponent, 0, 0, 2, blocksRegistry.getEntryByName("White Block"));

            Block redCrystalBlock = blocksRegistry.getEntryByName("Red Crystal Block");
            Block blueCrystalBlock = blocksRegistry.getEntryByName("Blue Crystal Block");

            for (int i = 0; i < 4; i += 4) {
                for (int j = 0; j < 2; j += 2) {
                    chunk.set(blocksComponent, i + 3, 0, j + 6, redCrystalBlock);
                    chunk.set(blocksComponent, i + 3, 1, j + 6, redCrystalBlock);
                    chunk.set(blocksComponent, i + 3, 2, j + 6, redCrystalBlock);
                    chunk.set(blocksComponent, i + 3, 3, j + 6, redCrystalBlock);
                    chunk.set(blocksComponent, i + 3, 4, j + 6, redCrystalBlock);

                    chunk.set(blocksComponent, i + 4, 0, j + 6, blueCrystalBlock);
                    chunk.set(blocksComponent, i + 4, 1, j + 6, blueCrystalBlock);
                    chunk.set(blocksComponent, i + 4, 2, j + 6, blueCrystalBlock);
                    chunk.set(blocksComponent, i + 4, 3, j + 6, blueCrystalBlock);
                    chunk.set(blocksComponent, i + 4, 4, j + 6, blueCrystalBlock);
                }
            }
        }
    }
}