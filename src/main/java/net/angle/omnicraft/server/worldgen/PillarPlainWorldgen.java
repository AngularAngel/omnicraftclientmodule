package net.angle.omnicraft.server.worldgen;

import com.samrj.devil.math.Vec3i;
import net.angle.omniblocks.api.Block;
import net.angle.omnicraft.common.OmnicraftGameWorld;
import net.angle.omnicraft.server.OmnicraftServer;
import net.angle.omnigeneration.api.GenerationManager;
import net.angle.omnigeneration.api.process.GenerationRequest;
import net.angle.omnigeneration.impl.BasicGenerationProcedure;
import net.angle.omnigeneration.impl.BasicGenerationTicket;
import net.angle.omnigeneration.impl.SimpleGenerationStep;
import net.angle.omniregistry.api.DatumRegistry;
import net.angle.omniregistry.api.Registry;
import net.angle.omniregistry.api.RegistryCollection;
import net.angle.omnisides.api.Side;
import net.angle.omniworld.api.chunks.Chunk;
import net.angle.omniworld.api.chunks.ChunkMap;
import net.angle.omniworld.api.chunks.VoxelComponent;
import net.angle.omniworld.impl.chunks.BasicChunk;
import net.angle.omniworld.impl.chunks.BasicChunkMap;
import net.angle.omniworld.impl.chunks.EmptyChunkGenerationStep;
import net.angle.omniworld.impl.chunks.FinalChunkGenerationStep;

/**
 *
 * @author angle
 */
public class PillarPlainWorldgen extends BasicGenerationProcedure<Chunk, Vec3i> {
    private final OmnicraftServer server;
    private final ChunkLoader chunkLoader;
    private final int lightingChunkHeight = 6;
    private SimpleGenerationStep<Chunk, Vec3i> finalChunkGenerationStep;

    public PillarPlainWorldgen(GenerationManager<Chunk, Vec3i> generationManager, OmnicraftServer server, ChunkLoader chunkLoader) {
        super(generationManager);
        this.server = server;
        this.chunkLoader = chunkLoader;
    }
    
    @Override
    public void init() {
        OmnicraftGameWorld gameWorld = server.getGameWorld();
        RegistryCollection<Registry> registries = server.getRegistries();
        DatumRegistry<VoxelComponent> chunkComponents = registries.getEntryByName("Chunk Components");
        ChunkMap existingChunkMap = gameWorld.getChunkMap();
        int blockEdgeLengthOfChunk = existingChunkMap.getCoordinateSystem().getBlockEdgeLengthOfChunk();
        int lightingHeight = lightingChunkHeight * blockEdgeLengthOfChunk;
        BasicChunkMap generatingChunkMap = new BasicChunkMap(existingChunkMap.getCoordinateSystem(), chunkComponents.getAllEntries());
        
        SimpleGenerationStep<Chunk, Vec3i> beginChunkGenerationStep = new EmptyChunkGenerationStep(BasicChunk::new, existingChunkMap, generatingChunkMap);
        SimpleGenerationStep<Chunk, Vec3i> chunkTerrainGenerationStep = new PillarPlainChunkGeneration(chunkComponents, (DatumRegistry<Block>) registries.getEntryByName("Blocks"),
                            (DatumRegistry<Side>) registries.getEntryByName("Sides"), blockEdgeLengthOfChunk * 3, blockEdgeLengthOfChunk * 6);
        SimpleGenerationStep<Chunk, Vec3i> chunkTerrainGenerationStep2 = new HollowSphereChunkGeneration(chunkComponents, (DatumRegistry<Block>) registries.getEntryByName("Blocks"),
                            (DatumRegistry<Side>) registries.getEntryByName("Sides"), blockEdgeLengthOfChunk * 4, blockEdgeLengthOfChunk * 5);
            
        finalChunkGenerationStep = new FinalChunkGenerationStep<>(gameWorld) {
            @Override
            public void process(GenerationRequest<Chunk, Vec3i> request) {
                super.process(request);
                chunkLoader.chunkLoaded(request.getResult());
            }
        };
        
        chunkTerrainGenerationStep.addDependency(beginChunkGenerationStep);
        chunkTerrainGenerationStep2.addDependency(chunkTerrainGenerationStep);
        
        beginChunkGenerationStep.addDependentStep(chunkTerrainGenerationStep);
        chunkTerrainGenerationStep.addDependentStep(chunkTerrainGenerationStep2);
        
        finalChunkGenerationStep.addDependency(chunkTerrainGenerationStep2);
        chunkTerrainGenerationStep2.addDependentStep(finalChunkGenerationStep);
        
        LightingWorldgen.apply(server, this, lightingHeight, -6 * blockEdgeLengthOfChunk, finalChunkGenerationStep);
        
        addStep(beginChunkGenerationStep);
        addStep(chunkTerrainGenerationStep);
        addStep(chunkTerrainGenerationStep2);
        addStep(finalChunkGenerationStep);
        
        super.init();
    }
    
    @Override
    public void resubmit(GenerationRequest<Chunk, Vec3i> request) {
        request.removeCompletedStep(finalChunkGenerationStep);
        request.submitTicket(new BasicGenerationTicket(finalChunkGenerationStep, request));
    }
}