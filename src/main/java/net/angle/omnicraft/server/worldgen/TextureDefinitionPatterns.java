package net.angle.omnicraft.server.worldgen;

import com.samrj.devil.math.Vec3;
import com.samrj.devil.math.Vec4;

/**
 *
 * @author angle
 */
public class TextureDefinitionPatterns {
    public static final int SET =          0x0;
    public static final int ADD =          0x1;
    public static final int SUB =          0x2;
    public static final int MULT =         0x3;
    public static final int DIV =          0x4;
    public static final int MIX =          0x5;
    public static final int THRESHOLDSET = 0x6;
    
    public static final int COLOR =       0x80;
    public static final int NORMAL =      0x40;
    public static final int METALLICITY = 0x20;
    public static final int SPECULAR =    0x10;
    public static final int ROUGHNESS =   0x08;
    public static final int LIGHT =       0x04;
    
    public static final int FINAL =       0x01;
    
    public static Vec4[] combine(Vec4[]... patterns) {
        int size = 0;
        for (Vec4[] pattern : patterns)
            size += pattern.length;
        
        Vec4[] result = new Vec4[size];
        int i = 0;
        for (Vec4[] pattern : patterns)
            for (Vec4 vec : pattern)
                result[i++] = vec;
        
        return result;
    }
    
    public static Vec4[] empty(Vec4 applicationParams) {
        return new Vec4[] {applicationParams, new Vec4(0, 0, 0, 0)};
    }
    
    public static Vec4[] solid(Vec4 applicationParams, Vec4 color) {
        return new Vec4[] {applicationParams, new Vec4(0, 0, 0, 1), color};
    }
    
    public static Vec4[] randomList(Vec4 applicationParams, Vec4... colors) {
        Vec4[] result = new Vec4[2 + colors.length];
        result[0] = applicationParams;
        result[1] = new Vec4(1, 0, 0, colors.length);
        int i = 2;
        for (Vec4 color : colors)
            result[i++] = color;
        return result;
    }
    
    public static Vec4[] sqCheckerboard(Vec4 applicationParams, int size, Vec4... colors) {
        Vec4[] result = new Vec4[2 + colors.length];
        result[0] = applicationParams;
        result[1] = new Vec4(2, size, size, colors.length);
        int i = 2;
        for (Vec4 color : colors)
            result[i++] = color;
        return result;
    }
    
    public static Vec4[] valueNoise(Vec4 applicationParams, float intensity, Vec4 color) {
        return new Vec4[] {applicationParams, new Vec4(3, intensity, 0, 1), color};
    }
    
    public static Vec4[] randomRange(Vec4 applicationParams, float range) {
        return new Vec4[] {applicationParams, new Vec4(4, 0, 0, range)};
    }
    
    public static Vec4[] mlSimplex(Vec4 applicationParams, float brightnessOffset, Vec4[] layerNoises, Vec4... colors) {
        Vec4[] result = new Vec4[2 + layerNoises.length + colors.length];
        result[0] = applicationParams;
        result[1] = new Vec4(5, 1 + ((layerNoises.length - 1) / 2.0f), brightnessOffset, colors.length);
        int i = 2;
        for (Vec4 layerNoise : layerNoises)
            result[i++] = layerNoise;
        for (Vec4 color : colors)
            result[i++] = color;
        
        return result;
    }
    
    public static Vec4[] transparentValueNoise(Vec4 applicationParams, float intensity, Vec4 color) {
        return new Vec4[] {applicationParams, new Vec4(7, intensity, 0, 1), color};
    }
    
    public static Vec4[] thresholdValueNoise(Vec4 applicationParams, float intensity, Vec4 color) {
        return new Vec4[] {applicationParams, new Vec4(8, intensity, 0, 1), color};
    }
    
    public static Vec4[] flatRayMarching(int textureID) {
        return new Vec4[] {new Vec4(0, 0, 0, textureID)};
    }
    
    public static Vec4[] internalBlockRayMarching(float size) {
        return new Vec4[] {new Vec4(1, size, size, size), new Vec4(0, 0, 0, 0)};
    }
    
    public static Vec4[] internalBlockRayMarching(float xsize, float ysize, float zsize, int textureID) {
        return new Vec4[] {new Vec4(1, xsize, ysize, zsize), new Vec4(0, 0, 0, textureID)};
    }
    
    public static Vec4[] internalBlockRayMarching(float xsize, float ysize, float zsize, Vec4 offset) {
        return new Vec4[] {new Vec4(1, xsize, ysize, zsize), offset};
    }
    
    public static Vec4[] valueNoiseRayMarching(float modifier, float intensity, int textureID) {
        return new Vec4[] {new Vec4(2, modifier, intensity, textureID)};
    }
    
    public static Vec4[] valueNoise1DRayMarching(Vec3 dir, float modifier, float intensity, int textureID) {
        return new Vec4[] {new Vec4(4, dir.x, dir.y, dir.z), new Vec4(0, modifier, intensity, textureID)};
    }
    
    public static Vec4[] multipleConditionRayMarching(Vec4[]... conditions) {
        Vec4[][] patterns = new Vec4[conditions.length + 1][];
        int i = 0;
        patterns[i++] = new Vec4[] {new Vec4(3, conditions.length, 0, 0)};
        for (Vec4[] condition : conditions)
            patterns[i++] = condition;
        return combine(patterns);
    }
}