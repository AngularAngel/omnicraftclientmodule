package net.angle.omnicraft.server.worldgen;

import com.samrj.devil.math.Vec3i;
import lombok.RequiredArgsConstructor;
import net.angle.omniblocks.api.Block;
import net.angle.omnigeneration.api.process.GenerationRequest;
import net.angle.omnigeneration.impl.SimpleGenerationStep;
import net.angle.omnilighting.impl.PrimaryLightingChannels;
import net.angle.omniregistry.api.DatumRegistry;
import net.angle.omniworld.api.chunks.Chunk;
import net.angle.omniworld.api.chunks.VoxelComponent;

/**
 *
 * @author angle
 */
@RequiredArgsConstructor
public class VerticalChunkLightingGeneration extends SimpleGenerationStep<Chunk, Vec3i> {
    private final DatumRegistry<VoxelComponent> chunkComponents;
    private final int lightingHeight, blockEdgeLengthOfChunk;

    @Override
    public void process(GenerationRequest<Chunk, Vec3i> request) {
        Chunk chunk = request.getResult();
        VoxelComponent<Block> blocksComponent = chunkComponents.getEntryByName("Blocks");
        VoxelComponent<Long> lightingComponent = chunkComponents.getEntryByName("Lighting");
        long saturatedChannels = PrimaryLightingChannels.VERTICAL.saturateChannel(0);
        Vec3i identifier = request.getIdentifier();
        identifier.y += blockEdgeLengthOfChunk;
        GenerationRequest<Chunk, Vec3i> otherRequest = request.getGenerationManager().getRequest(identifier);
        Chunk aboveChunk = null;
        if (identifier.y <= lightingHeight)
            aboveChunk = otherRequest.getResult();
        
        for (int x = 0; x < blockEdgeLengthOfChunk; x++)
            for (int z = 0; z < blockEdgeLengthOfChunk; z++) {
                if (identifier.y > lightingHeight || (aboveChunk.get(lightingComponent, x, blockEdgeLengthOfChunk - 1, z) & saturatedChannels) == saturatedChannels) {
                    for (int y = blockEdgeLengthOfChunk - 1; y >= 0; y--) {
                        if (!chunk.get(blocksComponent, x, y, z).isTransparent())
                            break;
                        else {
                            chunk.set(lightingComponent, x, y, z, chunk.get(lightingComponent, x, y, z) | saturatedChannels);
                        }
                    }
                }
            }
        
        request.getResult().getChunkMap().chunkUpdated(request.getResult());
    }
}